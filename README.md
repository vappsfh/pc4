#Placity - Plattform für Interaktive Wissensvermittlung   
---

Reengineering-Projekt im Rahmen des Bachelor-Studiengangs Informatik WS1516 an der FH-Bingen.


Auftraggeber: 	
>	Medien&Bildung.com <br/>
>	Herr Christian Kleinhanß 



>	KaiserdomAPP Projekt<br/>
>	Herr Fabian Kögel  


## "Placity" Verzeichnisstruktur ## 

```
/opt/PC4
├───00-InstallScripts  -> Setup: Server/Datenbank/Dependencies/Komponenten 
├───01-app             -> Placity-App zur Wiedergabe von Routen 
├───02-qe              -> QuestionEditor zur Erstellung von Routen 
├───03-df2             -> Server-Backend: Schnittstelle zu DB-, Fileserver 
|                         und verschiedener Webservices via REST-API 
├───04-ffmpeg          -> Custom-Build von ffmpeg zur Video-Encoding 
├───05-fileserver      -> Medienserver und Video-Encoding 
├───config             -> Dateien zur Server- und Systemkonfiguration 
└───DBSchema-CTs-Doku  -> Teil-, Zwischenlösungen und Doku 

```

## Backend-Server

### Dreamfactory 

Die Dreamfactory als Backend bietet eine leicht zu bedienende Oberfläche zur Verwaltung und Erstellung vielfältiger REST-APIs. Hier ist zusätzliche die User-Verwaltung, der Fileserver und eine ausführliche REST-API-Doku angesiedelt. 

Eine umfangreiche Dokumentation befindet sich im Git-Ordner unter: 

` pc4/DB-CTs-Src-Doku/dreamfactoryDoku/* `

Über folgenden Link ist die DreamFactory erreichbar:
http://df.[DOMAIN.TLD]/

[Dreamfactory: Tutorials](http://wiki.dreamfactory.com/DreamFactory/Tutorials#Configuration_Settings)


Architektur 
![Dreamfactory](Dreamfactory-architecture.png)




### phpPGadmin

PhpPGadmin bietet unter dem Link: http://[DOMAIN.TLD]/phpPGadmin eine zusätzliche grafische Oberfläche zur Verwaltung der PostgeSQL-Datenbank: 


Hier die Initialen Passwörter von PostgreSQL nach Installation: 

>Datenbank: placity<br/>
>Benutzer:  placity<br/>
>Password:  placity<br/>
>root: 	    postgres<br/>
>Password:  VAPPS2016<br/>   

*Bitte nach der Installation ändern*  

![Oberfläche](PostgreSQL_phpPGadmin.jpg)

### Datenbank Backup/Restore PostgreSQL über SSH

Ein Datenbankbackup lässt sich am einfachsten über eine SSH-Console in der BASH erstellen.   

##### Datensicherung erstellen:

Eine einzelne Datenbank kann mit dem Befehl pg_dump gesichert werden, dabei werden alle Daten gesichert.
```
sudo -u postgres pg_dump --format=custom --dbname=placity --file=datenbank.dump 
```


Um alle vorhandenen Datenbanken (inkl. Systemtabellen, Benutzer, Zugriffsrechte) sichern:

```
sudo -u postgres pg_dumpall > alle-datenbanken.dump 
```

##### Datensicherung wiederherstellen

Einzelne Datenbank aus der Datensicherung wiederherstellen:
```
sudo -u postgres pg_restore -d placity datenbank.dump 
```
Vollständige Datensicherung aller Datenbanken wiederherstellen:

```
sudo -u postgres psql -f alle-datenbanken.dump postgres 
```

#### Passwort des Root-DB-User postgres ändern
```
sudo -u postgres psql  		
\password <neues PW>  
\q  
```

#### Settings für PostgreSQL 

Settings für die PostgreSQL-Datenbank finden sich in folgenden Dateien: 
```
/etc/postgresql/9.3/main/environment
/etc/postgresql/9.3/main/pg_ctl.conf
/etc/postgresql/9.3/main/postgresql.conf
/etc/postgresql/9.3/main/start.conf
```

Settings für Authentifizierung hier:

```
/etc/postgresql/9.3/main/pg_hba.conf
/etc/postgresql/9.3/main/pg_ident.conf
```

#### offizielle Doku

[Offizielle Doku PostgreSQL 9.3](http://www.postgresql.org/docs/9.3/interactive/index.html)



### Veröffentlichung & Lizenzierung ### 

Alle Ergebnisse des Praxisprojekts sind hiermit veröffentlicht und können unter Beachtung des folgenden Lizenztextes frei verwendet werden.
```
Copyright (c) 2016, VAPPS Projektteam der FH-Bingen Proj-WS1516

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```

----------------------
Alexander Weiß 17.02.2016