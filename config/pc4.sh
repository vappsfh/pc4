#!/bin/bash 
# ------------------------------------------------------------------------#
# Setup DreamFactory / ffmpeg / postgreSQL                                #
# ------------------------------------------------------------------------# 
# Author: Alexander Weiß                                                  # 
# 15.01.2016                                                              #
# ------------------------------------------------------------------------# 
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        # 
#                                                                         # 
# Permission to use,copy, modify, and/or distribute this software for any # 
# purpose with or without fee is hereby granted, provided that the above  # 
# copyright notice and this permission notice appear in all copies.       # 
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  # 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        # 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR # 
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  # 
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   # 
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF # 
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          # 
# #########################################################################
. config.sh

## highlighting with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }     #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }     #yellow
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 	#green
export -f logError
export -f highlight
export -f myecho


## --------------------------------------------------------------------------------------
## install-01-dependencies.sh 
## --------------------------------------------------------------------------------------

function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }     #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }     #yellow
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 	#green
export -f logError
export -f highlight
export -f myecho

highlight<<yellow
############################################################################
## Install 3rd party Components 
## ----------------------------
## Webserver-Components: 
##--------------------------------------------------------------------------
yellow


function installDPKG(){
        echo "Installiere $1" | highlight
        apt-get -y -qq install $1 2> >(logError)
        exitcode=$?
        if [ $exitcode  -gt  0 ]
        then
                echo "Installation nicht erfolgreich bei Paket $1" | logError | tee -a errorlog
        else
                echo "Installation erfolgreich bei Paket $1" | myecho
        fi
        echo "$exitcode $1" | tee -a log | myecho
		
}


highlight<<YELLOW
###########################################################################
## Install 3rd party Components 
## ----------------------------
## Webserver-Components: 
## 			php5	5.
##   	 apache2 	2.
## 	  postgreSQL	9.3
## 	  phpPGadmin	 
## 	  	 openssh 	
## 	
## Middleware:	v8js node.js npm composer grunt yo bower openssl
## Tools: 		git curl vim git g++ cpp	
###########################################################################
YELLOW

highlight<<eof
## update dpkg-list / upgrade 
eof
apt-get -y -qq update
apt-get -y -qq dist-upgrade
highlight<<eof
## dependencies
eof

installDPKG curl 
installDPKG vim 
installDPKG git 
installDPKG g++ 
installDPKG cpp 
installDPKG jq
installDPKG crudini
installDPKG gzip 
installDPKG bzip2
installDPKG tar
installDPKG mercurial 
installDPKG libpython2.7-stdlib
installDPKG python-chardet
installDPKG libwrap0
installDPKG python-six
installDPKG python2.7-minimal
installDPKG tcpd
installDPKG openssh-server
installDPKG libpython-stdlib
installDPKG python-requests
installDPKG python-minimal
installDPKG ssh-import-id
installDPKG libpython2.7-minimal
installDPKG python-urllib3
installDPKG libck-connector0
installDPKG python2.7
installDPKG ncurses-term
installDPKG python
installDPKG openssh-sftp-server


highlight<<eof
## php5 
eof

installDPKG php5
installDPKG php5-ldap
installDPKG php5-imagick 
installDPKG php5-gd 
installDPKG php5-imap 
installDPKG php5-curl 					
installDPKG php5-mysql
installDPKG php5-sqlite
installDPKG php5-pgsql 
installDPKG php5-sybase 
installDPKG php-pear 
installDPKG php5-common 
installDPKG php5-cli 
installDPKG php5-json 
installDPKG php5-mcrypt 
installDPKG php5-dev 
installDPKG libv8-dev 
installDPKG php5-cli

highlight<<eof
## apache2 
eof

installDPKG apache2 
installDPKG apache2-doc 

highlight<<eof
## postgresql-9.3 
eof
installDPKG postgresql-client-common 
installDPKG postgresql-doc
installDPKG postgresql-common
installDPKG postgresql-client
installDPKG postgresql-contrib
installDPKG libpq5
installDPKG postgresql-contrib-9.3 
installDPKG postgresql-client-9.3
installDPKG libossp-uuid16 
installDPKG ssl-cert 
installDPKG libxslt1.1 
installDPKG postgresql-9.3
installDPKG postgresql
installDPKG postgresql-doc-9.3

highlight<<eof
## phpadmin 
eof
installDPKG phppgadmin

highlight<<eof
## v8js node.js npm composer grunt yo bower
eof
apt-get install -f -y -qq
apt-get update -qq -y 

curl -sL https://deb.nodesource.com/setup_5.x | bash -
apt-get install -y -qq nodejs
if [ $? -gt 0 ] ; 
	then 
		echo "Installation nicht erfolgreich bei Paket nodejs" | logError | tee -a errorlog 
	else
		echo "Installation erfolgreich bei Paket nodejs" | highlight
	fi

	
npm install --global yo  
npm install --global bower
npm install --global grunt grunt-cli

				
curl -sS https://getcomposer.org/installer | php 
mv composer.phar /usr/local/bin/composer 

pecl install v8js-0.1.3
touch /etc/php5/mods-available/v8js.ini
echo extension=v8js.so >> /etc/php5/apache2/php.ini
echo extension=v8js.so >> /etc/php5/cli/php.ini

apt-get -y autoremove



highlight<<yellow

## ---------------------------------------------------------------------------------------------
## install-02-gitclone.sh
## ---------------------------------------------------------------------------------------------





yellow


function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }     #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }     #yellow
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 	#green

genUpstartScript(){
cat<<EOF

#!/bin/sh
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

. $PC_CONFIG/config.sh
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }     #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }     #yellow
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 	#green
mount --bind $FILE_STORE $FILES
EOF
}

cp /etc/rc.local rc.local_
genUpstartScript > /etc/rc.local
cat rc.local_ | grep -e '^[^#]' >>  cat /etc/rc.local



highlight<<yellow
###########################################################################

###########################################################################
## clone main-repository from git
########################################################################### 
yellow

deployment_key(){
cat<<key
-----BEGIN RSA PRIVATE KEY-----
MIIEoQIBAAKCAQEAiBuwooj4SehP+H9bcgt9HG38o4HY26HCjVLIwX1C1t3HWUi1
kfH34Hvwk8gl+2lewKey/aiCs6Zj3FAMt5ImJhdjTp1s7G2UgEXVmGXVWEnOjfq9
7xFmTALOsDcE7RCfDaDjmyGxIGhz9mCHpkIAV9FHUcF4ydwXXAZLFFBi9kJnCmDs
I1/SIMZAOuK1Ug5jAMbGnqnd6Y1vOgijANCfi6vvSfxxoRbnTrbQaYbRibNKL+nQ
QgkWFNDco6mAn/TBLqDqGTr9u6CbuIqTIVFuPhTU5RZtBUSJP8B1gSZPvL83LJDY
BnvFImN+In5OIXtRa2AxIJa08CXIZ5JGwtgkYwIBJQKCAQBJkm1Q8BeB5Vuo6uVS
ZxMx9kNfTRtGSZmYf8Z2bTji2L7IelRO46iVBLlko4onCIY+n9ZfmVtoBune3yl3
+/jx/s3lTi0KLWUHEP3V1i4h4rTXH8Cc5s+DCG+5Mn80CPwHXeLQZT0mRkyFLUJn
tPlEOcWvptmB3sCExSGqHZ1G1v6BXQ7xWqXEwyiMPVDy+gIsdDYfmymD2t5d9Aso
QeaI+smcUGekVVFhvQXO9RMODyB1dKe78CvyikBv4DZMTbYmZHpLgnMIR63KVtn4
ous/0l0MGrxELnDzW2IkygqCBjQ9wsOjnN8lxO+ysPFmWCE6gRZejwm4MaCiIbj8
XxdNAoGBAMrAa6WZ4LjH/gH4BqqjwIRFBxotsqpuJ7POlb0On9HImOqT8u4YLgVO
PbvDAQTC+olcFUwCWaSL6rtetBja6EWaHNtiVf4hhxzwK7JFDZZReDzsZuKXD4FM
Z4ANPqpgjL+2klDqURAf53DmQCBkcNbKaYP/oJZRhmGpr6J4DysPAoGBAKvaorex
ZDN5kmZync6GZcEctXuDS1nhENM9op4a6hZ43KlU2yoyfj6lQcnnps/stl5g7+eK
ZlNumZB1/QNRfLdnFvlEzURkYbB4cWc2sAsc8QVvF64jRYPKvLMWPquFQJbQiWn6
SH6WNh6YEG8mGqOOVKolOkyjdspevdreTEFtAoGAfgjo+D0PJsGCOJNCahLDx9fh
0gC7K6xCMYBq6yTENkxDYWLjHmIOxQcfdLd9LHkz9IVZWMMwzhHJRApv88ZZCKvo
bLK/1VMco0kv61tbeR3i9XBboZwsO54ygAg7tgSqhQLJp+uaLJ417DwaBkxGJKdW
WPjZcjKtevrHH84XREcCgYA3vIfM35YeiEsoJSxC+yfrmpu5WwOucoH/UkKUJGeY
leazdXeDTqV8GevSx6u5DoBV9ezxJfesWzi5OwXzPQXanfmc87gx2130C2MMt8pk
eBbRVHZh/ZoBOtVqg8FMYpF9BVz/xstnU0/Ski7X8K6xlfHx/jxr4f0DXP9N5ztT
fQKBgQCaIHvBh9v8wbGbJISP3EGxmAbKNYssav9AyYSPrPKEYEdlcV3wxh9jemN1
oAH1qvm3aRmmbGXNKv2m7ft7WWoRxi0+ezwFyc/VQPcAviTa8gLM5riUEo6IdRv5
T2aUNR0hplDsv4SVwz3cXdZ9LOYHYUkLSkTsdpZMPX5M5lI5FA==
-----END RSA PRIVATE KEY-----
key
}
key(){
cat<<key
---- BEGIN SSH2 PUBLIC KEY ----
Comment: "rsa-key-20160223"
AAAAB3NzaC1yc2EAAAABJQAAAQEAiBuwooj4SehP+H9bcgt9HG38o4HY26HCjVLI
wX1C1t3HWUi1kfH34Hvwk8gl+2lewKey/aiCs6Zj3FAMt5ImJhdjTp1s7G2UgEXV
mGXVWEnOjfq97xFmTALOsDcE7RCfDaDjmyGxIGhz9mCHpkIAV9FHUcF4ydwXXAZL
FFBi9kJnCmDsI1/SIMZAOuK1Ug5jAMbGnqnd6Y1vOgijANCfi6vvSfxxoRbnTrbQ
aYbRibNKL+nQQgkWFNDco6mAn/TBLqDqGTr9u6CbuIqTIVFuPhTU5RZtBUSJP8B1
gSZPvL83LJDYBnvFImN+In5OIXtRa2AxIJa08CXIZ5JGwtgkYw==
---- END SSH2 PUBLIC KEY ----
key
}
mkdir ~/.ssh
deployment_key > ~/.ssh/deployment-key
key > ~/.ssh/deployment-key.pub
chmod -R 0600 ~/.ssh
ssh-add ~/.ssh/deployment-key
eval `ssh-agent` 
ssh-add ~/.ssh/deployment-key

echo "Host bitbucket
	  HostName bitbucket.org
      IdentityFile ~/.ssh/deplyment-key" >> ~/.ssh/config

if [ -d $PC_INSTALL_DIR ] 
then 
	mv $PC_INSTALL_DIR $PC_INSTALL_DIR-old ;
	echo "Fehler: $PC_INSTALL_DIR existierte bereits vor download von git!!!" > >(logError)
	echo "$PC_INSTALL_DIR wurde in $PC_INSTALL_DIR-old umbenannt. Bitte prüfen!" > >(logError)
fi 
	  
git clone git@bitbucket.org:vappsfh/pc4.git $PC_INSTALL_DIR

exitcode=$?

if [ $exitcode -gt 0 ]; then echo "git clone wurde beendet mit Fehler-Code: $exitcode "  > >(logError) ; fi

cat config.sh  > $PC_INSTALL_DIR/config/config.sh  2> >(logError)
exitcode=$?
if [ $exitcode -gt 0 ]
then 
	echo "Kopieren von config.sh nicht erfolgreich!!! Code: $exitcode"  > >(logError)
	echo "Installation wird bei Skript 'install-02-gitclone.sh' abgebrochen. 
	Bitte Clone-Prozess manuell durchführen!
	Installation danach mit Skript 'install-03-a2setup.sh' fortsetzen."  > >(logError)
fi

## ---------------------------------------------------------------------------------------------
## starte: install-03-a2setup.sh
## ---------------------------------------------------------------------------------------------
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' >&1; }     #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' >&1; }     #yellow
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' >&1; } 	#green

highlight<<yellow
###########################################################################
## APACHE Konfiguration 
## ------------------------------------------------------------------------ 
yellow


a2enmod rewrite
a2enmod ssl
php5enmod v8js
php5enmod mcrypt
php5enmod ldap
php5enmod `ls  /etc/php5/mods-av* | grep -e '^[^.)]*' -o`


highlight<<yellow
############################################################################
## APACHE Konfiguration 
## --------------------
## alias:    
##	$DOMAIN/phppgadmin  -> phpPGadmin		/usr/share/phppgadmin
##	$DOMAIN/media       -> media			$FILES
##
## vhosts: 
##	$DF2_SUBDOMAIN	    -> dreamfactory		$DF_ROOT_DIR
##	$QE_SUBDOMAIN		-> QuestionEditor	$QE_ROOT_DIR
##	$PLACITY_SUBDOMAIN	-> App 				$APP_ROOT_DIR
##			
############################################################################
yellow



mkdir $PC_CONFIG/apache2
mkdir $PC_CONFIG/apache2/conf-enabled
mkdir $PC_CONFIG/apache2/sites-enabled


echo "
Alias /phppgadmin /usr/share/phppgadmin

<Directory /usr/share/phppgadmin>

DirectoryIndex index.php
AllowOverride None

order deny,allow
#deny from all
#allow from 127.0.0.0/255.0.0.0 ::1/128
allow from all

<IfModule mod_php5.c>
  php_flag magic_quotes_gpc Off
  php_flag track_vars On
  #php_value include_path .
</IfModule>

</Directory>
" > $PC_CONFIG/apache2/conf-enabled/phppgadmin.conf

echo "
Alias /media $FILES
        <Directory $FILES>
                Options all
               AllowOverride All
               AddDefaultCharset utf-8
                Require all granted
        </Directory>
" > $PC_CONFIG/apache2/conf-enabled/alias-media.conf

echo "
<VirtualHost *:80>
   ServerName $DF2_SUBDOMAIN
    DocumentRoot $DF2_ROOT_DIR
    ServerAdmin webmaster@$DOMAIN

    <Directory $DF2_ROOT_DIR>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        AllowOverride None
        Require all granted
        RewriteEngine on
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^.*$ /index.php [L]

        <LimitExcept GET HEAD PUT DELETE PATCH POST>
            Allow from all
        </LimitExcept>
    </Directory>
</VirtualHost>
" > $PC_CONFIG/apache2/sites-enabled/df-vhost.conf 
echo "
<VirtualHost *:80>
    ServerName $QE_SUBDOMAIN 
    DocumentRoot $QE_ROOT_DIR
    ServerAdmin webmaster@$DOMAIN

    <Directory $QE_ROOT_DIR>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        AllowOverride None
        Require all granted
        RewriteEngine on
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^.*$ /index.php [L]

        <LimitExcept GET HEAD PUT DELETE PATCH POST>
            Allow from all
        </LimitExcept>
    </Directory>
</VirtualHost>
" > $PC_CONFIG/apache2/sites-enabled/qe-vhost.conf 

echo "
<VirtualHost *:80>
   ServerName $PLACITY_SUBDOMAIN
    DocumentRoot $APP_ROOT_DIR
    ServerAdmin webmaster@$DOMAIN

    <Directory $APP_ROOT_DIR>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        AllowOverride None
        Require all granted
        RewriteEngine on
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^.*$ /index.php [L]

        <LimitExcept GET HEAD PUT DELETE PATCH POST>
            Allow from all
        </LimitExcept>
    </Directory>
</VirtualHost>
" > $PC_CONFIG/apache2/sites-enabled/placity-vhost.conf 

rm /etc/apache2/conf-enabled/phppgadmin.conf
rm /etc/apache2/conf-enabled/alias-media.conf
rm /etc/apache2/sites-enabled/df-vhost.conf
rm /etc/apache2/sites-enabled/qe-vhost.conf
rm /etc/apache2/sites-enabled/placity-vhost.conf

ln $PC_CONFIG/apache2/conf-enabled/phppgadmin.conf /etc/apache2/conf-enabled
ln $PC_CONFIG/apache2/conf-enabled/alias-media.conf /etc/apache2/conf-enabled
ln $PC_CONFIG/apache2/sites-enabled/df-vhost.conf /etc/apache2/sites-enabled
ln $PC_CONFIG/apache2/sites-enabled/qe-vhost.conf /etc/apache2/sites-enabled
ln $PC_CONFIG/apache2/sites-enabled/placity-vhost.conf /etc/apache2/sites-enabled
service apache2 restart || service apache2 start


############################################################################
## ssl 
############################################################################
##
## echo "
##<IfModule mod_ssl.c>
##<VirtualHost *:443>
##   ServerName df.$DOMAIN 
##    DocumentRoot /opt/df2/public
##    ServerAdmin webmaster@$DOMAIN
##
##    <Directory /opt/df2/public>
##        Options -Indexes +FollowSymLinks -MultiViews
##        AllowOverride All
##        AllowOverride None
##        Require all granted
##        RewriteEngine on
##        RewriteBase /
##        RewriteCond %{REQUEST_FILENAME} !-f
##        RewriteCond %{REQUEST_FILENAME} !-d
##        RewriteRule ^.*$ /index.php [L]
##
##        <LimitExcept GET HEAD PUT DELETE PATCH POST>
##            Allow from all
##        </LimitExcept>
##    </Directory>
##</VirtualHost>
##</IfModule>
##" > /etc/apache2/sites-available/df-ssl.conf 

## echo "
##<IfModule mod_ssl.c>
##<VirtualHost *:443>
##   ServerName df.$DOMAIN 
##    DocumentRoot /opt/df2/public
##    ServerAdmin webmaster@$DOMAIN
##
##    <Directory /opt/df2/public>
##        Options -Indexes +FollowSymLinks -MultiViews
##        AllowOverride All
##        AllowOverride None
##        Require all granted
##        RewriteEngine on
##        RewriteBase /
##        RewriteCond %{REQUEST_FILENAME} !-f
##        RewriteCond %{REQUEST_FILENAME} !-d
##        RewriteRule ^.*$ /index.php [L]
##
##        <LimitExcept GET HEAD PUT DELETE PATCH POST>
##            Allow from all
##        </LimitExcept>
##    </Directory>
##</VirtualHost>
##</IfModule>
##" > $PC_CONFIG/apache2/sites-enabled/qe-ssl.conf 

## echo "
##<IfModule mod_ssl.c>
##<VirtualHost *:443>
##   ServerName df.$DOMAIN 
##    DocumentRoot /opt/df2/public
##    ServerAdmin webmaster@$DOMAIN
##
##    <Directory /opt/df2/public>
##        Options -Indexes +FollowSymLinks -MultiViews
##        AllowOverride All
##        AllowOverride None
##        Require all granted
##        RewriteEngine on
##        RewriteBase /
##        RewriteCond %{REQUEST_FILENAME} !-f
##        RewriteCond %{REQUEST_FILENAME} !-d
##        RewriteRule ^.*$ /index.php [L]
##
##        <LimitExcept GET HEAD PUT DELETE PATCH POST>
##            Allow from all
##        </LimitExcept>
##    </Directory>
##</VirtualHost>
##</IfModule>
##" > $PC_CONFIG/apache2/sites-enabled/placity-ssl.conf
## 

## ln $PC_CONFIG/apache2/sites-enabled/df-ssl.conf /etc/apache2/sites-enabled
## ln $PC_CONFIG/apache2/sites-enabled/qe-ssl.conf /etc/apache2/sites-enabled
## ln $PC_CONFIG/apache2/sites-enabled/placity-ssl.conf /etc/apache2/sites-enabled
## service apache2 restart || service apache2 start
############################################################################ 

highlight<<YELLOW
###########################################################################
## ENDE: config apache php 
###########################################################################
YELLOW

highlight<<yellow
###########################################################################

###########################################################################
## start Installation ffmpeg,ffprobe...
###########################################################################
yellow
. $FF_MPEG_DIR/install/ffmpeg_install.sh 2> >(tee -a errorlog) | logError

highlight<<yellow
###########################################################################
## ende Installation ffmpeg,ffprobe...
###########################################################################
yellow


highlight<<YELLOW
############################################################################
## Setup database 
## -----------------------
## ------------------------------------------------------------------------- 
YELLOW


sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD $PW_PGSQL_ROOT;"
usermod -g postgres www-data
sudo -u postgres createuser -d placity
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD $PW_PG_PACITY;"
sudo -u postgres createdb -O placity placity
sudo -u postgres pg_restore --dbname=placity --create --verbose $PLACITY_DB_DUMP
service postgresql restart

highlight<<YELLOW
############################################################################
## SETUP DREAMFACTORY  
## -----------------------
## ------------------------------------------------------------------------- 
YELLOW



cd $PC_INSTALL_DIR

chown -R www-data:www-data $PC_INSTALL_DIR
chmod 2775 -R $PC_INSTALL_DIR

echo "{
    "github-oauth": {
        "github.com": "8d6585743724301f577a40cdb0c88620af776ab8"
    }
}" >> /root/.composer/auth.json 

composer install 
if [ $? -gt 0 ] ; then echo "Fehler bei Installation von Dreamfactory.
Bitte starten Sie das Skript neu: '"'composer install'"'" > logError ; fi


highlight<<yellow
## Modifizierung der Login-Prozeduren in DREAMFACTORY
## Ermöglicht Login für UserAccounts aus Bestandsdatenübernahme  (MD5-Hash) 
yellow
rm $PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/DatabaseUserProvider.php
rm $PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/EloquentUserProvider.php

cp ./EloquentUserProvider.php    $PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/
cp ./DatabaseUserProvider.php    $PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/


highlight<<yellow
#############################################################################
## FileServer
## --------------------------------------------------------------------------
yellow
echo "mount --bind $FILE_STORE $FILES" >> /etc/env*
mount --bind $FILE_STORE $FILES


highlight<<yellow
#############################################################################

"Installation und Setup des Backend-Servers abgeschlossen" 

yellow


