
## highlighting with some colors  
logError() { cat $@ | sed 's,.*,\x1B[31m&\x1B[0m,' | tee -a inst-libs-ffmpeg.err >&2 ; }
highlight(){ echo $@ |  sed 's,.*,\x1B[33m&\x1B[0m,'; } 
export -f logError
export -f highlight

while read line 
do
highlight<< YELLOW
*********************************************************************
                     install $(line)
*********************************************************************
YELLOW

    apt-get -s -y install $line        1> >(tee -a inst-libs-ffmpeg.log) \
                                      2> >(sed 's,.*,\x1B[31m&\x1B[0m,')
    if [ $? -gt 0 ] 
        then

logError<<RED
*********************************************************************
ERROR: apt-get install $line
*********************************************************************
Fehler beim Installieren des Pakets: $line
Für Details siehe log-file:         at inst-libs-ffmpeg.err | more
---------------------------------------------------------------------
$(tail -10 | sed -e "s/.\{20\}/&\n/g")

*********************************************************************
*********************************************************************

RED

    echo $[$line]  | sed 's,.*,\x1B[31m&\x1B[0m,' >>failed-lines.info
  fi
highlight<< YELLOW
*********************************************************************
                  ENDE: install $(line)
*********************************************************************
YELLOW

done<< EOF    
libass                               
libassuan-dev                        
libassuan0                           
libassuan0-dbg                       
libass-dev                           
libass4                              
libassa3.5-5                         
libassa3.5-5-dbg                     
libassa3.5-5-dev                     
libassimp-dev                        
libassimp-doc                        
libassimp3                           
libvpx                               
libvpx-dev                           
libvpx-doc                           
libvpx1                              
libvpx1-dbg                          
python-webm                          
libvpx                               
libvpx-1.5.0                         
libx264                              
libx264-142                          
libx264-dev                          
libx265                              
libass                               
libassuan-dev                        
libassuan0                           
libassuan0-dbg                       
libass-dev                           
libass4                              
libassa3.5-5                         
libassa3.5-5-dbg                     
libassa3.5-5-dev                     
libassimp-dev                        
libassimp-doc                        
libassimp3                           
libxvid                              
libxvidcore-dev                      
libxvidcore4                         
libwebp                              
libwebp-dev                          
libwebp5                             
libwebpdemux1                        
libwebpmux1                          
python-webm                          
librtmp                              
librtmp-dev                          
librtmp0                             
libgnutls                            
libgnutls-dev                        
libgnutls-openssl27                  
libgnutls26                          
libgnutls26-dbg                      
libgnutlsxx27                        
libgnutls-xssl0                      
libgnutls28                          
libgnutls28-dbg                      
libgnutls28-dev                      
libgnutlsxx28                        
python-gnutls                        
libtheora                            
libtheora-dbg                        
libtheora-dev                        
libtheora-doc                        
libtheora0                           
libtheora-bin                        
libtheora-ocaml                      
libtheora-ocaml-dev                  
libfrei0r                            
libfrei0r-ocaml                      
libfrei0r-ocaml-dev                  
libvidstab                           
libfreetype                          
libfreetype6                         
libfreetype6-dev                     
libgd-gd2-perl                       
libcoin80                            
libgd-gd2-noxpm-perl                 
libgd-perl                           
libopenjpeg                          
libopenjpeg-dev                      
libopenjpeg2                         
libopenjpeg2-dbg                     
libsoxr                              
libsoxr-dev                          
libsoxr-lsr0                         
libsoxr0                             
libopus                              
libopus-dbg                          
libopus-dev                          
libopus-doc                          
libopus0                             
libopus-ocaml                        
libopus-ocaml-dev                    
libopusfile-dbg                      
libopusfile-dev                      
libopusfile-doc                      
libopusfile0                         
opus-tools                           
libspeex                             
libspeex-dbg                         
libspeex-dev                         
libspeex1                            
libspeexdsp-dev                      
libspeexdsp1                         
libspeex-ocaml                       
libspeex-ocaml-dev                   
libvorbis                            
libvorbis-dbg                        
libvorbis-dev                        
libvorbis0a                          
libvorbisenc2                        
libvorbisfile3                       
python-pyvorbis                      
python-pyvorbis-dbg                  
libogg-vorbis-decoder-perl           
libomxil-bellagio0-components-vorbis 
libvorbis-ocaml                      
libvorbis-ocaml-dev                  
libvorbisidec-dev                    
libvorbisidec1                       
libvorbisspi-java                    
libmp3lame                           
libmp3lame-dev                       
libmp3lame-ocaml                     
libmp3lame-ocaml-dev                 
libmp3lame0                          
libsox-fmt-mp3                       
libvo-amrwbenc                       
libvo-amrwbenc-dev                   
libvo-amrwbenc0                      
libopencore-amrnb                    
libopencore-amrnb-dev                
libopencore-amrnb0                   
libopencore-amrnb0-dbg               
libopencore-amrwb                    
libopencore-amrwb-dev                
libopencore-amrwb0                   
libopencore-amrwb0-dbg               
EOF
