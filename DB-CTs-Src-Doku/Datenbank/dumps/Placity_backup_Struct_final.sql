--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.10
-- Dumped by pg_dump version 9.3.1
-- Started on 2016-02-16 22:15:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE SEQUENCE c_id_seq
    START WITH 100
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE content_id_seq
    START WITH 100
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE ct_id_seq
    START WITH 100
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;

--


CREATE SEQUENCE group_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE mediafile_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE organization_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE page_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE route_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE routemodel_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--

CREATE SEQUENCE routeinfo_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
CREATE TABLE address (
    id_orga integer NOT NULL,
    street character varying(255) NOT NULL,
    postcode numeric(5,0) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL
);


ALTER TABLE public.address OWNER TO placity;
--
-- TOC entry 278 (class 1259 OID 26624)
-- Name: content; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--



CREATE TABLE content (
    id integer DEFAULT nextval('c_id_seq'::regclass) NOT NULL,
    id_page integer,
    id_content_type integer NOT NULL,
    data_obj json,
    pos integer DEFAULT 0
);


ALTER TABLE public.content OWNER TO placity;

--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 278
-- Name: COLUMN content.data_obj; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN content.data_obj IS '{
     "languages": 
     [
         {"lang": "de_DE", "fields": [ "key":"value", "key":"value" , ... ],
         {"lang": "en_EN", "fields": [ "key":"value", "key":"value" , ... ]
     ]
}';


--
-- TOC entry 257 (class 1259 OID 26406)
-- Name: contenttype; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contenttype (
    id integer DEFAULT nextval('ct_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    description text,
    data_schema json,
    app_ctrl_class character varying(255) NOT NULL,
    app_html_tpl_file character varying(255) NOT NULL,
    qe_ctrl_class character varying(255) NOT NULL,
    qe_html_tpl_file character varying(255) NOT NULL,
    sid integer,
    helptext json
);


ALTER TABLE public.contenttype OWNER TO placity;

--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.name; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.name IS 'Name der Instanz des Entitytypen ContentType. Eine Entity des Entitytypen ContentType klassifiziert eine Gruppe von gleich strukturierten Contentinstanzen; ähnlich einer Klassendefinition in objektorientierten Programmiersprachen.   ';


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.description; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.description IS 'Multiligualität fehlt hier. ';


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.app_ctrl_class; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.app_ctrl_class IS 'Wiedergabe Controller AngularJS';


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.app_html_tpl_file; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.app_html_tpl_file IS 'Wiedergabe Struktur';


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.qe_ctrl_class; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.qe_ctrl_class IS 'Eingabeformular Controller AngularJS ';


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.qe_html_tpl_file; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.qe_html_tpl_file IS 'Eingabeformular';


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN contenttype.helptext; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.helptext IS '{
"languages": 
   [ 
      {"lang":"de_DE", "val":"Hilfetext"},
      {"lang":"de_EN", "val":"help text"} 
   ]
} ';



--
-- TOC entry 258 (class 1259 OID 26413)
-- Name: filesharedforgroup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE filesharedforgroup (
    id_group integer NOT NULL,
    id_file integer NOT NULL
);


ALTER TABLE public.filesharedforgroup OWNER TO placity;

--
-- TOC entry 259 (class 1259 OID 26416)
-- Name: filesharedfororga; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE filesharedfororga (
    id_file integer NOT NULL,
    id_orga integer NOT NULL
);


ALTER TABLE public.filesharedfororga OWNER TO placity;

--
-- TOC entry 261 (class 1259 OID 26422)
-- Name: group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE "group" (
    id integer DEFAULT nextval('group_id_seq'::regclass) NOT NULL,
    groupname character varying(255) NOT NULL,
    group_admin integer NOT NULL,
    id_orga integer
);


ALTER TABLE public."group" OWNER TO placity;

--
-- TOC entry 260 (class 1259 OID 26419)
-- Name: groupmember; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE groupmember (
    id_group integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.groupmember OWNER TO placity;

--
-- TOC entry 263 (class 1259 OID 26428)
-- Name: highscore; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE highscore (
    name text NOT NULL,
    id_route integer NOT NULL,
    points numeric(7,7) NOT NULL,
    date date DEFAULT now() NOT NULL
);


ALTER TABLE public.highscore OWNER TO placity;

--
-- TOC entry 289 (class 1259 OID 26782)
-- Name: mediafile; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE mediafile (
    id integer DEFAULT nextval('mediafile_id_seq'::regclass) NOT NULL,
    file_name character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    url character varying(512) NOT NULL,
    public integer,
    file_type character varying(255),
    filesize integer NOT NULL,
    id_user integer NOT NULL,
    encoding boolean DEFAULT false,
    encoded boolean DEFAULT false
);


ALTER TABLE public.mediafile OWNER TO placity;

--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 289
-- Name: COLUMN mediafile.encoding; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN mediafile.encoding IS 'true, solange Datei in kompatibles Format überführt wird. ';


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 289
-- Name: COLUMN mediafile.encoded; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN mediafile.encoded IS 'true, wenn Transkodierung abgeschlossen, oder Format erfolgrich auf Kompatibilität geprüft wurde. ';


--
-- TOC entry 265 (class 1259 OID 26440)
-- Name: one_time_access_code; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE one_time_access_code (
    code character varying(255) NOT NULL,
    id_orga integer NOT NULL,
    id_group integer NOT NULL
);


ALTER TABLE public.one_time_access_code OWNER TO placity;

--
-- TOC entry 266 (class 1259 OID 26443)
-- Name: orgamember; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE orgamember (
    id_orga integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.orgamember OWNER TO placity;

--
-- TOC entry 267 (class 1259 OID 26446)
-- Name: organization; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE organization (
    id integer DEFAULT nextval('organization_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(4096),
    orga_admin integer NOT NULL
);


ALTER TABLE public.organization OWNER TO placity;

--
-- TOC entry 269 (class 1259 OID 26455)
-- Name: page; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE page (
    id integer DEFAULT nextval('page_id_seq'::regclass) NOT NULL,
    next integer,
    page_name character varying(255) NOT NULL,
    pos integer,
    id_route integer
);


ALTER TABLE public.page OWNER TO placity;

--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 269
-- Name: TABLE page; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON TABLE page IS 'Page ist eine Seite einer bestimmten Route. Eine Page enthält verschiedene Instanzen vom Content ';


--
-- TOC entry 271 (class 1259 OID 26461)
-- Name: player; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE player (
    name character varying(255) NOT NULL,
    avatar_img character varying(255)
);


ALTER TABLE public.player OWNER TO placity;

--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 271
-- Name: COLUMN player.avatar_img; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN player.avatar_img IS 'base64';


--
-- TOC entry 272 (class 1259 OID 26467)
-- Name: route; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE route (
    id integer DEFAULT nextval('route_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    title character varying(255),
    subtitle character varying(30),
    description character varying(255),
    image text,
    app_scheme character varying(255) DEFAULT 'APP_SCHEME'::character varying NOT NULL,
    android_scheme character varying(255) NOT NULL,
    win_scheme character varying(255),
    private character(1) NOT NULL,
    id_orga integer,
    id_user integer NOT NULL,
    id_group integer
);


ALTER TABLE public.route OWNER TO placity;

CREATE TABLE routeinfo (
	id integer DEFAULT nextval('routemodel_id_seq'::regclass) NOT NULL,
	id_route integer Not NULL,
    name character varying(255) NOT NULL,
    title character varying(255),
    subtitle character varying(30),
    description character varying(255),
    image text,
	lang character varying(255) DEFAULT 'en_EN',
	CONSTRAINT rdata_pkey PRIMARY KEY (id)
);
ALTER TABLE ONLY routeinfo ADD Constraint route FOREIGN KEY (id_route) REFERENCES route(id);
 
--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 272
-- Name: COLUMN route.image; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.image IS 'encoded in base64';


--
-- TOC entry 2456 (class 0 OID 0)
-- Dependencies: 272
-- Name: COLUMN route.app_scheme; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.app_scheme IS 'todo: App_Scheme sec. ID ';


--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 272
-- Name: COLUMN route.android_scheme; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.android_scheme IS 'todo: ANDROID_SCHEME sec. ID';


--
-- TOC entry 274 (class 1259 OID 26477)
-- Name: routentag; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE routentag (
    id_route integer NOT NULL,
    tag_name character varying(255) NOT NULL
);


ALTER TABLE public.routentag OWNER TO placity;

--
-- TOC entry 275 (class 1259 OID 26480)
-- Name: tag; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE tag (
    tag_name character varying(255) NOT NULL
);


ALTER TABLE public.tag OWNER TO placity;

--
-- TOC entry 2297 (class 2606 OID 26632)
-- Name: content_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY content
    ADD CONSTRAINT content_pkey PRIMARY KEY (id);


--
-- TOC entry 2265 (class 2606 OID 26490)
-- Name: contenttype_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT contenttype_pkey PRIMARY KEY (id);


--
-- TOC entry 2263 (class 2606 OID 25940)
-- Name: file_service_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY file_service_config
    ADD CONSTRAINT file_service_config_pkey PRIMARY KEY (service_id);


--
-- TOC entry 2269 (class 2606 OID 26492)
-- Name: filesharedforgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT filesharedforgroup_pkey PRIMARY KEY (id_file, id_group);


--
-- TOC entry 2271 (class 2606 OID 26494)
-- Name: filesharedfororga_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT filesharedfororga_pkey PRIMARY KEY (id_file, id_orga);


--
-- TOC entry 2285 (class 2606 OID 26486)
-- Name: fksuccessor_id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT fksuccessor_id UNIQUE (next);


--
-- TOC entry 2275 (class 2606 OID 26498)
-- Name: group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- TOC entry 2273 (class 2606 OID 26496)
-- Name: groupmember_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT groupmember_pkey PRIMARY KEY (id_group, id_user);


--
-- TOC entry 2277 (class 2606 OID 26859)
-- Name: highscore_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT highscore_pkey PRIMARY KEY (name, id_route);


--
-- TOC entry 2299 (class 2606 OID 26790)
-- Name: id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- TOC entry 2301 (class 2606 OID 26792)
-- Name: mediafile_file_name_key; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT mediafile_file_name_key UNIQUE (file_name);


--
-- TOC entry 2279 (class 2606 OID 26504)
-- Name: one_time_access_code_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY one_time_access_code
    ADD CONSTRAINT one_time_access_code_pkey PRIMARY KEY (code);


--
-- TOC entry 2281 (class 2606 OID 26506)
-- Name: orgamember_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT orgamember_pkey PRIMARY KEY (id_orga, id_user);


--
-- TOC entry 2283 (class 2606 OID 26508)
-- Name: organization_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);


--
-- TOC entry 2287 (class 2606 OID 26510)
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- TOC entry 2289 (class 2606 OID 26512)
-- Name: player_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY player
    ADD CONSTRAINT player_pkey PRIMARY KEY (name);


--
-- TOC entry 2291 (class 2606 OID 26514)
-- Name: route_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_pkey PRIMARY KEY (id);


--
-- TOC entry 2293 (class 2606 OID 26516)
-- Name: routentag_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT routentag_pkey PRIMARY KEY (id_route, tag_name);


--
-- TOC entry 2267 (class 2606 OID 26815)
-- Name: sid; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT sid UNIQUE (sid);


--
-- TOC entry 2295 (class 2606 OID 26518)
-- Name: tag_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (tag_name);


--
-- TOC entry 2317 (class 2606 OID 26633)
-- Name: contenttype_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY content
    ADD CONSTRAINT contenttype_fk FOREIGN KEY (id_content_type) REFERENCES contenttype(id);


--
-- TOC entry 2302 (class 2606 OID 25934)
-- Name: file_service_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY file_service_config
    ADD CONSTRAINT file_service_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- TOC entry 2309 (class 2606 OID 26521)
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY one_time_access_code
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- TOC entry 2303 (class 2606 OID 26526)
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- TOC entry 2305 (class 2606 OID 26546)
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- TOC entry 2314 (class 2606 OID 26591)
-- Name: name_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT name_fk FOREIGN KEY (tag_name) REFERENCES tag(tag_name);


--
-- TOC entry 2307 (class 2606 OID 26860)
-- Name: name_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT name_fk FOREIGN KEY (name) REFERENCES player(name);


--
-- TOC entry 2304 (class 2606 OID 26541)
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- TOC entry 2310 (class 2606 OID 26571)
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- TOC entry 2313 (class 2606 OID 26576)
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY route
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- TOC entry 2306 (class 2606 OID 26662)
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- TOC entry 2316 (class 2606 OID 26638)
-- Name: page_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY content
    ADD CONSTRAINT page_fk FOREIGN KEY (id_page) REFERENCES page(id);


--
-- TOC entry 2308 (class 2606 OID 26556)
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- TOC entry 2315 (class 2606 OID 26586)
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- TOC entry 2312 (class 2606 OID 26596)
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY page
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- TOC entry 2311 (class 2606 OID 26601)
-- Name: successor_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY page
    ADD CONSTRAINT successor_fk FOREIGN KEY (next) REFERENCES page(id);


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 289
-- Name: mediafile; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE mediafile FROM PUBLIC;
REVOKE ALL ON TABLE mediafile FROM placity;
GRANT ALL ON TABLE mediafile TO placity;
GRANT ALL ON TABLE mediafile TO PUBLIC;


-- Completed on 2016-02-16 22:16:04

--
-- PostgreSQL database dump complete
--

-- ----------------------------------------------------------------------------------
-- DATA DATA DATA 
-------------------------------------------------------------------------------------


INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (1, 'ct_text', 'ContentType Text: Instanz des ct_text,  ist ein einfacher Fließtext in einer Route', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "text"
              ],
              "properties": {
                "text": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctTextAppCtrl.js', 'ctTextAppTpl.html', 'ctTextQECtrl.js', 'ctTextQETpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (4, 'headline', 'Überschrift ', NULL, 'ctHeadlineAppCtrl.js', 'ctHeadlineAppTpl.html', 'ctHeadlineQECtrl.js', 'ctHeadlineQETmpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (2, 'ct_MultipleChoice', 'ContentType MultipleChoice: Instanz des ct_MultipleChoice, ist eine Frage mit mehreren Antwortmöglichkeiten', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "text",
                "answer",
                "cmessage",
                "wmessage"
              ],
              "properties": {
                "text": {
                  "type": "string",
                  "minLength": 1
                },
                "answer": {
                  "type": "string",
                  "minLength": 1
                },
                "choice": {
                  "type": "array",
                  "items": {
                    "required": [],
                    "properties": {}
                  }
                },
                "cmessage": {
                  "type": "string",
                  "minLength": 1
                },
                "wmessage": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          }
        }
      }
    },
    "points": {
      "type": "number"
    }
  },
  "required": [
    "languages",
    "points"
  ]
}
', 'ctMultipleChoice.js', 'ctMultipleChoice.html', 'ctMultipleChoiceQE.js', 'ctMultipleChoiceQE.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (3, 'ct_Dialog', 'ContentType Dialog: Instanz des ct_Dialog, ist eine Unterhaltung zwischen 2 Personen', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "avatar1",
                "avatar2"
              ],
              "properties": {
                "avatar1": {
                  "type": "string",
                  "minLength": 1
                },
                "avatar2": {
                  "type": "string",
                  "minLength": 1
                },
                "text": {
                  "type": "array",
                  "items": {
                    "required": [],
                    "properties": {}
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctDialog.js', 'ctDialog.html', 'ctDialogQE.js', 'ctDialogQE.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (5, 'ct_FrageFreiText', 'ContentType FrageFreitext: Textuelle Frage, dessen Antwort frei formuliert wird.', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang",
          "cmessage",
          "wmessage"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "text",
                "answer"
              ],
              "properties": {
                "text": {
                  "type": "string",
                  "minLength": 1
                },
                "answer": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          },
          "cmessage": {
            "type": "string",
            "minLength": 1
          },
          "wmessage": {
            "type": "string",
            "minLength": 1
          }
        }
      }
    },
    "points": {
      "type": "number"
    }
  },
  "required": [
    "languages",
    "points"
  ]
}
', 'ctFrageFreitext.js', 'ctFrageFreitext.html', 'ctFrageFreitextQETmpl.js', 'ctFrageFreitextQETmpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (6, 'ct_Audio', 'ContentType Audio: Audio Inhalt', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "id",
                "audiofileurl",
                "info",
                "autoplay"
              ],
              "properties": {
                "id": {
                  "type": "string",
                  "minLength": 1
                },
                "audiofileurl": {
                  "type": "string",
                  "minLength": 1
                },
                "info": {
                  "type": "string",
                  "minLength": 1
                },
                "autoplay": {
                  "type": "boolean"
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctAudio.js', 'ctAudioApp.html', 'ctAudioQETmpl.js', 'ctAudioQETmpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (7, 'ct_Video', 'ContentType Video: Video Inhalt', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "contenttype",
                "src",
                "description",
                "autoplay"
              ],
              "properties": {
                "contenttype": {
                  "type": "string",
                  "minLength": 1
                },
                "src": {
                  "type": "string",
                  "minLength": 1
                },
                "description": {
                  "type": "string",
                  "minLength": 1
                },
                "autoplay": {
                  "type": "boolean"
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctVideo.js', 'ctVideo.html', 'ctVideoQETmpl.js', 'ctVideoQETml.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (8, 'ct_Image', 'ContentType Image: Bild Inhalt', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "src",
                "alt"
              ],
              "properties": {
                "src": {
                  "type": "string",
                  "minLength": 1
                },
                "alt": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctImage.js', 'ctImage.html', 'ctImageQETmpl.js', 'ctImageQETmpl.html', NULL, NULL);

ALTER TABLE ONLY address
    ADD CONSTRAINT orga_id PRIMARY KEY (id_orga);