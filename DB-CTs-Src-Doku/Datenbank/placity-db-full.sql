--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: placity; Type: COMMENT; Schema: -; Owner: placity
--

COMMENT ON DATABASE placity IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: func_leave_group(integer, integer); Type: FUNCTION; Schema: public; Owner: placity
--

CREATE FUNCTION func_leave_group(id_user_in integer, id_group_in integer) RETURNS void
    LANGUAGE sql
    AS $$delete from groupmember where id_user=id_user_in and id_group=id_group_in$$;


ALTER FUNCTION public.func_leave_group(id_user_in integer, id_group_in integer) OWNER TO placity;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: address; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE address (
    id_orga integer NOT NULL,
    street character varying(255) NOT NULL,
    postcode numeric(5,0) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL
);


ALTER TABLE public.address OWNER TO placity;

--
-- Name: app; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    api_key character varying(255),
    description character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    type integer DEFAULT 0 NOT NULL,
    path text,
    url text,
    storage_service_id integer,
    storage_container character varying(255),
    requires_fullscreen boolean DEFAULT false NOT NULL,
    allow_fullscreen_toggle boolean DEFAULT true NOT NULL,
    toggle_location character varying(64) DEFAULT 'top'::character varying NOT NULL,
    role_id integer,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.app OWNER TO placity;

--
-- Name: app_group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app_group (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255),
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.app_group OWNER TO placity;

--
-- Name: app_group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_group_id_seq OWNER TO placity;

--
-- Name: app_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_group_id_seq OWNED BY app_group.id;


--
-- Name: app_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_id_seq OWNER TO placity;

--
-- Name: app_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_id_seq OWNED BY app.id;


--
-- Name: app_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app_lookup (
    id integer NOT NULL,
    app_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.app_lookup OWNER TO placity;

--
-- Name: app_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_lookup_id_seq OWNER TO placity;

--
-- Name: app_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_lookup_id_seq OWNED BY app_lookup.id;


--
-- Name: app_to_app_group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app_to_app_group (
    id integer NOT NULL,
    app_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.app_to_app_group OWNER TO placity;

--
-- Name: app_to_app_group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_to_app_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_to_app_group_id_seq OWNER TO placity;

--
-- Name: app_to_app_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_to_app_group_id_seq OWNED BY app_to_app_group.id;


--
-- Name: aws_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE aws_config (
    service_id integer NOT NULL,
    key text,
    secret text,
    region character varying(255)
);


ALTER TABLE public.aws_config OWNER TO placity;

--
-- Name: c_group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE c_group_id_seq
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.c_group_id_seq OWNER TO placity;

--
-- Name: c_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE c_id_seq
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.c_id_seq OWNER TO placity;

--
-- Name: cloud_email_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE cloud_email_config (
    service_id integer NOT NULL,
    domain character varying(255),
    key text NOT NULL
);


ALTER TABLE public.cloud_email_config OWNER TO placity;

--
-- Name: contact; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contact (
    id integer NOT NULL,
    first_name character varying(40) NOT NULL,
    last_name character varying(40) NOT NULL,
    image_url text,
    twitter character varying(18),
    skype character varying(255),
    notes text
);


ALTER TABLE public.contact OWNER TO placity;

--
-- Name: contact_group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contact_group (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE public.contact_group OWNER TO placity;

--
-- Name: contact_group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE contact_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_group_id_seq OWNER TO placity;

--
-- Name: contact_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE contact_group_id_seq OWNED BY contact_group.id;


--
-- Name: contact_group_relationship; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contact_group_relationship (
    id integer NOT NULL,
    contact_id integer NOT NULL,
    contact_group_id integer NOT NULL
);


ALTER TABLE public.contact_group_relationship OWNER TO placity;

--
-- Name: contact_group_relationship_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE contact_group_relationship_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_group_relationship_id_seq OWNER TO placity;

--
-- Name: contact_group_relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE contact_group_relationship_id_seq OWNED BY contact_group_relationship.id;


--
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_id_seq OWNER TO placity;

--
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE contact_id_seq OWNED BY contact.id;


--
-- Name: contact_info; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contact_info (
    id integer NOT NULL,
    ordinal integer,
    contact_id integer NOT NULL,
    info_type character varying(32) NOT NULL,
    phone character varying(32) NOT NULL,
    email character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    city character varying(64) NOT NULL,
    state character varying(64) NOT NULL,
    zip character varying(16) NOT NULL,
    country character varying(64) NOT NULL
);


ALTER TABLE public.contact_info OWNER TO placity;

--
-- Name: contact_info_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE contact_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_info_id_seq OWNER TO placity;

--
-- Name: contact_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE contact_info_id_seq OWNED BY contact_info.id;


--
-- Name: content; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE content (
    id integer DEFAULT nextval('c_id_seq'::regclass) NOT NULL,
    id_page integer,
    id_content_type integer NOT NULL,
    data_obj json,
    pos integer DEFAULT 0
);


ALTER TABLE public.content OWNER TO placity;

--
-- Name: COLUMN content.data_obj; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN content.data_obj IS '{
     "languages": 
     [
         {"lang": "de_DE", "fields": [ "key":"value", "key":"value" , ... ],
         {"lang": "en_EN", "fields": [ "key":"value", "key":"value" , ... ]
     ]
}';


--
-- Name: content_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE content_id_seq
    START WITH 2
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.content_id_seq OWNER TO placity;

--
-- Name: ct_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE ct_id_seq
    START WITH 2
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ct_id_seq OWNER TO placity;

--
-- Name: contenttype; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contenttype (
    id integer DEFAULT nextval('ct_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    description text,
    data_schema json,
    app_ctrl_class character varying(255) NOT NULL,
    app_html_tpl_file character varying(255) NOT NULL,
    qe_ctrl_class character varying(255) NOT NULL,
    qe_html_tpl_file character varying(255) NOT NULL,
    sid integer,
    helptext json
);


ALTER TABLE public.contenttype OWNER TO placity;

--
-- Name: COLUMN contenttype.name; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.name IS 'Name der Instanz des Entitytypen ContentType. Eine Entity des Entitytypen ContentType klassifiziert eine Gruppe von gleich strukturierten Contentinstanzen; ähnlich einer Klassendefinition in objektorientierten Programmiersprachen.   ';


--
-- Name: COLUMN contenttype.description; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.description IS 'Multiligualität fehlt hier. ';


--
-- Name: COLUMN contenttype.app_ctrl_class; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.app_ctrl_class IS 'Wiedergabe Controller AngularJS';


--
-- Name: COLUMN contenttype.app_html_tpl_file; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.app_html_tpl_file IS 'Wiedergabe Struktur';


--
-- Name: COLUMN contenttype.qe_ctrl_class; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.qe_ctrl_class IS 'Eingabeformular Controller AngularJS ';


--
-- Name: COLUMN contenttype.qe_html_tpl_file; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.qe_html_tpl_file IS 'Eingabeformular';


--
-- Name: COLUMN contenttype.helptext; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.helptext IS '{
"languages": 
   [ 
      {"lang":"de_DE", "val":"Hilfetext"},
      {"lang":"de_EN", "val":"help text"} 
   ]
} ';


--
-- Name: cors_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE cors_config (
    id integer NOT NULL,
    path character varying(255) NOT NULL,
    origin character varying(255) NOT NULL,
    header text NOT NULL,
    method integer DEFAULT 0 NOT NULL,
    max_age integer DEFAULT 3600 NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.cors_config OWNER TO placity;

--
-- Name: cors_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE cors_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cors_config_id_seq OWNER TO placity;

--
-- Name: cors_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE cors_config_id_seq OWNED BY cors_config.id;


--
-- Name: couchdb_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE couchdb_config (
    service_id integer NOT NULL,
    dsn character varying(255) DEFAULT '0'::character varying,
    options text
);


ALTER TABLE public.couchdb_config OWNER TO placity;

--
-- Name: db_field_extras; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE db_field_extras (
    id integer NOT NULL,
    service_id integer,
    "table" character varying(255) NOT NULL,
    field character varying(255) NOT NULL,
    label character varying(255),
    extra_type character varying(255),
    description text,
    picklist text,
    validation text,
    client_info text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer,
    alias character varying(255),
    db_function text,
    ref_service_id integer,
    ref_table character varying(255),
    ref_fields character varying(255),
    ref_on_update character varying(255),
    ref_on_delete character varying(255)
);


ALTER TABLE public.db_field_extras OWNER TO placity;

--
-- Name: db_field_extras_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE db_field_extras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.db_field_extras_id_seq OWNER TO placity;

--
-- Name: db_field_extras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE db_field_extras_id_seq OWNED BY db_field_extras.id;


--
-- Name: db_relationship_extras; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE db_relationship_extras (
    id integer NOT NULL,
    service_id integer,
    "table" character varying(255) NOT NULL,
    relationship character varying(255) NOT NULL,
    alias character varying(255),
    label character varying(255),
    description text,
    always_fetch boolean DEFAULT false NOT NULL,
    flatten boolean DEFAULT false NOT NULL,
    flatten_drop_prefix boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.db_relationship_extras OWNER TO placity;

--
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE db_relationship_extras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.db_relationship_extras_id_seq OWNER TO placity;

--
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE db_relationship_extras_id_seq OWNED BY db_relationship_extras.id;


--
-- Name: db_table_extras; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE db_table_extras (
    id integer NOT NULL,
    service_id integer NOT NULL,
    "table" character varying(255) NOT NULL,
    label character varying(255),
    plural character varying(255),
    name_field character varying(128),
    model character varying(255),
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer,
    alias character varying(255)
);


ALTER TABLE public.db_table_extras OWNER TO placity;

--
-- Name: db_table_extras_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE db_table_extras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.db_table_extras_id_seq OWNER TO placity;

--
-- Name: db_table_extras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE db_table_extras_id_seq OWNED BY db_table_extras.id;


--
-- Name: email_parameters_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE email_parameters_config (
    id integer NOT NULL,
    service_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE public.email_parameters_config OWNER TO placity;

--
-- Name: email_parameters_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE email_parameters_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_parameters_config_id_seq OWNER TO placity;

--
-- Name: email_parameters_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE email_parameters_config_id_seq OWNED BY email_parameters_config.id;


--
-- Name: email_template; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE email_template (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255),
    "to" text,
    cc text,
    bcc text,
    subject character varying(80),
    body_text text,
    body_html text,
    from_name character varying(80),
    from_email character varying(255),
    reply_to_name character varying(80),
    reply_to_email character varying(255),
    defaults text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.email_template OWNER TO placity;

--
-- Name: email_template_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE email_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_template_id_seq OWNER TO placity;

--
-- Name: email_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE email_template_id_seq OWNED BY email_template.id;


--
-- Name: event_script; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE event_script (
    name character varying(80) NOT NULL,
    type character varying(40) NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    affects_process boolean DEFAULT false NOT NULL,
    content text,
    config text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.event_script OWNER TO placity;

--
-- Name: event_subscriber; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE event_subscriber (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    type character varying(255) NOT NULL,
    config text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.event_subscriber OWNER TO placity;

--
-- Name: event_subscriber_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE event_subscriber_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_subscriber_id_seq OWNER TO placity;

--
-- Name: event_subscriber_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE event_subscriber_id_seq OWNED BY event_subscriber.id;


--
-- Name: file_service_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE file_service_config (
    service_id integer NOT NULL,
    public_path text,
    container text
);


ALTER TABLE public.file_service_config OWNER TO placity;

--
-- Name: filesharedforgroup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE filesharedforgroup (
    id_group integer NOT NULL,
    id_file integer NOT NULL
);


ALTER TABLE public.filesharedforgroup OWNER TO placity;

--
-- Name: filesharedfororga; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE filesharedfororga (
    id_file integer NOT NULL,
    id_orga integer NOT NULL
);


ALTER TABLE public.filesharedfororga OWNER TO placity;

--
-- Name: group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_id_seq OWNER TO placity;

--
-- Name: group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE "group" (
    id integer DEFAULT nextval('group_id_seq'::regclass) NOT NULL,
    groupname character varying(255) NOT NULL,
    group_admin integer NOT NULL,
    id_orga integer
);


ALTER TABLE public."group" OWNER TO placity;

--
-- Name: groupmember; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE groupmember (
    id_group integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.groupmember OWNER TO placity;

--
-- Name: highscore; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE highscore (
    name text NOT NULL,
    id_route integer NOT NULL,
    points numeric(7,7) NOT NULL,
    date date DEFAULT now() NOT NULL
);


ALTER TABLE public.highscore OWNER TO placity;

--
-- Name: ldap_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE ldap_config (
    service_id integer NOT NULL,
    default_role integer NOT NULL,
    host character varying(255) NOT NULL,
    base_dn character varying(255) NOT NULL,
    account_suffix character varying(255),
    map_group_to_role boolean DEFAULT false NOT NULL,
    username character varying(50),
    password text
);


ALTER TABLE public.ldap_config OWNER TO placity;

--
-- Name: mediafile_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE mediafile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mediafile_id_seq OWNER TO placity;

--
-- Name: mediafile; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE mediafile (
    id integer DEFAULT nextval('mediafile_id_seq'::regclass) NOT NULL,
    file_name character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    url character varying(512) NOT NULL,
    public integer,
    file_type character varying(255),
    filesize integer NOT NULL,
    id_user integer NOT NULL,
    encoding boolean DEFAULT false,
    encoded boolean DEFAULT false
);


ALTER TABLE public.mediafile OWNER TO placity;

--
-- Name: COLUMN mediafile.encoding; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN mediafile.encoding IS 'true, solange Datei in kompatibles Format überführt wird. ';


--
-- Name: COLUMN mediafile.encoded; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN mediafile.encoded IS 'true, wenn Transkodierung abgeschlossen, oder Format erfolgrich auf Kompatibilität geprüft wurde. ';


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO placity;

--
-- Name: mongodb_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE mongodb_config (
    service_id integer NOT NULL,
    dsn character varying(255) DEFAULT '0'::character varying,
    options text,
    driver_options text
);


ALTER TABLE public.mongodb_config OWNER TO placity;

--
-- Name: oauth_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE oauth_config (
    service_id integer NOT NULL,
    default_role integer NOT NULL,
    client_id character varying(255) NOT NULL,
    client_secret text NOT NULL,
    redirect_url character varying(255) NOT NULL,
    icon_class character varying(255)
);


ALTER TABLE public.oauth_config OWNER TO placity;

--
-- Name: one_time_access_code; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE one_time_access_code (
    code character varying(255) NOT NULL,
    id_orga integer NOT NULL,
    id_group integer NOT NULL
);


ALTER TABLE public.one_time_access_code OWNER TO placity;

--
-- Name: orgamember; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE orgamember (
    id_orga integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.orgamember OWNER TO placity;

--
-- Name: organization_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE organization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organization_id_seq OWNER TO placity;

--
-- Name: organization; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE organization (
    id integer DEFAULT nextval('organization_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(4096),
    orga_admin integer NOT NULL
);


ALTER TABLE public.organization OWNER TO placity;

--
-- Name: page_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_id_seq OWNER TO placity;

--
-- Name: page; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE page (
    id integer DEFAULT nextval('page_id_seq'::regclass) NOT NULL,
    next integer,
    page_name character varying(255) NOT NULL,
    pos integer,
    id_route integer
);


ALTER TABLE public.page OWNER TO placity;

--
-- Name: TABLE page; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON TABLE page IS 'Page ist eine Seite einer bestimmten Route. Eine Page enthält verschiedene Instanzen vom Content ';


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.password_resets OWNER TO placity;

--
-- Name: player; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE player (
    name character varying(255) NOT NULL,
    avatar_img character varying(255)
);


ALTER TABLE public.player OWNER TO placity;

--
-- Name: COLUMN player.avatar_img; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN player.avatar_img IS 'base64';


--
-- Name: rackspace_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rackspace_config (
    service_id integer NOT NULL,
    username character varying(255),
    password text,
    tenant_name character varying(255),
    api_key text,
    url character varying(255),
    region character varying(255),
    storage_type character varying(255)
);


ALTER TABLE public.rackspace_config OWNER TO placity;

--
-- Name: role; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.role OWNER TO placity;

--
-- Name: role_adldap; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role_adldap (
    role_id integer NOT NULL,
    dn character varying(255) NOT NULL
);


ALTER TABLE public.role_adldap OWNER TO placity;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO placity;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: role_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role_lookup (
    id integer NOT NULL,
    role_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.role_lookup OWNER TO placity;

--
-- Name: role_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE role_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_lookup_id_seq OWNER TO placity;

--
-- Name: role_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE role_lookup_id_seq OWNED BY role_lookup.id;


--
-- Name: role_service_access; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role_service_access (
    id integer NOT NULL,
    role_id integer NOT NULL,
    service_id integer,
    component character varying(255),
    verb_mask integer DEFAULT 0 NOT NULL,
    requestor_mask integer DEFAULT 0 NOT NULL,
    filters text,
    filter_op character varying(32) DEFAULT 'and'::character varying NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.role_service_access OWNER TO placity;

--
-- Name: role_service_access_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE role_service_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_service_access_id_seq OWNER TO placity;

--
-- Name: role_service_access_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE role_service_access_id_seq OWNED BY role_service_access.id;


--
-- Name: route_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE route_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.route_id_seq OWNER TO placity;

--
-- Name: route; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE route (
    id integer DEFAULT nextval('route_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    title character varying(255),
    subtitle character varying(30),
    description character varying(255),
    image text,
    app_scheme character varying(255) DEFAULT 'APP_SCHEME'::character varying NOT NULL,
    android_scheme character varying(255) NOT NULL,
    win_scheme character varying(255),
    private character(1) NOT NULL,
    id_orga integer,
    id_user integer NOT NULL,
    id_group integer
);


ALTER TABLE public.route OWNER TO placity;

--
-- Name: COLUMN route.image; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.image IS 'encoded in base64';


--
-- Name: COLUMN route.app_scheme; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.app_scheme IS 'todo: App_Scheme sec. ID ';


--
-- Name: COLUMN route.android_scheme; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.android_scheme IS 'todo: ANDROID_SCHEME sec. ID';


--
-- Name: routemodel_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE routemodel_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.routemodel_id_seq OWNER TO placity;

--
-- Name: routemodel; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE routemodel (
    id integer DEFAULT nextval('routemodel_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    title text,
    subtitle text,
    description text,
    lang text,
    id_route integer NOT NULL
);


ALTER TABLE public.routemodel OWNER TO placity;

--
-- Name: routentag; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE routentag (
    id_route integer NOT NULL,
    tag_name character varying(255) NOT NULL
);


ALTER TABLE public.routentag OWNER TO placity;

--
-- Name: rws_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rws_config (
    service_id integer NOT NULL,
    base_url text NOT NULL
);


ALTER TABLE public.rws_config OWNER TO placity;

--
-- Name: rws_headers_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rws_headers_config (
    id integer NOT NULL,
    service_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    pass_from_client boolean DEFAULT false NOT NULL,
    action integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.rws_headers_config OWNER TO placity;

--
-- Name: rws_headers_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE rws_headers_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rws_headers_config_id_seq OWNER TO placity;

--
-- Name: rws_headers_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE rws_headers_config_id_seq OWNED BY rws_headers_config.id;


--
-- Name: rws_parameters_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rws_parameters_config (
    id integer NOT NULL,
    service_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    exclude boolean DEFAULT false NOT NULL,
    outbound boolean DEFAULT false NOT NULL,
    cache_key boolean DEFAULT false NOT NULL,
    action integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.rws_parameters_config OWNER TO placity;

--
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE rws_parameters_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rws_parameters_config_id_seq OWNER TO placity;

--
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE rws_parameters_config_id_seq OWNED BY rws_parameters_config.id;


--
-- Name: salesforce_db_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE salesforce_db_config (
    service_id integer NOT NULL,
    username character varying(255),
    password text,
    security_token text
);


ALTER TABLE public.salesforce_db_config OWNER TO placity;

--
-- Name: script_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE script_config (
    service_id integer NOT NULL,
    type character varying(40) NOT NULL,
    content text,
    config text
);


ALTER TABLE public.script_config OWNER TO placity;

--
-- Name: script_type; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE script_type (
    name character varying(40) NOT NULL,
    class_name character varying(255) NOT NULL,
    label character varying(80) NOT NULL,
    description character varying(255),
    sandboxed boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.script_type OWNER TO placity;

--
-- Name: service; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service (
    id integer NOT NULL,
    name character varying(40) NOT NULL,
    label character varying(80) NOT NULL,
    description character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    type character varying(40) NOT NULL,
    mutable boolean DEFAULT true NOT NULL,
    deletable boolean DEFAULT true NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.service OWNER TO placity;

--
-- Name: service_cache_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service_cache_config (
    service_id integer NOT NULL,
    cache_enabled boolean DEFAULT false NOT NULL,
    cache_ttl integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.service_cache_config OWNER TO placity;

--
-- Name: service_doc; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service_doc (
    id integer NOT NULL,
    service_id integer NOT NULL,
    format integer DEFAULT 0 NOT NULL,
    content text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.service_doc OWNER TO placity;

--
-- Name: service_doc_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE service_doc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_doc_id_seq OWNER TO placity;

--
-- Name: service_doc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE service_doc_id_seq OWNED BY service_doc.id;


--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_id_seq OWNER TO placity;

--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE service_id_seq OWNED BY service.id;


--
-- Name: service_type; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service_type (
    name character varying(40) NOT NULL,
    class_name character varying(255) NOT NULL,
    config_handler character varying(255),
    label character varying(80) NOT NULL,
    description character varying(255),
    "group" character varying(255),
    singleton boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.service_type OWNER TO placity;

--
-- Name: smtp_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE smtp_config (
    service_id integer NOT NULL,
    host character varying(255) NOT NULL,
    port character varying(255) DEFAULT '587'::character varying NOT NULL,
    encryption character varying(255) DEFAULT 'tls'::character varying NOT NULL,
    username text NOT NULL,
    password text NOT NULL
);


ALTER TABLE public.smtp_config OWNER TO placity;

--
-- Name: soap_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE soap_config (
    service_id integer NOT NULL,
    wsdl character varying(255) DEFAULT '0'::character varying,
    options text
);


ALTER TABLE public.soap_config OWNER TO placity;

--
-- Name: sql_db_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE sql_db_config (
    service_id integer NOT NULL,
    driver character varying(32) NOT NULL,
    dsn character varying(255) NOT NULL,
    username text,
    password text,
    options text,
    attributes text,
    default_schema_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sql_db_config OWNER TO placity;

--
-- Name: system_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_config (
    db_version character varying(32) NOT NULL,
    login_with_user_name boolean DEFAULT false NOT NULL,
    default_app_id integer,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_config OWNER TO placity;

--
-- Name: system_custom; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_custom (
    name character varying(255) NOT NULL,
    value text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_custom OWNER TO placity;

--
-- Name: system_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_lookup (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_lookup OWNER TO placity;

--
-- Name: system_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE system_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_lookup_id_seq OWNER TO placity;

--
-- Name: system_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE system_lookup_id_seq OWNED BY system_lookup.id;


--
-- Name: system_resource; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_resource (
    name character varying(40) NOT NULL,
    class_name character varying(255) NOT NULL,
    label character varying(80) NOT NULL,
    description character varying(255),
    model_name character varying(255),
    singleton boolean DEFAULT false NOT NULL,
    read_only boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.system_resource OWNER TO placity;

--
-- Name: system_setting; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_setting (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_setting OWNER TO placity;

--
-- Name: system_setting_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE system_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_setting_id_seq OWNER TO placity;

--
-- Name: system_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE system_setting_id_seq OWNED BY system_setting.id;


--
-- Name: tag; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE tag (
    tag_name character varying(255) NOT NULL
);


ALTER TABLE public.tag OWNER TO placity;

--
-- Name: test; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW test AS
 SELECT route.id,
    route.name,
    route.title,
    route.subtitle,
    route.description,
    route.image,
    route.app_scheme,
    route.android_scheme,
    route.win_scheme,
    route.private,
    route.id_orga,
    route.id_user,
    route.id_group,
    rt.array_pages_json
   FROM route,
    ( SELECT p.id_route,
            json_agg(p.page_json) AS array_pages_json
           FROM ( SELECT pg.id_route,
                    row_to_json(pg.*) AS page_json
                   FROM ( SELECT page.id,
                            page.next,
                            page.page_name,
                            page.pos,
                            page.id_route,
                            js.array_content_json
                           FROM (page
                             JOIN ( SELECT c.id_page,
                                    json_agg(c.content_json) AS array_content_json
                                   FROM ( SELECT content.id_page,
    row_to_json(content.*, true) AS content_json
   FROM content,
    page page_1
  WHERE (content.id_page = page_1.id)) c
                                  GROUP BY c.id_page) js ON ((js.id_page = page.id)))) pg,
                    route route_1
                  WHERE (pg.id_route = route_1.id)) p
          GROUP BY p.id_route) rt
  WHERE (rt.id_route = route.id);


ALTER TABLE public.test OWNER TO placity;

--
-- Name: token_map; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE token_map (
    user_id integer NOT NULL,
    token text NOT NULL,
    iat integer NOT NULL,
    exp integer NOT NULL
);


ALTER TABLE public.token_map OWNER TO placity;

--
-- Name: user; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    last_login_date timestamp(0) without time zone,
    email character varying(255) NOT NULL,
    password text,
    is_sys_admin boolean DEFAULT false NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    phone character varying(32),
    security_question character varying(255),
    security_answer text,
    confirm_code character varying(255),
    default_app_id integer,
    remember_token character varying(100),
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer,
    oauth_provider character varying(50),
    adldap character varying(50)
);


ALTER TABLE public."user" OWNER TO placity;

--
-- Name: user_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_config (
    service_id integer NOT NULL,
    allow_open_registration boolean DEFAULT false NOT NULL,
    open_reg_role_id integer,
    open_reg_email_service_id integer,
    open_reg_email_template_id integer,
    invite_email_service_id integer,
    invite_email_template_id integer,
    password_email_service_id integer,
    password_email_template_id integer
);


ALTER TABLE public.user_config OWNER TO placity;

--
-- Name: user_custom; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_custom (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.user_custom OWNER TO placity;

--
-- Name: user_custom_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_custom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_custom_id_seq OWNER TO placity;

--
-- Name: user_custom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_custom_id_seq OWNED BY user_custom.id;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO placity;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_lookup (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.user_lookup OWNER TO placity;

--
-- Name: user_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_lookup_id_seq OWNER TO placity;

--
-- Name: user_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_lookup_id_seq OWNED BY user_lookup.id;


--
-- Name: user_to_app_to_role; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_to_app_to_role (
    id integer NOT NULL,
    user_id integer NOT NULL,
    app_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.user_to_app_to_role OWNER TO placity;

--
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_to_app_to_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_to_app_to_role_id_seq OWNER TO placity;

--
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_to_app_to_role_id_seq OWNED BY user_to_app_to_role.id;


--
-- Name: v_content_page; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_content_page AS
 SELECT page.id,
    page.next,
    page.page_name,
    page.pos,
    page.id_route,
    js.json_agg
   FROM (page
     JOIN ( SELECT c.id_page,
            json_agg(c.row_to_json) AS json_agg
           FROM ( SELECT content.id_page,
                    row_to_json(content.*, true) AS row_to_json
                   FROM content,
                    page page_1
                  WHERE (content.id_page = page_1.id)) c
          GROUP BY c.id_page) js ON ((js.id_page = page.id)));


ALTER TABLE public.v_content_page OWNER TO placity;

--
-- Name: v_emails_group; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_emails_group AS
 SELECT groupmember.id_group,
    groupmember.id_user,
    "user".name,
    "user".email
   FROM (groupmember
     JOIN "user" ON ((groupmember.id_user = "user".id)));


ALTER TABLE public.v_emails_group OWNER TO placity;

--
-- Name: v_groupmember; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_groupmember AS
 SELECT "group".id AS group_pkey,
    "group".groupname,
    "user".id,
    "user".name,
    "user".first_name,
    "user".last_name,
    "user".email,
    "user".is_sys_admin,
    admin.id AS group_admin,
    admin.name AS "grpAdmin_username",
    admin.first_name AS group_admin_fristname,
    admin.last_name AS group_admin_lastname,
    admin.email AS group_admin_email,
    admin.is_sys_admin AS is_group_admin_sys_admin,
    "group".id_orga,
    organization.name AS orga_name
   FROM groupmember,
    "group",
    "user",
    "user" admin,
    organization
  WHERE ((((groupmember.id_user = "user".id) AND ("group".id = groupmember.id_group)) AND ("group".group_admin = admin.id)) AND (organization.id = "group".id_orga));


ALTER TABLE public.v_groupmember OWNER TO placity;

--
-- Name: v_file_shared_with_group; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_file_shared_with_group AS
 SELECT mediafile.id AS file_id,
    mediafile.file_name,
    v_groupmember.id AS user_id,
    v_groupmember.group_pkey AS group_id,
    v_groupmember.id_orga AS orga_id
   FROM ((mediafile
     JOIN filesharedforgroup ON ((filesharedforgroup.id_file = mediafile.id)))
     JOIN v_groupmember ON ((v_groupmember.group_pkey = filesharedforgroup.id_group)));


ALTER TABLE public.v_file_shared_with_group OWNER TO placity;

--
-- Name: v_file_shared_with_orga; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_file_shared_with_orga AS
 SELECT mediafile.id AS file_id,
    mediafile.file_name,
    v_groupmember.id AS user_id,
    v_groupmember.group_pkey AS group_id,
    v_groupmember.id_orga AS orga_id
   FROM ((mediafile
     JOIN filesharedfororga ON ((filesharedfororga.id_file = mediafile.id)))
     JOIN v_groupmember ON ((v_groupmember.id_orga = filesharedfororga.id_orga)));


ALTER TABLE public.v_file_shared_with_orga OWNER TO placity;

--
-- Name: v_groupmember_orgaadmin; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_groupmember_orgaadmin AS
 SELECT "group".id AS group_pkey,
    "group".groupname,
    "user".id,
    "user".name,
    "user".first_name,
    "user".last_name,
    "user".email,
    "user".is_sys_admin,
    admin.id AS group_admin,
    admin.name AS "grpAdmin_username",
    admin.first_name AS group_admin_fristname,
    admin.last_name AS group_admin_lastname,
    admin.email AS group_admin_email,
    admin.is_sys_admin AS is_group_admin_sys_admin,
    "group".id_orga,
    organization.name AS orga_name,
    orga_admin.id AS orga_group_admin,
    orga_admin.name AS orga_admin_username,
    orga_admin.first_name AS orga_admin_fristname,
    orga_admin.last_name AS orga_admin_lastname,
    orga_admin.email AS orga_admin_email,
    orga_admin.is_sys_admin AS is_orga_admin_sys_admin
   FROM groupmember,
    "group",
    "user",
    "user" admin,
    organization,
    "user" orga_admin
  WHERE (((((groupmember.id_user = "user".id) AND ("group".id = groupmember.id_group)) AND ("group".group_admin = admin.id)) AND (organization.id = "group".id_orga)) AND (organization.orga_admin = orga_admin.id));


ALTER TABLE public.v_groupmember_orgaadmin OWNER TO placity;

--
-- Name: v_groups_in_orga; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_groups_in_orga AS
 SELECT organization.id AS orga_id,
    organization.name,
    organization.orga_admin,
    "group".groupname,
    "group".id AS group_id
   FROM (organization
     JOIN "group" ON (("group".id_orga = organization.id)));


ALTER TABLE public.v_groups_in_orga OWNER TO placity;

--
-- Name: v_orga; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_orga AS
 SELECT organization.id,
    organization.name,
    organization.description,
    organization.orga_admin,
    address.id_orga,
    address.street,
    address.postcode,
    address.city,
    address.country
   FROM organization,
    address
  WHERE (organization.id = address.id_orga);


ALTER TABLE public.v_orga OWNER TO placity;

--
-- Name: v_orgamember; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_orgamember AS
 SELECT organization.id AS orga_pkey,
    organization.name AS orga_name,
    organization.description,
    "user".id,
    "user".name,
    "user".first_name,
    "user".last_name,
    "user".email,
    "user".is_sys_admin,
    admin.id AS orga_admin,
    admin.name AS orga_admin_username,
    admin.first_name AS orga_admin_fristname,
    admin.last_name AS orga_admin_lastname,
    admin.email AS orga_admin_email,
    admin.is_sys_admin AS is_orga_admin_sys_admin
   FROM orgamember,
    organization,
    "user",
    "user" admin
  WHERE (((orgamember.id_user = "user".id) AND (organization.id = orgamember.id_orga)) AND (organization.orga_admin = admin.id));


ALTER TABLE public.v_orgamember OWNER TO placity;

--
-- Name: v_route_json; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_route_json AS
 SELECT rjson.id,
    rjson.name,
    rjson.title,
    rjson.subtitle,
    rjson.description,
    rjson.image,
    rjson.app_scheme,
    rjson.android_scheme,
    rjson.win_scheme,
    rjson.private,
    rjson.id_orga,
    rjson.id_user,
    rjson.id_group,
    row_to_json(rjson.*, true) AS routejson
   FROM ( SELECT route.id,
            route.name,
            route.title,
            route.subtitle,
            route.description,
            route.image,
            route.app_scheme,
            route.android_scheme,
            route.win_scheme,
            route.private,
            route.id_orga,
            route.id_user,
            route.id_group,
            rt.array_pages_json
           FROM route,
            ( SELECT p.id_route,
                    json_agg(p.page_json) AS array_pages_json
                   FROM ( SELECT pg.id_route,
                            row_to_json(pg.*, true) AS page_json
                           FROM ( SELECT page.id,
                                    page.next,
                                    page.page_name,
                                    page.pos,
                                    page.id_route,
                                    js.array_content_json
                                   FROM (page
                                     JOIN ( SELECT c.id_page,
    json_agg(c.content_json) AS array_content_json
   FROM ( SELECT content.id_page,
      row_to_json(content.*, true) AS content_json
     FROM content,
      page page_1
    WHERE (content.id_page = page_1.id)) c
  GROUP BY c.id_page) js ON ((js.id_page = page.id)))) pg,
                            route route_1
                          WHERE (pg.id_route = route_1.id)) p
                  GROUP BY p.id_route) rt
          WHERE (rt.id_route = route.id)) rjson;


ALTER TABLE public.v_route_json OWNER TO placity;

--
-- Name: v_route_page; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_route_page AS
 SELECT route.id,
    route.name,
    route.title,
    route.subtitle,
    route.description,
    route.image,
    route.app_scheme,
    route.android_scheme,
    route.win_scheme,
    route.private,
    route.id_orga,
    route.id_user,
    route.id_group,
    rt.array_pages
   FROM route,
    ( SELECT p.id_route,
            array_agg(p.page_json) AS array_pages
           FROM ( SELECT pg.id_route,
                    row_to_json(pg.*) AS page_json
                   FROM ( SELECT page.id,
                            page.next,
                            page.page_name,
                            page.pos,
                            page.id_route,
                            js.array_content_json
                           FROM (page
                             JOIN ( SELECT c.id_page,
                                    array_agg(c.content_json) AS array_content_json
                                   FROM ( SELECT content.id_page,
    row_to_json(content.*, true) AS content_json
   FROM content,
    page page_1
  WHERE (content.id_page = page_1.id)) c
                                  GROUP BY c.id_page) js ON ((js.id_page = page.id)))) pg,
                    route route_1
                  WHERE (pg.id_route = route_1.id)) p
          GROUP BY p.id_route) rt
  WHERE (rt.id_route = route.id);


ALTER TABLE public.v_route_page OWNER TO placity;

--
-- Name: v_route_page_content_tojson; Type: VIEW; Schema: public; Owner: placity
--

CREATE VIEW v_route_page_content_tojson AS
 SELECT route.id,
    route.name,
    route.title,
    route.subtitle,
    route.description,
    route.image,
    route.app_scheme,
    route.android_scheme,
    route.win_scheme,
    route.private,
    route.id_orga,
    route.id_user,
    route.id_group,
    rt.array_pages_json
   FROM route,
    ( SELECT p.id_route,
            json_agg(p.page_json) AS array_pages_json
           FROM ( SELECT pg.id_route,
                    row_to_json(pg.*) AS page_json
                   FROM ( SELECT page.id,
                            page.next,
                            page.page_name,
                            page.pos,
                            page.id_route,
                            js.array_content_json
                           FROM (page
                             JOIN ( SELECT c.id_page,
                                    json_agg(c.content_json) AS array_content_json
                                   FROM ( SELECT content.id_page,
    row_to_json(content.*, true) AS content_json
   FROM content,
    page page_1
  WHERE (content.id_page = page_1.id)) c
                                  GROUP BY c.id_page) js ON ((js.id_page = page.id)))) pg,
                    route route_1
                  WHERE (pg.id_route = route_1.id)) p
          GROUP BY p.id_route) rt
  WHERE (rt.id_route = route.id);


ALTER TABLE public.v_route_page_content_tojson OWNER TO placity;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app ALTER COLUMN id SET DEFAULT nextval('app_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_group ALTER COLUMN id SET DEFAULT nextval('app_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup ALTER COLUMN id SET DEFAULT nextval('app_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_to_app_group ALTER COLUMN id SET DEFAULT nextval('app_to_app_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact ALTER COLUMN id SET DEFAULT nextval('contact_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact_group ALTER COLUMN id SET DEFAULT nextval('contact_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact_group_relationship ALTER COLUMN id SET DEFAULT nextval('contact_group_relationship_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact_info ALTER COLUMN id SET DEFAULT nextval('contact_info_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cors_config ALTER COLUMN id SET DEFAULT nextval('cors_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras ALTER COLUMN id SET DEFAULT nextval('db_field_extras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras ALTER COLUMN id SET DEFAULT nextval('db_relationship_extras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras ALTER COLUMN id SET DEFAULT nextval('db_table_extras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_parameters_config ALTER COLUMN id SET DEFAULT nextval('email_parameters_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_template ALTER COLUMN id SET DEFAULT nextval('email_template_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_subscriber ALTER COLUMN id SET DEFAULT nextval('event_subscriber_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup ALTER COLUMN id SET DEFAULT nextval('role_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access ALTER COLUMN id SET DEFAULT nextval('role_service_access_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_headers_config ALTER COLUMN id SET DEFAULT nextval('rws_headers_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_parameters_config ALTER COLUMN id SET DEFAULT nextval('rws_parameters_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service ALTER COLUMN id SET DEFAULT nextval('service_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service_doc ALTER COLUMN id SET DEFAULT nextval('service_doc_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_lookup ALTER COLUMN id SET DEFAULT nextval('system_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_setting ALTER COLUMN id SET DEFAULT nextval('system_setting_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_custom ALTER COLUMN id SET DEFAULT nextval('user_custom_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup ALTER COLUMN id SET DEFAULT nextval('user_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role ALTER COLUMN id SET DEFAULT nextval('user_to_app_to_role_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY address (id_orga, street, postcode, city, country) FROM stdin;
1	Im Kloster 8	56179	Niederwerth	Germany
8	Goethestr.	55435	Gau-Algesheim	Deutschland
9	Affe	896	Bieber	Deutschland
\.


--
-- Data for Name: app; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY app (id, name, api_key, description, is_active, type, path, url, storage_service_id, storage_container, requires_fullscreen, allow_fullscreen_toggle, toggle_location, role_id, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
1	admin	6498a8ad1beb9d84d63035c5d1120c007fad6de706734db9689f8996707e0f7d	An application for administering this instance.	t	3	dreamfactory/dist/index.html	\N	\N	\N	f	t	top	\N	2015-12-29 16:11:18	2015-12-29 16:11:18	\N	\N
3	filemanager	b5cb82af7b5d4130f36149f90aa2746782e59a872ac70454ac188743cb55b0ba	An application for managing file services.	t	3	filemanager/index.html	\N	\N	\N	f	t	top	\N	2015-12-29 16:11:19	2016-01-11 20:45:03	\N	1
12	placity-app	427994563fdc8f1159ff7d04bd00c62ecab42f7bcd3f9e99ae2a5a38f5408d3d		t	0	\N	\N	\N	\N	f	t	top	3	2016-01-13 08:32:38	2016-01-13 09:45:10	1	1
2	swagger	36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88	A Swagger-base application allowing viewing and testing API documentation.	t	3	swagger/index.html	\N	\N	\N	f	t	top	\N	2015-12-29 16:11:18	2016-02-16 02:34:44	\N	1
6	QuestionEditor	b06beaf7a884e085b20b8c1b10d2f4712c49526c4e11597d70bff5fa4ee05988	Question-Editor	t	0	\N	http://143.93.91.92/steffen/index.html	\N	\N	f	t	top	2	2016-01-05 14:29:01	2016-02-16 04:11:29	2	1
\.


--
-- Data for Name: app_group; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY app_group (id, name, description, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: app_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_group_id_seq', 1, false);


--
-- Name: app_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_id_seq', 12, true);


--
-- Data for Name: app_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY app_lookup (id, app_id, name, value, private, description, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: app_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_lookup_id_seq', 1, false);


--
-- Data for Name: app_to_app_group; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY app_to_app_group (id, app_id, group_id) FROM stdin;
\.


--
-- Name: app_to_app_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_to_app_group_id_seq', 1, false);


--
-- Data for Name: aws_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY aws_config (service_id, key, secret, region) FROM stdin;
\.


--
-- Name: c_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('c_group_id_seq', 9, false);


--
-- Name: c_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('c_id_seq', 102, true);


--
-- Data for Name: cloud_email_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY cloud_email_config (service_id, domain, key) FROM stdin;
\.


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY contact (id, first_name, last_name, image_url, twitter, skype, notes) FROM stdin;
1	Jon	Yang		@jon24	jon24	
2	Eugene	Huang		@eugene10	eugene10	
3	Ruben	Torres		@ruben35	ruben35	
\.


--
-- Data for Name: contact_group; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY contact_group (id, name) FROM stdin;
1	Armed Forces
2	Pacific
3	South East
\.


--
-- Name: contact_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('contact_group_id_seq', 27, true);


--
-- Data for Name: contact_group_relationship; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY contact_group_relationship (id, contact_id, contact_group_id) FROM stdin;
\.


--
-- Name: contact_group_relationship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('contact_group_relationship_id_seq', 301, true);


--
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('contact_id_seq', 300, true);


--
-- Data for Name: contact_info; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY contact_info (id, ordinal, contact_id, info_type, phone, email, address, city, state, zip, country) FROM stdin;
1	0	1	home	500 555-0162	jon24@Home.com	3761 N. 14th St	MEDINA	ND	58467	USA
2	0	1	work	500 555-0110	jon24@Work.com	2243 W St.	MEDINA	ND	58467	USA
3	0	1	mobile	500 555-0184	jon24@Mobile.com	5844 Linden Land	MEDINA	ND	58467	USA
\.


--
-- Name: contact_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('contact_info_id_seq', 900, true);


--
-- Data for Name: content; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY content (id, id_page, id_content_type, data_obj, pos) FROM stdin;
1	1	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	1
14	2	5	{"languages":[{"lang":"de_DE","fields":[{"text":"<p>Wann wurde Rom gegründet?</p>","answer":"753","cmessage": "richtig","wmessage": "falsch"}]},{"lang":"en_EN","fields":[{"text":"<p>When was Rome foundet?</p>","answer":"753","cmessage": "right","wmessage": "wrong"}]}]}	2
19	3	6	{"languages":[{"lang":"de_DE","fields":[{"id":"14","audiofileurl":"http://143.93.91.92/upload/2016_1_16/Ab%20An%20De%20See.mp3","info":"InfoText"}]},{"lang":"en_EN","fields":[{"id":"14","audiofileurl":"http://143.93.91.92/upload/2016_1_16/Ab%20An%20De%20See.mp3","info":"InfoText"}]}]}	4
24	5	8	{"languages": [{"lang": "de_DE","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]},{"lang": "en_EN","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]}]}	2
25	5	8	{"languages": [{"lang": "de_DE","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]},{"lang": "en_EN","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]}]}	3
16	3	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	1
2	1	2	{"languages":[{"lang":"de_DE","fields":[{"text":"<p>Wann wurde Rom gegründet?</p>","answer":"753","choice":["357","17","753","166","1652"],"cmessage": "richtig","wmessage": "falsch"}]},{"lang":"en_EN","fields":[{"text":"<p>When was Rome foundet?</p>","answer":"753","choice":["357","17","753","166","1652"],"cmessage": "right","wmessage": "wrong"}]}]}	2
22	4	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>2Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	2
23	5	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>3Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	1
17	3	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	2
18	3	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	3
20	3	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	5
13	2	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	1
9	1	3	{"languages":[{"lang": "de_DE","fields": [{"avatar1" : "http://143.93.91.92/philipp/avatar1.jpg","avatar2" : "http://143.93.91.92/philipp/avatar1.jpg","text" :["<p>Hi Alex</p>","<p>Hi Paul</p>","<p>Na wie gehts?</p>","<p>Alles bestens, kann nicht klagen</p>"]}]},{"lang": "en_EN","fields": [{"avatar1" : "http://143.93.91.92/philipp/avatar1.jpg","avatar2" : "http://143.93.91.92/philipp/avatar1.jpg","text" :["<p>Hi Alex</p>","<p>Hi Paul</p>","<p>How are you?</p>","<p>Quite nice, thanks!</p>"]}]}]}	3
21	4	7	{"languages": [{"lang": "de_DE","fields": [{"contenttype": "video", "src": "https://upload.wikimedia.org/wikipedia/commons/transcoded/8/87/Schlossbergbahn.webm/Schlossbergbahn.webm.480p.webm", "description": "Eine Bahn faehrt einen Berg hoch", "autoplay": true}]},{"lang": "en_EN","fields": [{"contenttype": "video", "src": "https://upload.wikimedia.org/wikipedia/commons/transcoded/8/87/Schlossbergbahn.webm/Schlossbergbahn.webm.480p.webm", "description": "Eine Bahn faehrt einen Berg hoch", "autoplay": true}]}]}	1
98	98	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	1
15	2	1	{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text,</p> <p>der vom <h1 style=\\"color: red\\">MEGA MAN</h1> als <span style=\\"color: blue\\">magischer</span>  text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}	3

\.


--
-- Name: content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('content_id_seq', 2, false);


--
-- Data for Name: contenttype; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY contenttype (id, name, description, data_schema, app_ctrl_class, app_html_tpl_file, qe_ctrl_class, qe_html_tpl_file, sid, helptext) FROM stdin;
1	ct_text	ContentType Text: Instanz des ct_text,  ist ein einfacher Fließtext in einer Route	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "text"\n              ],\n              "properties": {\n                "text": {\n                  "type": "string",\n                  "minLength": 1\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  },\n  "required": [\n    "languages"\n  ]\n}\n	ctTextAppCtrl.js	ctTextAppTpl.html	ctTextQECtrl.js	ctTextQETpl.html	\N	\N
4	headline	Überschrift 	\N	ctHeadlineAppCtrl.js	ctHeadlineAppTpl.html	ctHeadlineQECtrl.js	ctHeadlineQETmpl.html	\N	\N
2	ct_MultipleChoice	ContentType MultipleChoice: Instanz des ct_MultipleChoice, ist eine Frage mit mehreren Antwortmöglichkeiten	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "text",\n                "answer",\n                "cmessage",\n                "wmessage"\n              ],\n              "properties": {\n                "text": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "answer": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "choice": {\n                  "type": "array",\n                  "items": {\n                    "required": [],\n                    "properties": {}\n                  }\n                },\n                "cmessage": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "wmessage": {\n                  "type": "string",\n                  "minLength": 1\n                }\n              }\n            }\n          }\n        }\n      }\n    },\n    "points": {\n      "type": "number"\n    }\n  },\n  "required": [\n    "languages",\n    "points"\n  ]\n}\n	ctMultipleChoice.js	ctMultipleChoice.html	ctMultipleChoiceQE.js	ctMultipleChoiceQE.html	\N	\N
3	ct_Dialog	ContentType Dialog: Instanz des ct_Dialog, ist eine Unterhaltung zwischen 2 Personen	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "avatar1",\n                "avatar2"\n              ],\n              "properties": {\n                "avatar1": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "avatar2": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "text": {\n                  "type": "array",\n                  "items": {\n                    "required": [],\n                    "properties": {}\n                  }\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  },\n  "required": [\n    "languages"\n  ]\n}\n	ctDialog.js	ctDialog.html	ctDialogQE.js	ctDialogQE.html	\N	\N
5	ct_FrageFreiText	ContentType FrageFreitext: Textuelle Frage, dessen Antwort frei formuliert wird.	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang",\n          "cmessage",\n          "wmessage"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "text",\n                "answer"\n              ],\n              "properties": {\n                "text": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "answer": {\n                  "type": "string",\n                  "minLength": 1\n                }\n              }\n            }\n          },\n          "cmessage": {\n            "type": "string",\n            "minLength": 1\n          },\n          "wmessage": {\n            "type": "string",\n            "minLength": 1\n          }\n        }\n      }\n    },\n    "points": {\n      "type": "number"\n    }\n  },\n  "required": [\n    "languages",\n    "points"\n  ]\n}\n	ctFrageFreitext.js	ctFrageFreitext.html	ctFrageFreitextQETmpl.js	ctFrageFreitextQETmpl.html	\N	\N
6	ct_Audio	ContentType Audio: Audio Inhalt	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "id",\n                "audiofileurl",\n                "info",\n                "autoplay"\n              ],\n              "properties": {\n                "id": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "audiofileurl": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "info": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "autoplay": {\n                  "type": "boolean"\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  },\n  "required": [\n    "languages"\n  ]\n}\n	ctAudio.js	ctAudioApp.html	ctAudioQETmpl.js	ctAudioQETmpl.html	\N	\N
7	ct_Video	ContentType Video: Video Inhalt	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "contenttype",\n                "src",\n                "description",\n                "autoplay"\n              ],\n              "properties": {\n                "contenttype": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "src": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "description": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "autoplay": {\n                  "type": "boolean"\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  },\n  "required": [\n    "languages"\n  ]\n}\n	ctVideo.js	ctVideo.html	ctVideoQETmpl.js	ctVideoQETml.html	\N	\N
8	ct_Image	ContentType Image: Bild Inhalt	{\n  "$schema": "http://json-schema.org/draft-04/schema#",\n  "description": "",\n  "type": "object",\n  "properties": {\n    "languages": {\n      "type": "array",\n      "uniqueItems": true,\n      "minItems": 1,\n      "items": {\n        "required": [\n          "lang"\n        ],\n        "properties": {\n          "lang": {\n            "type": "string",\n            "minLength": 1\n          },\n          "fields": {\n            "type": "array",\n            "uniqueItems": true,\n            "minItems": 1,\n            "items": {\n              "required": [\n                "src",\n                "alt"\n              ],\n              "properties": {\n                "src": {\n                  "type": "string",\n                  "minLength": 1\n                },\n                "alt": {\n                  "type": "string",\n                  "minLength": 1\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  },\n  "required": [\n    "languages"\n  ]\n}\n	ctImage.js	ctImage.html	ctImageQETmpl.js	ctImageQETmpl.html	\N	\N
\.


--
-- Data for Name: cors_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY cors_config (id, path, origin, header, method, max_age, enabled, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
1	*	*	*	31	0	t	2015-12-29 17:06:43	2016-01-13 20:07:50	1	2
\.


--
-- Name: cors_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('cors_config_id_seq', 1, true);


--
-- Data for Name: couchdb_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY couchdb_config (service_id, dsn, options) FROM stdin;
\.


--
-- Name: ct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('ct_id_seq', 4, true);


--
-- Data for Name: db_field_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) FROM stdin;
1	1	sql_db_config	dsn	Connection String (DSN)	\N	Specify the connection string for the database you're connecting to.	\N	\N	\N	2015-12-29 16:11:19	2015-12-29 16:11:19	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	1	sql_db_config	options	Driver Options	\N	A key=>value array of driver-specific connection options.	\N	\N	\N	2015-12-29 16:11:19	2015-12-29 16:11:19	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	1	sql_db_config	attributes	Driver Attributes	\N	A key=>value array of driver-specific attributes.	\N	\N	\N	2015-12-29 16:11:20	2015-12-29 16:11:20	\N	\N	\N	\N	\N	\N	\N	\N	\N
4	4	contact	id	Contact Id	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	4	contact	first_name	\N	\N	\N	\N	{"not_empty":{"on_fail":"First name value must not be empty."}}	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
6	4	contact	last_name	\N	\N	\N	\N	{"not_empty":{"on_fail":"Last name value must not be empty."}}	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
7	4	contact	image_url	image_url	\N	\N	\N	{"url":{"on_fail":"Not a valid URL value."}}	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
8	4	contact	twitter	Twitter Handle	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	4	contact	skype	Skype Account	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
10	4	contact	notes	notes	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
11	4	contact_info	id	Info Id	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	4	contact_info	ordinal	\N	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	4	contact_info	contact_id	\N	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	4	contact_info	info_type	\N	\N	\N	["work","home","mobile","other"]	{"not_empty":{"on_fail":"Information type can not be empty."},"picklist":{"on_fail":"Not a valid information type."}}	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	4	contact_info	phone	Phone Number	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	4	contact_info	email	Email Address	\N	\N	\N	{"email":{"on_fail":"Not a valid email address."}}	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	4	contact_info	address	Street Address	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	4	contact_info	city	city	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
19	4	contact_info	state	state	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
20	4	contact_info	zip	zip	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
21	4	contact_info	country	country	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
22	4	contact_group	id	\N	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
23	4	contact_group	name	\N	\N	\N	\N	{"not_empty":{"on_fail":"Group name value must not be empty."}}	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
24	4	contact_group_relationship	id	\N	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
25	4	contact_group_relationship	contact_id	\N	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
26	4	contact_group_relationship	contact_group_id	\N	\N	\N	\N	\N	\N	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N	\N	\N	\N	\N	\N	\N
27	4	TestToDelete	example_field	Example Field	\N	\N	\N	\N	\N	2016-01-05 22:08:59	2016-01-05 22:08:59	\N	\N	\N	\N	\N	\N	\N	\N	\N
28	4	TestToDelete2	example_field	Example Field	\N	\N	\N	\N	\N	2016-01-05 22:09:23	2016-01-05 22:09:23	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Name: db_field_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_field_extras_id_seq', 28, true);


--
-- Data for Name: db_relationship_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY db_relationship_extras (id, service_id, "table", relationship, alias, label, description, always_fetch, flatten, flatten_drop_prefix, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
5	4	route	highscore_by_id_route	\N	Highscore By Id Route	\N	f	f	f	2016-01-15 21:39:48	2016-01-15 21:39:48	\N	\N
6	4	route	page_by_id_route	\N	Page By Id Route	\N	f	f	f	2016-01-16 01:23:19	2016-01-16 01:23:19	\N	\N
\.


--
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_relationship_extras_id_seq', 6, true);


--
-- Data for Name: db_table_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) FROM stdin;
1	1	user	\N	\N	\N	DreamFactory\\Core\\Models\\User	\N	2015-12-29 16:11:14	2015-12-29 16:11:14	\N	\N	\N
2	1	user_lookup	\N	\N	\N	DreamFactory\\Core\\Models\\UserLookup	\N	2015-12-29 16:11:14	2015-12-29 16:11:14	\N	\N	\N
3	1	user_to_app_to_role	\N	\N	\N	DreamFactory\\Core\\Models\\UserAppRole	\N	2015-12-29 16:11:14	2015-12-29 16:11:14	\N	\N	\N
4	1	service	\N	\N	\N	DreamFactory\\Core\\Models\\Service	\N	2015-12-29 16:11:15	2015-12-29 16:11:15	\N	\N	\N
5	1	service_type	\N	\N	\N	DreamFactory\\Core\\Models\\ServiceType	\N	2015-12-29 16:11:15	2015-12-29 16:11:15	\N	\N	\N
6	1	service_doc	\N	\N	\N	DreamFactory\\Core\\Models\\ServiceDoc	\N	2015-12-29 16:11:15	2015-12-29 16:11:15	\N	\N	\N
7	1	role	\N	\N	\N	DreamFactory\\Core\\Models\\Role	\N	2015-12-29 16:11:15	2015-12-29 16:11:15	\N	\N	\N
8	1	role_service_access	\N	\N	\N	DreamFactory\\Core\\Models\\RoleServiceAccess	\N	2015-12-29 16:11:15	2015-12-29 16:11:15	\N	\N	\N
9	1	role_lookup	\N	\N	\N	DreamFactory\\Core\\Models\\RoleLookup	\N	2015-12-29 16:11:16	2015-12-29 16:11:16	\N	\N	\N
10	1	app	\N	\N	\N	DreamFactory\\Core\\Models\\App	\N	2015-12-29 16:11:16	2015-12-29 16:11:16	\N	\N	\N
11	1	app_lookup	\N	\N	\N	DreamFactory\\Core\\Models\\AppLookup	\N	2015-12-29 16:11:16	2015-12-29 16:11:16	\N	\N	\N
12	1	app_group	\N	\N	\N	DreamFactory\\Core\\Models\\AppGroup	\N	2015-12-29 16:11:16	2015-12-29 16:11:16	\N	\N	\N
13	1	app_to_app_group	\N	\N	\N	DreamFactory\\Core\\Models\\AppToAppGroup	\N	2015-12-29 16:11:16	2015-12-29 16:11:16	\N	\N	\N
14	1	system_resource	\N	\N	\N	DreamFactory\\Core\\Models\\SystemResource	\N	2015-12-29 16:11:17	2015-12-29 16:11:17	\N	\N	\N
15	1	script_type	\N	\N	\N	DreamFactory\\Core\\Models\\ScriptType	\N	2015-12-29 16:11:17	2015-12-29 16:11:17	\N	\N	\N
16	1	event_script	\N	\N	\N	DreamFactory\\Core\\Models\\EventScript	\N	2015-12-29 16:11:17	2015-12-29 16:11:17	\N	\N	\N
17	1	event_subscriber	\N	\N	\N	DreamFactory\\Core\\Models\\EventSubscriber	\N	2015-12-29 16:11:17	2015-12-29 16:11:17	\N	\N	\N
18	1	email_template	\N	\N	\N	DreamFactory\\Core\\Models\\EmailTemplate	\N	2015-12-29 16:11:18	2015-12-29 16:11:18	\N	\N	\N
19	1	system_setting	\N	\N	\N	DreamFactory\\Core\\Models\\Setting	\N	2015-12-29 16:11:18	2015-12-29 16:11:18	\N	\N	\N
20	1	system_lookup	\N	\N	\N	DreamFactory\\Core\\Models\\Lookup	\N	2015-12-29 16:11:18	2015-12-29 16:11:18	\N	\N	\N
21	1	sql_db_config	\N	\N	\N	DreamFactory\\Core\\SqlDb\\Models\\SqlDbConfig	\N	2015-12-29 16:11:19	2015-12-29 16:11:19	\N	\N	\N
22	1	role_adldap	\N	\N	\N	DreamFactory\\Core\\ADLdap\\Models\\RoleADLdap	\N	2015-12-29 16:11:25	2015-12-29 16:11:25	\N	\N	\N
25	4	contact	\N	\N	\N	\N	The main table for tracking contacts.	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N
26	4	contact_info	\N	\N	\N	\N	The contact details sub-table, owned by contact table row.	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N
27	4	contact_group	\N	\N	\N	\N	The main table for tracking groups of contact.	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N
28	4	contact_group_relationship	\N	\N	\N	\N	The join table for tracking contacts in groups.	2016-01-05 14:37:13	2016-01-05 14:37:13	\N	\N	\N
29	4	TestToDelete	\N	\N	\N	\N	\N	2016-01-05 22:08:59	2016-01-05 22:08:59	\N	\N	\N
30	4	TestToDelete2	TestToDelete2	TestToDelete2	\N	\N	\N	2016-01-05 22:09:23	2016-01-05 22:09:23	\N	\N	TestToDelete2
31	4	v_route_page_content_tojson	V Route Page Content Tojson	V Route Page Content Tojsons	\N	\N	\N	2016-01-11 08:37:53	2016-01-11 08:37:53	\N	\N	\N
\.


--
-- Name: db_table_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_table_extras_id_seq', 31, true);


--
-- Data for Name: email_parameters_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY email_parameters_config (id, service_id, name, value, active) FROM stdin;
\.


--
-- Name: email_parameters_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('email_parameters_config_id_seq', 1, false);


--
-- Data for Name: email_template; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY email_template (id, name, description, "to", cc, bcc, subject, body_text, body_html, from_name, from_email, reply_to_name, reply_to_email, defaults, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
1	User Invite Default	Email sent to invite new users to your DreamFactory instance.	\N	\N	\N	[DF] New User Invitation	\N	<div style="padding: 10px;">\n                                <p>\n                                Hi {first_name},\n                                </p>\n                                <p>\n                                    You have been invited to the DreamFactory Instance of {instance_name}. Go to the following url, enter the code below, and set\n                                    your password to confirm your account.\n                                    <br/>\n                                    <br/>\n                                    {link}\n                                    <br/>\n                                    <br/>\n                                    Confirmation Code: {confirm_code}<br/>\n                                </p>\n                                <p>\n                                    <cite>-- The Dream Team</cite>\n                                </p>\n                              </div>	DO NOT REPLY	no-reply@dreamfactory.com	\N	\N	\N	2015-12-29 16:11:23	2015-12-29 17:07:04	\N	1
3	Password Reset Default	Email sent to users following a request to reset their password.	\N	\N	\N	[DF] Password Reset	\N	<div style="padding: 10px;">\n                                <p>\n                                    Hi {first_name},\n                                </p>\n                                <p>\n                                    You have requested to reset your password. Go to the following url, enter the code below, and set your new password.\n                                    <br>\n                                    <br>\n                                    {link}\n                                    <br>\n                                    <br>\n                                    Confirmation Code: {confirm_code}\n                                </p>\n                                <p>\n                                    <cite>-- The Dream Team</cite>\n                                </p>\n                            </div>	DO NOT REPLY	no-reply@dreamfactory.com	\N	\N	\N	2015-12-29 16:11:23	2015-12-29 17:07:22	\N	1
2	User Registration Default	Email sent to new users to complete registration.	\N	\N	\N	[DF] Registration Confirmation	\N	<div style="padding: 10px;">\n                                <p>\n                                    Hi {first_name},\n                                </p>\n                                <p>\n                                    You have registered an user account on the DreamFactory instance of {instance_name}. Go to the following url, enter the\n                                    code below, and set your password to confirm your account.\n                                    <br/>\n                                    <br/>\n                                    {link}\n                                    <br/>\n                                    <br/>\n                                    Confirmation Code: {confirm_code}\n                                    <br/>\n                                </p>\n                                <p>\n                                    <cite>-- The Dream Team</cite>\n                                </p>\n                            </div>	DO NOT REPLY	no-reply@dreamfactory.com	\N	\N	\N	2015-12-29 16:11:23	2015-12-29 17:07:30	\N	1
\.


--
-- Name: email_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('email_template_id_seq', 3, true);


--
-- Data for Name: event_script; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY event_script (name, type, is_active, affects_process, content, config, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
sample-scripts	v8js	f	f	//\tHere are a few examples to get you started. var_dump() can be used to log script output.\n\n//****************** Pre-processing script on a table ******************\n\n//\tA script that is triggered by a POST on /api/v2/db/_table/<tablename>. Runs before the db call is made.\n//  The script validates that every record in the request has a value for the name field.\n\nvar_dump(event.request); // outputs to file in storage/log of dreamfactory install directory\n\nvar lodash = require("lodash.min.js");\n\nif (event.request.payload.resource) {\n\n    lodash._.each (event.request.payload.resource, function( record ) {\n\n        if (!record.name) {\n            throw 'Name cannot be empty';\n        }\n    });\n}\n\n//****************** Post-processing script on a table ******************\n\n//  A script that is triggered by a GET on /api/v2/db/_table/<tablename>. Runs after the db call is made.\n//  The script adds a new field to each record in the response.\n\nvar_dump(event.response); // outputs to file in storage/log of dreamfactory install directory\n\nvar lodash = require("lodash.min.js");\n\nif (event.response.resource) {\n\n    lodash._.each (event.response.resource, function( record ) {\n\n        record.extraField = 'Feed the dog.';\n    });\n}\n	\N	2016-01-26 03:57:56	2016-01-26 04:01:52	1	1
db._table.player.post.pre_process	php	t	t	$param= \\Request::json()->all();\nforeach($param as $e){\n    shell_exec("echo $e >> /opt/df2/test_params.info");\n    \n}\n throw new \\Exception('Missing);	\N	2016-02-05 01:55:08	2016-02-05 02:33:27	1	1
files.{file_path}.post.pre_process	php	t	t	shell_exec("echo script > /opt/df2/file_script.info");	\N	2016-01-27 22:05:15	2016-02-05 03:18:01	1	1
db._table.mediafile.post.post_process	php	t	t	shell_exec('nohup php /opt/pc4/df2/fileserver/videoencoder.php &');\n	\N	2016-02-04 21:40:14	2016-02-16 15:46:03	1	1
\.


--
-- Data for Name: event_subscriber; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY event_subscriber (id, name, type, config, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: event_subscriber_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('event_subscriber_id_seq', 1, false);


--
-- Data for Name: file_service_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY file_service_config (service_id, public_path, container) FROM stdin;
3	[]	local
\.


--
-- Data for Name: filesharedforgroup; Type: TABLE DATA; Schema: public; Owner: placity
--

--
-- Data for Name: filesharedfororga; Type: TABLE DATA; Schema: public; Owner: placity
--




--
-- Data for Name: group; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY "group" (id, groupname, group_admin, id_orga) FROM stdin;
1	albus-group	2	1
11	Group001	1	1
13	test2	1	1
12	test2	3	2
14	test4	1	2
16	toDelete	14	2
15	Gruppe XY	13	9
18	Test Gruppe ABC	13	9
17	Test Gruppe	27	9
\.


--
-- Name: group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('group_id_seq', 20, true);


--
-- Data for Name: groupmember; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY groupmember (id_group, id_user) FROM stdin;
18	12
15	12
15	11
18	2
18	13
17	12
18	11
18	26
17	27
17	13
17	3
1	1
1	3
11	2
11	1
11	3
1	2
12	3
12	12
1	12
13	1
13	2
15	13
15	15
15	17
15	18
15	14
15	21
15	3
\.


--
-- Data for Name: highscore; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY highscore (name, id_route, points, date) FROM stdin;
\.


--
-- Data for Name: ldap_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY ldap_config (service_id, default_role, host, base_dn, account_suffix, map_group_to_role, username, password) FROM stdin;
\.


--
-- Data for Name: mediafile; Type: TABLE DATA; Schema: public; Owner: placity
--

-- COPY mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) FROM stdin;
-- 90	Test123Rakete.jpg	/upload/2016_1_29/	http://143.93.91.92/upload/2016_1_29/Test123Rakete.jpg	1	image/jpeg	610318	12	f	f
-- 94	123.jpg.jpg	/upload/2016_2_2/	http://143.93.91.92/upload/2016_2_2/123.jpg.jpg	1	image/jpeg	610318	12	f	f
-- 96	Affe_undefined.jpg	/upload/12/	http://143.93.91.92/upload/12/Affe_undefined.jpg	1	image/jpeg	722020	12	f	f
-- 98	Alligatoah - Musik Ist Keine Loesung (2015_12.jpg	/upload/12/	http://143.93.91.92/upload/12/Alligatoah - Musik Ist Keine Loesung (2015_12.jpg	1	image/jpeg	722020	12	f	f
-- 100	iHerzU_12.gif	/upload/12/	http://143.93.91.92/upload/12/iHerzU_12.gif	1	image/gif	381940	12	f	f
-- 47	raclette.JPG	2016_1_24/	http://143.93.91.92/upload/2016_1_24/raclette.JPG	1	image/jpeg	5242880	11	f	t
-- 46	darth_vader_armor_star_wars_film_hat_snow_93645_2560x1024.jpg	2016_1_20/	http://143.93.91.92/upload/2016_1_20/darth_vader_armor_star_wars_film_hat_snow_93645_2560x1024.jpg	1	image/jpeg	358894	12	f	t
-- 44	123.JPG	2016_1_19/	http://143.93.91.92/upload/2016_1_19/123.JPG	1	image/jpeg	4947968	12	f	t
-- 43	giphy.gif	2016_1_17/	http://143.93.91.92/upload/2016_1_17/giphy.gif	1	image/gif	381940	12	f	t
-- 42	90453.jpg	2016_1_16/	http://143.93.91.92/upload/2016_1_16/90453.jpg	1	image/jpeg	524425	12	f	t
-- 50	big_buck_bunny_720p_5mb.mp4	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_5mb.mp4	1	video	5253880	1	t	f
-- 64	big_buck_bunny_720p_50mb.flv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_50mb.flv	1	video	52512308	1	t	f
-- 61	big_buck_bunny_240p_50mb.mp4	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_240p_50mb.mp4	1	video	51246445	1	t	f
-- 38	big-buck-bunny_trailer.webm	2016_1_16/	http://143.93.91.92/upload/2016_1_16/big-buck-bunny_trailer.webm	1	video/webm	2165175	12	f	t
-- 39	Ab An De See.mp3	2016_1_16/	http://143.93.91.92/upload/2016_1_16/Ab An De See.mp3	1	audio/mp3	7719028	12	f	t
-- 58	big_buck_bunny_360p_50mb.mp4	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_360p_50mb.mp4	1	video	52489326	1	t	f
-- 55	big_buck_bunny_480p_50mb.mp4	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_480p_50mb.mp4	1	video	52460977	1	t	f
-- 52	big_buck_bunny_720p_50mb.mp4	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_50mb.mp4	1	video	52464391	1	t	f
-- 51	big_buck_bunny_720p_10mb.mp4	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_10mb.mp4	1	video	10498677	1	t	f
-- 89	rakete.jpg	/upload/2016_1_29/	http://143.93.91.92/upload/2016_1_29/rakete.jpg	1	image/jpeg	610318	12	f	f
-- 91	123.jpg	/upload/2016_2_1/	http://143.93.91.92/upload/2016_2_1/123.jpg	1	image/jpeg	1303391	12	f	f
-- 95	Alligatoah - Musik Ist Keine Loesung (2015).jpg	/upload/2016_2_2/	http://143.93.91.92/upload/2016_2_2/Alligatoah - Musik Ist Keine Loesung (2015).jpg	1	image/jpeg	722020	12	f	f
-- 97	AffeABC_12.jpg	/upload/12/	http://143.93.91.92/upload/12/AffeABC_12.jpg	1	image/jpeg	722020	12	f	f
-- 101	DS_12.JPG	/upload/12/	http://143.93.91.92/upload/12/DS_12.JPG	1	image/jpeg	3490897	12	f	f
-- 87	baderegeln.pdf	/upload/2016_1_25/	http://143.93.91.92/upload/2016_1_25/baderegeln.pdf	1	application/pdf	710773	12	f	f
-- 85	big_buck_bunny_240p_50mb.mkv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_240p_50mb.mkv	1	video	52474687	1	t	f
-- 82	big_buck_bunny_360p_50mb.mkv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_360p_50mb.mkv	1	video	52459935	1	t	f
-- 79	big_buck_bunny_480p_50mb.mkv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_480p_50mb.mkv	1	video	52515793	1	t	f
-- 76	big_buck_bunny_720p_50mb.mkv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_50mb.mkv	1	video	52524751	1	t	f
-- 73	big_buck_bunny_240p_50mb.flv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_240p_50mb.flv	1	video	52505638	1	t	f
-- 70	big_buck_bunny_360p_50mb.flv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_360p_50mb.flv	1	video	52498525	1	t	f
-- 67	big_buck_bunny_480p_50mb.flv	/upload/testfiles/	http://143.93.91.92/upload/testfiles/big_buck_bunny_480p_50mb.flv	1	video	52480075	1	t	f
-- \.


--
-- Name: mediafile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('mediafile_id_seq', 104, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY migrations (migration, batch) FROM stdin;
2015_01_27_190908_create_system_tables	1
2015_02_03_161456_create_sqldb_tables	1
2015_02_03_161457_create_couchdb_tables	1
2015_02_03_161457_create_mongodb_tables	1
2015_02_03_161457_create_salesforce_tables	1
2015_03_10_135522_create_aws_tables	1
2015_03_11_143913_create_rackspace_tables	1
2015_03_20_205504_create_remote_web_service_tables	1
2015_03_20_205504_create_soap_service_tables	1
2015_05_02_134911_update_user_table_for_oauth_support	1
2015_05_04_034605_create_adldap_tables	1
2015_05_21_190727_create_user_config_table	1
2015_07_10_161839_create_user_custom_table	1
2015_08_24_180219_default_schema_only	1
2015_08_25_202632_db_alias	1
2015_11_06_155036_db_function	1
2015_11_10_225902_db_foreign_key	1
2015_11_17_181913_ad_enhancements	1
2015_12_29_154754_myDB	1
2015_01_27_190908_create_system_tables	1
2015_02_03_161456_create_sqldb_tables	1
2015_02_03_161457_create_couchdb_tables	1
2015_02_03_161457_create_mongodb_tables	1
2015_02_03_161457_create_salesforce_tables	1
2015_03_10_135522_create_aws_tables	1
2015_03_11_143913_create_rackspace_tables	1
2015_03_20_205504_create_remote_web_service_tables	1
2015_03_20_205504_create_soap_service_tables	1
2015_05_02_134911_update_user_table_for_oauth_support	1
2015_05_04_034605_create_adldap_tables	1
2015_05_21_190727_create_user_config_table	1
2015_07_10_161839_create_user_custom_table	1
2015_08_24_180219_default_schema_only	1
2015_08_25_202632_db_alias	1
2015_11_06_155036_db_function	1
2015_11_10_225902_db_foreign_key	1
2015_11_17_181913_ad_enhancements	1
2015_12_29_154754_myDB	1
\.


--
-- Data for Name: mongodb_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY mongodb_config (service_id, dsn, options, driver_options) FROM stdin;
\.


--
-- Data for Name: oauth_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY oauth_config (service_id, default_role, client_id, client_secret, redirect_url, icon_class) FROM stdin;
\.


--
-- Data for Name: one_time_access_code; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY one_time_access_code (code, id_orga, id_group) FROM stdin;
ddd73ecb623c73dedad0897a006644	9	18
16638b3ec41215ed31e49cc1fde9e7	9	18
4c07bb63951eb235bce7f64ef8ae48	9	18
c28d83a4687cbf2e02ce942a5432cf	9	15
e5313e6f2368825be5beca8bb5618c	9	15
c60ffb6c41d5669dbe03f439896675	9	15
221a523fe8f96dfd3a5f9b2a4eea82	9	15
558f45470ae9737d7da8d559f130d2	9	15
f49b95d4924ffd7bb3d47744a761d8	9	15
76377652ea1eb84ced6999eaf98628	9	15
1ce42143069e6b9313d7e20e5234d4	9	15
8fcd5fdeeb3c424aa79e8d97a9ebf4	9	15
8f848a68ec71c9c2a6092ea50eea63	9	15
25bebc954c3131862d701a573b16d9	9	15
db8acd3a14b1fdeb8674c927838900	9	18
fbd9b3a6bf5c8415a6d161f40873b7	9	15
36180ac6ed213c4ef90fa2125faebb	9	15
b4dae25d8bfda31b867795c477a509	9	15
414fe3c7ac45c42ef59dd75ba6daf2	9	15
f549984275b0fcc8f51b83ebd3fad9	9	15
7d20db65bc2b27a9bee6d0141e98ae	9	15
2ad4ceed77aa7bf5f2d41c377003c8	9	15
59c9e959d092c5e3a36f5dbccd13a1	9	15
abe48f5254e2edef746657dfff2e00	9	15
4bf012165a9dd1cbf25a4676d799bc	9	15
bc6a71ad2cdc5a6da25ea6d584bb70	9	15
e9d5bfabbff02f9d597443845ca60c	9	15
02498986126cd4ae730968f1e8ef12	9	15
f5222fa6be9c38b7d2784df7400176	9	17
6742f89a90f51ee263ae56ef90a529	9	17
06c5e819f68fcfcf0d4e2ed44f59ea	9	17
11e7a815fb2ccdd4f9d40ac496311e	9	17
33c988935fb0c57f4bdfbe21ae25fa	9	17
9d27787155fd40cd4bf22806e5a4ef	9	18
995c5f374cb584639df732517f3c7d	9	18
1d649f1e05475a71b698021b06c340	9	18
6c06cb081070ea6fcdd80a0e875583	9	18
30d47d1a14d547fe3ef5adc11016c5	9	18
a5c093bfbd2d4677c4ef2a272cbd61	9	18
2a2e719214944819840092c3e0e066	9	15
07aac7f2feb98720ed0f661837421c	9	15
6522f372c3d9869e21dec451b745a6	9	15
bbc3216568d577f12b9288ea0a6123	9	15
3fd8644ffba975515bca14d7546838	9	15
48def375c9826827beb604c18d1b8a	9	15
9ef632e5c2dc354a73da62a75edeb4	9	15
2445b69fe67ec43e742364c8753cb2	9	15
40b941eff80cc6bcf22d70745e5d15	9	15
d9e505efe188d6181462402ec02391	9	17
c3d0c3bdb899450f9a6633236b6639	9	17
8ebca194e9c0418ac6296417f8b601	9	17
c9dca8d9909683926a619081237543	9	17
83e5f6718ed2a1eaf753435aed5975	9	17
331208ef4168348818bce8db66cb54	9	17
5d9f67736eee9527a9a1444d3ee522	9	17
0fd9f8d86b5e26ea38e20e166f2618	9	17
3ecef08af37f532d54a752c5c0bab4	9	17
862f26e28e99476a2f0d03b1099a28	9	17
6fef66d150e4a627ea5f163c7377c7	9	17
491dc3060fecc26596bc81a4746093	9	17
85a3c1de34bb016a2be7313f55c3c5	9	17
4df688476a5444497d8ae0acdc3b08	9	17
03e2d1f2b1eb64be3074f0832c6654	9	17
4ab855b10246a6864d0bc55bd22cc2	9	17
2f5152b7d1698502788bd254e756d1	9	17
88fdc01e40e905f1c974112c4bd931	9	17
e68748e9cba260ad67d1053d850162	9	17
c0f5b3699a486779359cd8364d1592	9	17
94d7a4f20d91513991a565fff10ad0	9	17
e24aa7b3651ea028938a25ba4b8255	9	17
435fa0759ac0032c1f6d98f89968f1	9	17
\.


--
-- Data for Name: orgamember; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY orgamember (id_orga, id_user) FROM stdin;
1	1
1	2
2	3
2	1
\.


--
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY organization (id, name, description, orga_admin) FROM stdin;
1	albus-it	Beschreibung der Organisation albus-it	1
2	test-orga	test-orga1	2
8	Orga-XP	Windows 7	2
9	Steffens Orga	Steffen	12
\.


--
-- Name: organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('organization_id_seq', 9, true);


--
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY page (id, next, page_name, pos, id_route) FROM stdin;
1	\N	seite1	1	1
2	\N	seite_2	2	1
19	\N	Seite_08	10	1
3	\N	seite_3	3	1
4	\N	Seite_4	4	1
5	\N	Seite_5	5	1
6	\N	Seite_6	6	1
7	\N	Seite_7	7	1
8	\N	Seite_8	8	1
10	\N	Seite_10	1	2
11	\N	Seite_11	2	2
12	\N	Seite_12	3	2
13	\N	Seite_13	4	2
14	\N	Seite_14	5	2
15	\N	Seite_15	6	2
16	\N	Seite_05	7	2
18	\N	Seite_07	9	2
17	\N	Seite_06	8	\N
98	\N	Text_Test_Seite_1	1	770
9	\N	Seite_9	9	1
99	\N	Text_Test_Seite_2	2	770
\.


--
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('page_id_seq', 45, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY password_resets (email, token, created_date) FROM stdin;
\.


--
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY player (name, avatar_img) FROM stdin;
player1	myimg.jpg
player2	myimg.jpg
player3	myimg.jpg
player4	myimg.jpg
mybodyplayer1	myimg.jpg
mybodyplayer21	myimg.jpg
allRequestFields	myimg.jpg
player111	myimg.jpg
player222	myimg.jpg
player333	myimg.jpg
\.


--
-- Data for Name: rackspace_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY rackspace_config (service_id, username, password, tenant_name, api_key, url, region, storage_type) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY role (id, name, description, is_active, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
2	BasicUser	Simple test User, Access to everything	t	2016-01-05 14:30:32	2016-01-05 14:31:19	2	2
3	Player	Player ist ein User der Placity App	t	2016-01-10 06:15:36	2016-02-12 17:10:36	1	21
\.


--
-- Data for Name: role_adldap; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY role_adldap (role_id, dn) FROM stdin;
\.


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_id_seq', 4, true);


--
-- Data for Name: role_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY role_lookup (id, role_id, name, value, private, description, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: role_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_lookup_id_seq', 1, false);


--
-- Data for Name: role_service_access; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY role_service_access (id, role_id, service_id, component, verb_mask, requestor_mask, filters, filter_op, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
2	2	\N	*	31	1	[]	AND	2016-01-05 14:31:06	2016-01-05 17:45:48	\N	\N
4	3	\N	*	31	3	[]	AND	2016-01-16 11:31:38	2016-01-16 11:31:58	\N	\N
5	3	4	*	1	1	[]	AND	2016-01-16 11:34:09	2016-02-12 17:14:00	\N	\N
6	3	4	_table/highscore/	3	1	[]	AND	2016-02-12 17:14:00	2016-02-12 17:14:00	\N	\N
\.


--
-- Name: role_service_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_service_access_id_seq', 6, true);


--
-- Data for Name: route; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY route (id, name, title, subtitle, description, image, app_scheme, android_scheme, win_scheme, private, id_orga, id_user, id_group) FROM stdin;
32	neueRoute	\N	\N	\N	\N	APP_SCHEME	android_scheme	\N	 	\N	2	\N
770	Text_Test_Seite_1	text test titel	text test subtitel	text test	\N	APP_SCHEME	android_scheme	\N	0	\N	2	\N
2	Mega Geile SUper Route	title	subtitle	description	IMAGE-URL|BASE64	app_scheme	android_scheme	win_scheme	0	\N	12	\N
31	neueRoute 31	\N	\N	\N	\N	APP_SCHEME	android_scheme	\N	 	\N	2	\N
1	testroute	title	subtitle	description	IMAGE-URL|BASE64	app_scheme	android_scheme	win_scheme	0	1	2	1
\.


--
-- Name: route_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('route_id_seq', 32, true);


--
-- Data for Name: routemodel; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY routemodel (id, name, title, subtitle, description, lang, id_route) FROM stdin;
\.


--
-- Name: routemodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('routemodel_id_seq', 100, false);


--
-- Data for Name: routentag; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY routentag (id_route, tag_name) FROM stdin;
1	best
1	hammer
1	placity
770	route
1	route
1	hunde
2	geil
31	route
32	route
2	route
\.


--
-- Data for Name: rws_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY rws_config (service_id, base_url) FROM stdin;
\.


--
-- Data for Name: rws_headers_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY rws_headers_config (id, service_id, name, value, pass_from_client, action) FROM stdin;
\.


--
-- Name: rws_headers_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('rws_headers_config_id_seq', 1, false);


--
-- Data for Name: rws_parameters_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY rws_parameters_config (id, service_id, name, value, exclude, outbound, cache_key, action) FROM stdin;
\.


--
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('rws_parameters_config_id_seq', 1, false);


--
-- Data for Name: salesforce_db_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY salesforce_db_config (service_id, username, password, security_token) FROM stdin;
\.


--
-- Data for Name: script_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY script_config (service_id, type, content, config) FROM stdin;
\.


--
-- Data for Name: script_type; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY script_type (name, class_name, label, description, sandboxed, created_date, last_modified_date) FROM stdin;
php	DreamFactory\\Core\\Scripting\\Engines\\Php	PHP	Script handler using native PHP.	f	2015-12-29 16:11:13	2015-12-29 16:11:13
nodejs	DreamFactory\\Core\\Scripting\\Engines\\NodeJs	Node.js	Server-side JavaScript handler using the Node.js engine.	f	2015-12-29 16:11:13	2015-12-29 16:11:13
v8js	DreamFactory\\Core\\Scripting\\Engines\\V8Js	V8js	Server-side JavaScript handler using the V8js engine.	t	2015-12-29 16:11:13	2015-12-29 16:11:13
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
1	system	System Management	Service for managing system resources.	t	system	f	f	2015-12-29 16:11:13	2015-12-29 16:11:13	\N	\N
2	api_docs	Live API Docs	API documenting and testing service.	t	swagger	f	f	2015-12-29 16:11:14	2015-12-29 16:11:14	\N	\N
5	email	Local Email Service	Email service used for sending user invites and/or password reset confirmation.	t	local_email	t	t	2015-12-29 16:11:23	2015-12-29 16:11:23	\N	\N
9	smtp	smtp-mail		t	smtp_email	t	t	2015-12-29 16:47:49	2016-01-04 15:55:25	1	2
6	user	User Management	Service for managing system users.	t	user	t	f	2015-12-29 16:11:24	2016-01-06 11:35:21	\N	2
3	files	Local File Storage	Service for accessing local file storage.	t	local_file	t	t	2015-12-29 16:11:14	2016-01-10 07:32:44	\N	1
4	db	Local SQL Database	Service for accessing pgSQL database	t	sql_db	t	t	2015-12-29 16:11:20	2016-02-04 21:37:06	\N	1
\.


--
-- Data for Name: service_cache_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY service_cache_config (service_id, cache_enabled, cache_ttl) FROM stdin;
4	t	0
\.


--
-- Data for Name: service_doc; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY service_doc (id, service_id, format, content, created_date, last_modified_date) FROM stdin;
\.


--
-- Name: service_doc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('service_doc_id_seq', 1, false);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('service_id_seq', 12, true);


--
-- Data for Name: service_type; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) FROM stdin;
system	DreamFactory\\Core\\Services\\System	\N	System Management Service	Service supporting management of the system.	System	t	2015-12-29 16:11:07	2015-12-29 16:11:07
swagger	DreamFactory\\Core\\Services\\Swagger	\N	Swagger API Docs	API documenting and testing service using Swagger specifications.	API Doc	t	2015-12-29 16:11:08	2015-12-29 16:11:08
event	DreamFactory\\Core\\Services\\Event	\N	Event Service	Service that allows clients to subscribe to system broadcast events.	Event	t	2015-12-29 16:11:08	2015-12-29 16:11:08
script	DreamFactory\\Core\\Services\\Script	DreamFactory\\Core\\Models\\ScriptConfig	Custom Scripting Service	Service that allows client-callable scripts utilizing the system scripting.	Custom	f	2015-12-29 16:11:08	2015-12-29 16:11:08
local_file	DreamFactory\\Core\\Services\\LocalFileService	DreamFactory\\Core\\Models\\FilePublicPath	Local File Service	File service supporting the local file system.	File	f	2015-12-29 16:11:08	2015-12-29 16:11:08
local_email	DreamFactory\\Core\\Services\\Email\\Local	\N	Local Email Service	Local email service using system configuration.	Email	f	2015-12-29 16:11:08	2015-12-29 16:11:08
smtp_email	DreamFactory\\Core\\Services\\Email\\Smtp	DreamFactory\\Core\\Models\\SmtpConfig	SMTP Email Service	SMTP-based email service	Email	f	2015-12-29 16:11:09	2015-12-29 16:11:09
mailgun_email	DreamFactory\\Core\\Services\\Email\\MailGun	DreamFactory\\Core\\Models\\MailGunConfig	Mailgun Email Service	Mailgun email service	Email	f	2015-12-29 16:11:09	2015-12-29 16:11:09
mandrill_email	DreamFactory\\Core\\Services\\Email\\Mandrill	DreamFactory\\Core\\Models\\MandrillConfig	Mandrill Email Service	Mandrill email service	Email	f	2015-12-29 16:11:09	2015-12-29 16:11:09
sql_db	DreamFactory\\Core\\SqlDb\\Services\\SqlDb	DreamFactory\\Core\\SqlDb\\Models\\SqlDbConfig	SQL DB	Database service supporting SQL connections.	Database	f	2015-12-29 16:11:19	2015-12-29 16:11:19
mongodb	DreamFactory\\Core\\MongoDb\\Services\\MongoDb	DreamFactory\\Core\\MongoDb\\Models\\MongoDbConfig	MongoDB	Database service for MongoDB connections.	Database	f	2015-12-29 16:11:20	2015-12-29 16:11:20
couchdb	DreamFactory\\Core\\CouchDb\\Services\\CouchDb	DreamFactory\\Core\\CouchDb\\Models\\CouchDbConfig	CouchDB	Database service for CouchDB connections.	Database	f	2015-12-29 16:11:20	2015-12-29 16:11:20
rws	DreamFactory\\Core\\Rws\\Services\\RemoteWeb	DreamFactory\\Core\\Rws\\Models\\RwsConfig	Remote Web Service	A service to handle Remote Web Services	Custom	f	2015-12-29 16:11:21	2015-12-29 16:11:21
soap	DreamFactory\\Core\\Soap\\Services\\Soap	DreamFactory\\Core\\Soap\\Models\\SoapConfig	SOAP Service	A service to handle SOAP Services	Custom	f	2015-12-29 16:11:21	2015-12-29 16:11:21
aws_s3	DreamFactory\\Core\\Aws\\Services\\S3	DreamFactory\\Core\\Aws\\Components\\AwsS3Config	AWS S3	File storage service supporting the AWS S3 file system.	File	f	2015-12-29 16:11:21	2015-12-29 16:11:21
aws_dynamodb	DreamFactory\\Core\\Aws\\Services\\DynamoDb	DreamFactory\\Core\\Aws\\Models\\AwsConfig	AWS DynamoDB	A database service supporting the AWS DynamoDB system.	Database	f	2015-12-29 16:11:21	2015-12-29 16:11:21
aws_sns	DreamFactory\\Core\\Aws\\Services\\Sns	DreamFactory\\Core\\Aws\\Models\\AwsConfig	AWS SNS	Push notification service supporting the AWS SNS system.	Notification	f	2015-12-29 16:11:21	2015-12-29 16:11:21
aws_ses	DreamFactory\\Core\\Aws\\Services\\Ses	DreamFactory\\Core\\Aws\\Models\\AwsConfig	AWS SES	Email service supporting the AWS SES system.	Email	f	2015-12-29 16:11:22	2015-12-29 16:11:22
rackspace_cloud_files	DreamFactory\\Core\\Rackspace\\Services\\OpenStackObjectStore	DreamFactory\\Core\\Rackspace\\Components\\RackspaceCloudFilesConfig	Rackspace Cloud Files	File service supporting Rackspace Cloud Files Storage system.	File	f	2015-12-29 16:11:22	2015-12-29 16:11:22
openstack_obect_storage	DreamFactory\\Core\\Rackspace\\Services\\OpenStackObjectStore	DreamFactory\\Core\\Rackspace\\Components\\OpenStackObjectStorageConfig	OpenStack Object Storage	File service supporting OpenStack Object Storage system.	File	f	2015-12-29 16:11:22	2015-12-29 16:11:22
salesforce_db	DreamFactory\\Core\\Salesforce\\Services\\SalesforceDb	DreamFactory\\Core\\Salesforce\\Models\\SalesforceConfig	SalesforceDB	Database service for Salesforce connections.	Database	f	2015-12-29 16:11:22	2015-12-29 16:11:22
user	DreamFactory\\Core\\User\\Services\\User	DreamFactory\\Core\\User\\Models\\UserConfig	User service	User service to allow user management.	User	t	2015-12-29 16:11:23	2015-12-29 16:11:23
oauth_facebook	DreamFactory\\Core\\OAuth\\Services\\Facebook	DreamFactory\\Core\\OAuth\\Models\\OAuthConfig	Facebook OAuth	OAuth service for supporting Facebook authentication and API access.	OAuth	f	2015-12-29 16:11:24	2015-12-29 16:11:24
oauth_twitter	DreamFactory\\Core\\OAuth\\Services\\Twitter	DreamFactory\\Core\\OAuth\\Models\\OAuthConfig	Twitter OAuth	OAuth service for supporting Twitter authentication and API access.	OAuth	f	2015-12-29 16:11:24	2015-12-29 16:11:24
oauth_github	DreamFactory\\Core\\OAuth\\Services\\Github	DreamFactory\\Core\\OAuth\\Models\\OAuthConfig	GitHub OAuth	OAuth service for supporting GitHub authentication and API access.	OAuth	f	2015-12-29 16:11:25	2015-12-29 16:11:25
oauth_google	DreamFactory\\Core\\OAuth\\Services\\Google	DreamFactory\\Core\\OAuth\\Models\\OAuthConfig	Google OAuth	OAuth service for supporting Google authentication and API access.	OAuth	f	2015-12-29 16:11:25	2015-12-29 16:11:25
oauth_linkedin	DreamFactory\\Core\\OAuth\\Services\\LinkedIn	DreamFactory\\Core\\OAuth\\Models\\OAuthConfig	LinkedIn OAuth	OAuth service for supporting LinkedIn authentication and API access.	OAuth	f	2015-12-29 16:11:25	2015-12-29 16:11:25
adldap	DreamFactory\\Core\\ADLdap\\Services\\ADLdap	DreamFactory\\Core\\ADLdap\\Models\\ADConfig	Active Directory LDAP	A service for supporting Active Directory integration	LDAP	f	2015-12-29 16:11:25	2015-12-29 16:11:25
ldap	DreamFactory\\Core\\ADLdap\\Services\\LDAP	DreamFactory\\Core\\ADLdap\\Models\\LDAPConfig	Standard LDAP	A service for supporting Open LDAP integration	LDAP	f	2015-12-29 16:11:26	2015-12-29 16:11:26
\.


--
-- Data for Name: smtp_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY smtp_config (service_id, host, port, encryption, username, password) FROM stdin;
9	smtp.gmail.com	587	tls	eyJpdiI6ImFOMkpESjlUMmV5M0J4NmtvQmp4bFE9PSIsInZhbHVlIjoiZnFDWCtUNzgwXC94TTVvVXF1NkJVbXNWSmx6bDUycGZFbVZNT3hjcWZqalU9IiwibWFjIjoiMmM4NzQ1NDdjYmU5ZDc3MzZmZjhjNTU3ODlkODM5ODY3ZWM1ZTZhMjc3MWQ3ZDI1YzZiODc2YTNlMzI3MzNkZCJ9	eyJpdiI6ImdYamk3UGZ2b0d6TUdvNWVpa1ZHOFE9PSIsInZhbHVlIjoic1hydWF1Nmoxd3FvV05LRml5RitzOG84MHlcL0RTU2VnVWJicjBaamNMUTQ9IiwibWFjIjoiNmIwNzJmMDE3M2U2ZWNhYjM2NjBkYWNiNjM5NTM1ZjU5YTg0MzhhM2VkYzg4YTk2NTQzOWNmZDA2ZmY4NmJmMCJ9
\.


--
-- Data for Name: soap_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY soap_config (service_id, wsdl, options) FROM stdin;
\.


--
-- Data for Name: sql_db_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY sql_db_config (service_id, driver, dsn, username, password, options, attributes, default_schema_only) FROM stdin;
4	pgsql	pgsql:host=localhost;port=5432;dbname=placity	eyJpdiI6IkRLWkp2eHk1U1BZU1AwM2M4OEViUWc9PSIsInZhbHVlIjoiTjllbTR1a29hUHZCS3FsMEl2UkYrZz09IiwibWFjIjoiZDM3Yzk1NDZmNzNhMzE0NjVmZmM3MmEyMTQyYzA1ZWEzM2Q0MDJhN2I3MzA2N2MyZTVhNmE2Y2YxODhlMDViOCJ9	eyJpdiI6InJiblFCd3lmQjZTV0hqenh4SmN3UGc9PSIsInZhbHVlIjoieWFiQndrSWVGM1FUZGJhY2dGbW1LUT09IiwibWFjIjoiZWQ1YzVhNDZmY2I4ODFjNDNmMTEwZDA0MzM3ODljM2Y0MTk3MDM5ZGI2ZWVjMmEwYWIwM2MyNTdlMjBhMTRiYSJ9	\N	\N	f
\.


--
-- Data for Name: system_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY system_config (db_version, login_with_user_name, default_app_id, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Data for Name: system_custom; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY system_custom (name, value, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Data for Name: system_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY system_lookup (id, name, value, private, description, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: system_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('system_lookup_id_seq', 1, false);


--
-- Data for Name: system_resource; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) FROM stdin;
admin	DreamFactory\\Core\\Resources\\System\\Admin	Administrators	Allows configuration of system administrators.	DreamFactory\\Core\\Models\\User	f	f	2015-12-29 16:11:09	2015-12-29 16:11:09
cache	DreamFactory\\Core\\Resources\\System\\Cache	Cache Administration	Allows administration of system-wide and service cache.	\N	f	f	2015-12-29 16:11:09	2015-12-29 16:11:09
config	DreamFactory\\Core\\Resources\\System\\Config	Configuration	Global system configuration.	\N	t	f	2015-12-29 16:11:10	2015-12-29 16:11:10
constant	DreamFactory\\Core\\Resources\\System\\Constant	Constants	Read-only listing of constants available for client use.	\N	f	t	2015-12-29 16:11:10	2015-12-29 16:11:10
cors	DreamFactory\\Core\\Resources\\System\\Cors	CORS Configuration	Allows configuration of CORS system settings.	DreamFactory\\Core\\Models\\CorsConfig	f	f	2015-12-29 16:11:10	2015-12-29 16:11:10
email_template	DreamFactory\\Core\\Resources\\System\\EmailTemplate	Email Templates	Allows configuration of email templates.	DreamFactory\\Core\\Models\\EmailTemplate	f	f	2015-12-29 16:11:10	2015-12-29 16:11:10
environment	DreamFactory\\Core\\Resources\\System\\Environment	Environment	Read-only system environment configuration.	\N	t	t	2015-12-29 16:11:10	2015-12-29 16:11:10
event	DreamFactory\\Core\\Resources\\System\\Event	Events	Allows registering server-side scripts to system generated events.	\N	f	f	2015-12-29 16:11:11	2015-12-29 16:11:11
lookup	DreamFactory\\Core\\Resources\\System\\Lookup	Lookup Keys	Allows configuration of lookup keys.	DreamFactory\\Core\\Models\\Lookup	f	f	2015-12-29 16:11:11	2015-12-29 16:11:11
role	DreamFactory\\Core\\Resources\\System\\Role	Roles	Allows role configuration.	DreamFactory\\Core\\Models\\Role	f	f	2015-12-29 16:11:11	2015-12-29 16:11:11
service	DreamFactory\\Core\\Resources\\System\\Service	Services	Allows configuration of services.	DreamFactory\\Core\\Models\\Service	f	f	2015-12-29 16:11:11	2015-12-29 16:11:11
service_type	DreamFactory\\Core\\Resources\\System\\ServiceType	Service Types	Read-only system service types.	DreamFactory\\Core\\Models\\ServiceType	f	t	2015-12-29 16:11:12	2015-12-29 16:11:12
script_type	DreamFactory\\Core\\Resources\\System\\ScriptType	Script Types	Read-only system scripting types.	DreamFactory\\Core\\Models\\ScriptType	f	t	2015-12-29 16:11:12	2015-12-29 16:11:12
setting	DreamFactory\\Core\\Resources\\System\\Setting	Custom Settings	Allows configuration of system-wide custom settings.	DreamFactory\\Core\\Models\\Setting	f	f	2015-12-29 16:11:12	2015-12-29 16:11:12
app	DreamFactory\\Core\\Resources\\System\\App	Apps	Allows management of user application(s)	DreamFactory\\Core\\Models\\App	f	f	2015-12-29 16:11:12	2015-12-29 16:11:12
app_group	DreamFactory\\Core\\Resources\\System\\AppGroup	App Groups	Allows grouping of user application(s)	DreamFactory\\Core\\Models\\AppGroup	f	f	2015-12-29 16:11:12	2015-12-29 16:11:12
custom	DreamFactory\\Core\\Resources\\System\\Custom	Custom Settings	Allows for creating system-wide custom settings	DreamFactory\\Core\\Models\\SystemCustom	f	f	2015-12-29 16:11:13	2015-12-29 16:11:13
user	DreamFactory\\Core\\User\\Resources\\System\\User	User Management	Allows user management capability.	DreamFactory\\Core\\Models\\User	f	f	2015-12-29 16:11:24	2015-12-29 16:11:24
\.


--
-- Data for Name: system_setting; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY system_setting (id, name, value, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: system_setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('system_setting_id_seq', 1, false);


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY tag (tag_name) FROM stdin;
best
hammer
placity
route
hunde
geil
\.


--
-- Data for Name: token_map; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY token_map (user_id, token, iat, exp) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) FROM stdin;
27	Frank Tutor	Frank	Tutor	\N	frank@de.de	$2y$10$lyg0rCGm9qMYLs/94GnWxeIpM0HuVeauJLCnNYiAsLqjc.CUUCJ8G	f	t	\N	\N	\N	\N	\N	\N	2016-01-29 10:29:34	2016-01-29 10:29:35	\N	\N	\N	\N
13	Janina Janina	Janina	Janina	\N	jan@ina.de	$2y$10$mxnWGkVYtL/a/6Z4yV/mAewFN.uzDbPGSUhEXHIxtiX6lC6/beWmq	f	t	\N	\N	\N	\N	\N	\N	2016-01-06 15:17:43	2016-01-06 15:17:43	11	\N	\N	\N
24	Kevin test Kevin test	Kevin test	Kevin test	2016-01-17 16:52:51	nope@keine.de	$2y$10$mSq0Xpf6.7fzjuYrLuzWIOvXevzPT.qEVIGd2NZIo0jdcSkQl46nG	f	t	\N	\N	\N	y	\N	\N	2016-01-17 16:52:26	2016-01-17 16:52:51	\N	\N	\N	\N
15	Tim Tim	Tim	Tim	\N	tim@bim.de	$2y$10$jVVaoT6TMpjWhYcbSevVd.1fAH8hHsWqU9efNu2tZCFG6NFBDLsem	f	t	\N	\N	\N	\N	\N	\N	2016-01-06 17:30:45	2016-01-06 17:30:45	\N	\N	\N	\N
11	test2 test2	test2	test2	2016-01-29 10:46:36	te@st.de	$2y$10$kjSaguuuvpZXgBJqaKg.uee1A3pR236TYvDmlVkpyLTXV4NR.76J6	f	t	\N	\N	\N	y	\N	\N	2016-01-06 12:15:45	2016-01-29 10:46:36	\N	2	\N	\N
14	Emma Emma	Emma	Emma	\N	em@ma.de	$2y$10$oE/qfxKCPFcksXynnfhUVeBhhS/y6SjacOc3XCVRAXpVq/x3wO/o.	f	f	\N	\N	\N	\N	\N	\N	2016-01-06 17:25:49	2016-01-06 17:25:49	\N	\N	\N	\N
18	Vanessa Vanessa	Vanessa	Vanessa	2016-02-14 11:44:55	vanessa@van.de	$2y$10$zs8DtcHa5e.cW4oLgYymSeRD4btlB.WBgx1rjMIMiP0PhRCKF6CXW	f	t	\N	\N	\N	y	\N	\N	2016-01-12 11:51:51	2016-02-14 11:44:55	12	2	\N	\N
22	f.koegel	\N	\N	2016-01-19 15:51:27	f.koegel@placity.de	$2y$10$LoaFJD8Yr/tX2mYrqIjUce0b754QXBtWUOo4oF0zokRFPlz5n9wb6	t	t	\N	\N	\N	y	\N	\N	2016-01-14 09:22:48	2016-01-19 15:51:27	1	\N	\N	\N
25	Albert Einstein	Albert	Einstein	\N	ein@stein.de	$2y$10$ICB/O69Em61gDgOlMlI8Xe4QtCgSN/ZTUInZay4ljQ/XgGoPk5Ss6	f	t	\N	\N	\N	\N	\N	\N	2016-01-19 17:00:33	2016-01-19 17:00:33	\N	\N	\N	\N
3	vanessa.m.merz	Vanessa	Merz	2016-02-15 09:49:01	vanessa.m.merz@fh-bingen.de	$2y$10$UpA5pFDEMsMrYdgMMh4SjeTlfbdJT1JuKbzxZik2wUNGdfVGlxrWW	t	t	\N	\N	\N	y	\N	\N	2015-12-29 16:13:59	2016-02-15 09:49:01	1	2	\N	\N
20	paul.koch	\N	\N	2016-02-15 22:53:39	paul.koch@fh-bingen.de	$2y$10$KUumJPErjKobuOVAfw8R5eF7rwoMpH4IP0515FRunXZDvp6p/kzvq	t	t	\N	\N	\N	y	\N	\N	2016-01-13 09:04:43	2016-02-15 22:53:39	1	\N	\N	\N
17	Franziska Franziska	Franziska	Franziska	2016-01-12 11:17:23	fr@nzi.de	$2y$10$BCXmbQ1ERKt3qmUC9sJGIuLJj4L.7euOzqyWQU2YrS1KFe5JXsYDG	f	t	\N	\N	\N	y	\N	\N	2016-01-12 11:16:19	2016-01-12 11:17:23	\N	\N	\N	\N
19	Pweller	Philipp	Weller	2016-01-13 08:18:35	phwe@gmx.net	$2y$10$t04M.S2v9d/JhQJEGwht7.yeZ2U3bxwId33YoAN.wwKOWXfBJ.f9e	f	t	\N	\N	\N	y	\N	\N	2016-01-13 08:18:33	2016-01-16 16:03:52	\N	1	\N	\N
30	albus	Alexander	Weiss	2016-02-16 04:17:08	alexander.weiss@fh-bingen.de	$2y$10$7oZ8ORBVh5eJlUqkMMhi7eCN3epDcuY5SqTy8EuXyJikbq4xvGIDC	f	t	\N	\N	\N	y	\N	\N	2016-02-16 04:13:16	2016-02-16 04:17:08	\N	\N	\N	\N
26	Peter Müller	Peter	Müller	2016-01-29 09:01:42	pet@er.de	$2y$10$X6RaNwUJLCWdjPqcqcVG6.kSLtuwH/pJvPvP5Yf8zBpAFxC3nT/OK	f	t	\N	\N	\N	y	\N	\N	2016-01-29 09:01:33	2016-01-29 09:01:42	\N	\N	\N	\N
1	Albus	Alexander	Weiß	2016-02-16 04:51:09	info@albus-it.com	$2y$10$2eowG1xUu7obeO.LKGvIBOBH2RJ0e0DUhk679eI8//LWXF5RREEeO	t	t	\N	\N	\N	y	1	\N	2015-12-29 16:12:09	2016-02-16 04:51:09	\N	1	\N	\N
12	Katrin Katrin	Katrin	Katrin	2016-02-16 10:59:07	kat@rin.de	$2y$10$8Hsu.jjtIls2fYjxuy7IY.w0Hq3aww7VWlekmC6FfJjDGLrGGTzTu	f	t	\N	\N	\N	y	\N	\N	2016-01-06 13:53:06	2016-02-16 10:59:07	\N	\N	\N	\N
23	Alex Weiss	Alex	Weiss	2016-02-09 16:21:56	alexander.weiss@live.de	$2y$10$T6jTVSdvGaJs3CKmjwX4W.fWmv.eF7pMcPpRKlZH4KyUYS0nMY4m6	f	t	\N	\N	\N	y	\N	\N	2016-01-16 03:21:23	2016-02-09 16:21:56	\N	1	\N	\N
2	steffen.hollenbach	Steffen	Hollenbach	2016-02-02 10:18:58	steffen.hollenbach@fh-bingen.de	$2y$10$vnuJ2jpFGjh5fa0WF7ufb.sTn9itKHvskL1mrr5U6led10YgB1UIG	t	t		\N	\N	y	\N	\N	2015-12-29 16:13:30	2016-02-02 10:18:58	1	2	\N	\N
28	MD5_User	MD5	oldpwhash	2016-02-05 01:19:48	test@md5.de	$2y$10$Ucloe7rT2a8dQsqC9bNXqOaUghsV69NI8ZAt0iOqSTbvVNuyFGii6	f	t	\N	\N	\N	y	\N	\N	2016-02-05 00:00:00	2016-02-05 01:19:48	\N	1	\N	\N
21	philipp.weller	\N	\N	2016-02-12 17:10:19	philipp.weller@fh-bingen.de	$2y$10$aJXzkuiVTdL8Ek7dpT2FOuYvB7AjmN2rx6bSzHaz3SxKuTbZcf0U2	t	t	\N	\N	\N	y	\N	\N	2016-01-13 09:08:28	2016-02-12 17:10:19	1	\N	\N	\N
\.


--
-- Data for Name: user_config; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY user_config (service_id, allow_open_registration, open_reg_role_id, open_reg_email_service_id, open_reg_email_template_id, invite_email_service_id, invite_email_template_id, password_email_service_id, password_email_template_id) FROM stdin;
6	t	2	\N	\N	\N	\N	9	3
\.


--
-- Data for Name: user_custom; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY user_custom (id, user_id, name, value, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: user_custom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_custom_id_seq', 7, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_id_seq', 30, true);


--
-- Data for Name: user_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY user_lookup (id, user_id, name, value, private, description, created_date, last_modified_date, created_by_id, last_modified_by_id) FROM stdin;
\.


--
-- Name: user_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_lookup_id_seq', 1, false);


--
-- Data for Name: user_to_app_to_role; Type: TABLE DATA; Schema: public; Owner: placity
--

COPY user_to_app_to_role (id, user_id, app_id, role_id) FROM stdin;
17	11	1	2
18	11	2	2
19	11	3	2
20	11	6	2
21	12	1	2
22	12	2	2
23	12	3	2
24	12	6	2
25	13	1	2
26	13	2	2
27	13	3	2
28	13	6	2
29	14	1	2
30	14	2	2
31	14	3	2
32	14	6	2
33	15	1	2
34	15	2	2
35	15	3	2
36	15	6	2
41	17	1	2
42	17	2	2
43	17	6	2
44	17	3	2
46	18	1	2
47	18	2	2
48	18	6	2
49	18	3	2
52	19	2	2
53	19	6	2
54	19	3	2
56	23	1	2
57	23	2	2
58	23	6	2
59	23	3	2
61	23	12	2
62	19	12	3
51	19	1	2
63	24	1	2
64	24	2	2
65	24	6	2
66	24	3	2
68	24	12	2
69	25	1	2
70	25	2	2
71	25	6	2
72	25	3	2
74	25	12	2
75	26	1	2
76	26	2	2
77	26	6	2
78	26	3	2
80	26	12	2
81	27	1	2
82	27	2	2
83	27	6	2
84	27	3	2
86	27	12	2
87	28	1	2
88	28	2	2
89	28	3	2
90	28	6	2
92	28	12	2
93	30	1	2
94	30	3	2
95	30	12	2
96	30	2	2
97	30	6	2
\.


--
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_to_app_to_role_id_seq', 97, true);


--
-- Name: app_group_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_name_unique UNIQUE (name);


--
-- Name: app_group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_pkey PRIMARY KEY (id);


--
-- Name: app_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_pkey PRIMARY KEY (id);


--
-- Name: app_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_name_unique UNIQUE (name);


--
-- Name: app_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_pkey PRIMARY KEY (id);


--
-- Name: app_to_app_group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_to_app_group
    ADD CONSTRAINT app_to_app_group_pkey PRIMARY KEY (id);


--
-- Name: aws_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY aws_config
    ADD CONSTRAINT aws_config_pkey PRIMARY KEY (service_id);


--
-- Name: cloud_email_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY cloud_email_config
    ADD CONSTRAINT cloud_email_config_pkey PRIMARY KEY (service_id);


--
-- Name: contact_group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contact_group
    ADD CONSTRAINT contact_group_pkey PRIMARY KEY (id);


--
-- Name: contact_group_relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contact_group_relationship
    ADD CONSTRAINT contact_group_relationship_pkey PRIMARY KEY (id);


--
-- Name: contact_info_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contact_info
    ADD CONSTRAINT contact_info_pkey PRIMARY KEY (id);


--
-- Name: contact_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- Name: content_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY content
    ADD CONSTRAINT content_pkey PRIMARY KEY (id);


--
-- Name: contenttype_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT contenttype_pkey PRIMARY KEY (id);


--
-- Name: cors_config_path_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_path_unique UNIQUE (path);


--
-- Name: cors_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_pkey PRIMARY KEY (id);


--
-- Name: couchdb_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY couchdb_config
    ADD CONSTRAINT couchdb_config_pkey PRIMARY KEY (service_id);


--
-- Name: db_field_extras_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_pkey PRIMARY KEY (id);


--
-- Name: db_relationship_extras_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_pkey PRIMARY KEY (id);


--
-- Name: db_table_extras_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_pkey PRIMARY KEY (id);


--
-- Name: email_parameters_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY email_parameters_config
    ADD CONSTRAINT email_parameters_config_pkey PRIMARY KEY (id);


--
-- Name: email_template_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_name_unique UNIQUE (name);


--
-- Name: email_template_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_pkey PRIMARY KEY (id);


--
-- Name: event_script_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_pkey PRIMARY KEY (name);


--
-- Name: event_subscriber_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_name_unique UNIQUE (name);


--
-- Name: event_subscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_pkey PRIMARY KEY (id);


--
-- Name: file_service_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY file_service_config
    ADD CONSTRAINT file_service_config_pkey PRIMARY KEY (service_id);


--
-- Name: filesharedforgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT filesharedforgroup_pkey PRIMARY KEY (id_file, id_group);


--
-- Name: filesharedfororga_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT filesharedfororga_pkey PRIMARY KEY (id_file, id_orga);


--
-- Name: fkorg_add_id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY address
    ADD CONSTRAINT fkorg_add_id PRIMARY KEY (id_orga);


--
-- Name: fksuccessor_id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT fksuccessor_id UNIQUE (next);


--
-- Name: group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- Name: groupmember_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT groupmember_pkey PRIMARY KEY (id_group, id_user);


--
-- Name: highscore_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT highscore_pkey PRIMARY KEY (name, id_route);


--
-- Name: id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- Name: ldap_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY ldap_config
    ADD CONSTRAINT ldap_config_pkey PRIMARY KEY (service_id);


--
-- Name: mediafile_file_name_key; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT mediafile_file_name_key UNIQUE (file_name);


--
-- Name: mediafile_natural_key; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT mediafile_natural_key UNIQUE (file_name, path);


--
-- Name: mediafile_uri_key; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT mediafile_uri_key UNIQUE (url);


--
-- Name: mongodb_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mongodb_config
    ADD CONSTRAINT mongodb_config_pkey PRIMARY KEY (service_id);


--
-- Name: oauth_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY oauth_config
    ADD CONSTRAINT oauth_config_pkey PRIMARY KEY (service_id);


--
-- Name: one_time_access_code_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY one_time_access_code
    ADD CONSTRAINT one_time_access_code_pkey PRIMARY KEY (code);


--
-- Name: orgamember_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT orgamember_pkey PRIMARY KEY (id_orga, id_user);


--
-- Name: organization_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);


--
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- Name: player_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY player
    ADD CONSTRAINT player_pkey PRIMARY KEY (name);


--
-- Name: rackspace_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rackspace_config
    ADD CONSTRAINT rackspace_config_pkey PRIMARY KEY (service_id);


--
-- Name: role_adldap_dn_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_adldap
    ADD CONSTRAINT role_adldap_dn_unique UNIQUE (dn);


--
-- Name: role_adldap_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_adldap
    ADD CONSTRAINT role_adldap_pkey PRIMARY KEY (role_id);


--
-- Name: role_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_pkey PRIMARY KEY (id);


--
-- Name: role_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_name_unique UNIQUE (name);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: role_service_access_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_pkey PRIMARY KEY (id);


--
-- Name: route_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_pkey PRIMARY KEY (id);


--
-- Name: routemodel_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY routemodel
    ADD CONSTRAINT routemodel_pkey PRIMARY KEY (id);


--
-- Name: routentag_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT routentag_pkey PRIMARY KEY (id_route, tag_name);


--
-- Name: rws_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rws_config
    ADD CONSTRAINT rws_config_pkey PRIMARY KEY (service_id);


--
-- Name: rws_headers_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rws_headers_config
    ADD CONSTRAINT rws_headers_config_pkey PRIMARY KEY (id);


--
-- Name: rws_parameters_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rws_parameters_config
    ADD CONSTRAINT rws_parameters_config_pkey PRIMARY KEY (id);


--
-- Name: salesforce_db_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY salesforce_db_config
    ADD CONSTRAINT salesforce_db_config_pkey PRIMARY KEY (service_id);


--
-- Name: script_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY script_config
    ADD CONSTRAINT script_config_pkey PRIMARY KEY (service_id);


--
-- Name: script_type_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY script_type
    ADD CONSTRAINT script_type_pkey PRIMARY KEY (name);


--
-- Name: service_cache_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service_cache_config
    ADD CONSTRAINT service_cache_config_pkey PRIMARY KEY (service_id);


--
-- Name: service_doc_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service_doc
    ADD CONSTRAINT service_doc_pkey PRIMARY KEY (id);


--
-- Name: service_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_name_unique UNIQUE (name);


--
-- Name: service_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- Name: service_type_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service_type
    ADD CONSTRAINT service_type_pkey PRIMARY KEY (name);


--
-- Name: sid; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT sid UNIQUE (sid);


--
-- Name: smtp_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY smtp_config
    ADD CONSTRAINT smtp_config_pkey PRIMARY KEY (service_id);


--
-- Name: soap_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY soap_config
    ADD CONSTRAINT soap_config_pkey PRIMARY KEY (service_id);


--
-- Name: sql_db_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY sql_db_config
    ADD CONSTRAINT sql_db_config_pkey PRIMARY KEY (service_id);


--
-- Name: system_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_config
    ADD CONSTRAINT system_config_pkey PRIMARY KEY (db_version);


--
-- Name: system_custom_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_custom
    ADD CONSTRAINT system_custom_pkey PRIMARY KEY (name);


--
-- Name: system_lookup_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_name_unique UNIQUE (name);


--
-- Name: system_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_pkey PRIMARY KEY (id);


--
-- Name: system_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_resource
    ADD CONSTRAINT system_resource_pkey PRIMARY KEY (name);


--
-- Name: system_setting_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_name_unique UNIQUE (name);


--
-- Name: system_setting_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_pkey PRIMARY KEY (id);


--
-- Name: tag_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (tag_name);


--
-- Name: user_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_config
    ADD CONSTRAINT user_config_pkey PRIMARY KEY (service_id);


--
-- Name: user_custom_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_custom
    ADD CONSTRAINT user_custom_pkey PRIMARY KEY (id);


--
-- Name: user_email_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_unique UNIQUE (email);


--
-- Name: user_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_to_app_to_role_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_pkey PRIMARY KEY (id);


--
-- Name: app_lookup_name_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX app_lookup_name_index ON app_lookup USING btree (name);


--
-- Name: ldap_config_default_role_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX ldap_config_default_role_index ON ldap_config USING btree (default_role);


--
-- Name: ndx_contact_first_name; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX ndx_contact_first_name ON contact USING btree (first_name);


--
-- Name: ndx_contact_last_name; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX ndx_contact_last_name ON contact USING btree (last_name);


--
-- Name: oauth_config_default_role_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX oauth_config_default_role_index ON oauth_config USING btree (default_role);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: password_resets_token_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX password_resets_token_index ON password_resets USING btree (token);


--
-- Name: role_lookup_name_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX role_lookup_name_index ON role_lookup USING btree (name);


--
-- Name: user_lookup_name_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX user_lookup_name_index ON user_lookup USING btree (name);


--
-- Name: app_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_group_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_group_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_lookup_app_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_app_id_foreign FOREIGN KEY (app_id) REFERENCES app(id) ON DELETE CASCADE;


--
-- Name: app_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE SET NULL;


--
-- Name: app_storage_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_storage_service_id_foreign FOREIGN KEY (storage_service_id) REFERENCES service(id) ON DELETE SET NULL;


--
-- Name: app_to_app_group_app_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_to_app_group
    ADD CONSTRAINT app_to_app_group_app_id_foreign FOREIGN KEY (app_id) REFERENCES app(id) ON DELETE CASCADE;


--
-- Name: app_to_app_group_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_to_app_group
    ADD CONSTRAINT app_to_app_group_group_id_foreign FOREIGN KEY (group_id) REFERENCES app_group(id) ON DELETE CASCADE;


--
-- Name: aws_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY aws_config
    ADD CONSTRAINT aws_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: cloud_email_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cloud_email_config
    ADD CONSTRAINT cloud_email_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: contenttype_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY content
    ADD CONSTRAINT contenttype_fk FOREIGN KEY (id_content_type) REFERENCES contenttype(id);


--
-- Name: cors_config_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: cors_config_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: couchdb_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY couchdb_config
    ADD CONSTRAINT couchdb_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_field_extras_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_field_extras_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_field_extras_ref_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_ref_service_id_foreign FOREIGN KEY (ref_service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_field_extras_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_relationship_extras_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_relationship_extras_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_relationship_extras_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_table_extras_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_table_extras_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_table_extras_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: email_parameters_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_parameters_config
    ADD CONSTRAINT email_parameters_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: email_template_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: email_template_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_script_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_script_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_script_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_type_foreign FOREIGN KEY (type) REFERENCES script_type(name) ON DELETE CASCADE;


--
-- Name: event_subscriber_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_subscriber_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: file_service_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY file_service_config
    ADD CONSTRAINT file_service_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: fk_contact_group_relationship_contact_group_id; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact_group_relationship
    ADD CONSTRAINT fk_contact_group_relationship_contact_group_id FOREIGN KEY (contact_group_id) REFERENCES contact_group(id) ON DELETE CASCADE;


--
-- Name: fk_contact_group_relationship_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact_group_relationship
    ADD CONSTRAINT fk_contact_group_relationship_contact_id FOREIGN KEY (contact_id) REFERENCES contact(id) ON DELETE CASCADE;


--
-- Name: fk_contact_info_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY contact_info
    ADD CONSTRAINT fk_contact_info_contact_id FOREIGN KEY (contact_id) REFERENCES contact(id) ON DELETE CASCADE;


--
-- Name: group_admin; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT group_admin FOREIGN KEY (group_admin) REFERENCES "user"(id);


--
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY one_time_access_code
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: id_file; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT id_file FOREIGN KEY (id_file) REFERENCES mediafile(id);


--
-- Name: id_file; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT id_file FOREIGN KEY (id_file) REFERENCES mediafile(id);


--
-- Name: id_group; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY route
    ADD CONSTRAINT id_group FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: id_user; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY route
    ADD CONSTRAINT id_user FOREIGN KEY (id_user) REFERENCES "user"(id);


--
-- Name: id_user; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT id_user FOREIGN KEY (id_user) REFERENCES "user"(id);


--
-- Name: ldap_config_default_role_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY ldap_config
    ADD CONSTRAINT ldap_config_default_role_foreign FOREIGN KEY (default_role) REFERENCES role(id) ON DELETE RESTRICT;


--
-- Name: ldap_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY ldap_config
    ADD CONSTRAINT ldap_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: mongodb_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY mongodb_config
    ADD CONSTRAINT mongodb_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: name_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT name_fk FOREIGN KEY (tag_name) REFERENCES tag(tag_name);


--
-- Name: name_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT name_fk FOREIGN KEY (name) REFERENCES player(name);


--
-- Name: oauth_config_default_role_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY oauth_config
    ADD CONSTRAINT oauth_config_default_role_foreign FOREIGN KEY (default_role) REFERENCES role(id) ON DELETE RESTRICT;


--
-- Name: oauth_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY oauth_config
    ADD CONSTRAINT oauth_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: orga_admin; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT orga_admin FOREIGN KEY (orga_admin) REFERENCES "user"(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY address
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY route
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: page_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY content
    ADD CONSTRAINT page_fk FOREIGN KEY (id_page) REFERENCES page(id);


--
-- Name: rackspace_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rackspace_config
    ADD CONSTRAINT rackspace_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: role_adldap_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_adldap
    ADD CONSTRAINT role_adldap_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: role_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_lookup_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: role_service_access_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_service_access_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_service_access_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: role_service_access_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY page
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routemodel
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: rws_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_config
    ADD CONSTRAINT rws_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: rws_headers_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_headers_config
    ADD CONSTRAINT rws_headers_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: rws_parameters_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_parameters_config
    ADD CONSTRAINT rws_parameters_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: salesforce_db_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY salesforce_db_config
    ADD CONSTRAINT salesforce_db_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: script_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY script_config
    ADD CONSTRAINT script_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: script_config_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY script_config
    ADD CONSTRAINT script_config_type_foreign FOREIGN KEY (type) REFERENCES script_type(name) ON DELETE CASCADE;


--
-- Name: service_cache_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service_cache_config
    ADD CONSTRAINT service_cache_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: service_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: service_doc_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service_doc
    ADD CONSTRAINT service_doc_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: service_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: service_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_type_foreign FOREIGN KEY (type) REFERENCES service_type(name) ON DELETE CASCADE;


--
-- Name: smtp_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY smtp_config
    ADD CONSTRAINT smtp_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: soap_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY soap_config
    ADD CONSTRAINT soap_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: sql_db_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY sql_db_config
    ADD CONSTRAINT sql_db_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: successor_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY page
    ADD CONSTRAINT successor_fk FOREIGN KEY (next) REFERENCES page(id);


--
-- Name: system_config_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_config
    ADD CONSTRAINT system_config_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_config_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_config
    ADD CONSTRAINT system_config_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_custom_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_custom
    ADD CONSTRAINT system_custom_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_custom_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_custom
    ADD CONSTRAINT system_custom_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_setting_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_setting_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: token_map_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY token_map
    ADD CONSTRAINT token_map_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_config
    ADD CONSTRAINT user_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: user_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_custom_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_custom
    ADD CONSTRAINT user_custom_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_custom_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_custom
    ADD CONSTRAINT user_custom_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_custom_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_custom
    ADD CONSTRAINT user_custom_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT user_fk FOREIGN KEY (id_user) REFERENCES "user"(id);


--
-- Name: user_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT user_fk FOREIGN KEY (id_user) REFERENCES "user"(id);


--
-- Name: user_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_lookup_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_to_app_to_role_app_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_app_id_foreign FOREIGN KEY (app_id) REFERENCES app(id) ON DELETE CASCADE;


--
-- Name: user_to_app_to_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: user_to_app_to_role_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: mediafile; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE mediafile FROM PUBLIC;
REVOKE ALL ON TABLE mediafile FROM placity;
GRANT ALL ON TABLE mediafile TO placity;
GRANT ALL ON TABLE mediafile TO PUBLIC;


--
-- Name: routemodel_id_seq; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON SEQUENCE routemodel_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE routemodel_id_seq FROM placity;
GRANT ALL ON SEQUENCE routemodel_id_seq TO placity;
GRANT ALL ON SEQUENCE routemodel_id_seq TO PUBLIC;


--
-- Name: routemodel; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE routemodel FROM PUBLIC;
REVOKE ALL ON TABLE routemodel FROM placity;
GRANT ALL ON TABLE routemodel TO placity;
GRANT ALL ON TABLE routemodel TO PUBLIC;


--
-- Name: test; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE test FROM PUBLIC;
REVOKE ALL ON TABLE test FROM placity;
GRANT ALL ON TABLE test TO placity;
GRANT ALL ON TABLE test TO PUBLIC;


--
-- Name: user_custom.id; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL(id) ON TABLE user_custom FROM PUBLIC;
REVOKE ALL(id) ON TABLE user_custom FROM placity;
GRANT SELECT(id),INSERT(id),REFERENCES(id) ON TABLE user_custom TO PUBLIC;


--
-- Name: v_content_page; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_content_page FROM PUBLIC;
REVOKE ALL ON TABLE v_content_page FROM placity;
GRANT ALL ON TABLE v_content_page TO placity;
GRANT ALL ON TABLE v_content_page TO PUBLIC;


--
-- Name: v_emails_group; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_emails_group FROM PUBLIC;
REVOKE ALL ON TABLE v_emails_group FROM placity;
GRANT ALL ON TABLE v_emails_group TO placity;
GRANT ALL ON TABLE v_emails_group TO PUBLIC;


--
-- Name: v_groupmember; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_groupmember FROM PUBLIC;
REVOKE ALL ON TABLE v_groupmember FROM placity;
GRANT ALL ON TABLE v_groupmember TO placity;
GRANT ALL ON TABLE v_groupmember TO PUBLIC;


--
-- Name: v_file_shared_with_group; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_file_shared_with_group FROM PUBLIC;
REVOKE ALL ON TABLE v_file_shared_with_group FROM placity;
GRANT ALL ON TABLE v_file_shared_with_group TO placity;
GRANT ALL ON TABLE v_file_shared_with_group TO PUBLIC;


--
-- Name: v_file_shared_with_orga; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_file_shared_with_orga FROM PUBLIC;
REVOKE ALL ON TABLE v_file_shared_with_orga FROM placity;
GRANT ALL ON TABLE v_file_shared_with_orga TO placity;
GRANT ALL ON TABLE v_file_shared_with_orga TO PUBLIC;


--
-- Name: v_groupmember_orgaadmin; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_groupmember_orgaadmin FROM PUBLIC;
REVOKE ALL ON TABLE v_groupmember_orgaadmin FROM placity;
GRANT ALL ON TABLE v_groupmember_orgaadmin TO placity;
GRANT ALL ON TABLE v_groupmember_orgaadmin TO PUBLIC;


--
-- Name: v_groups_in_orga; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_groups_in_orga FROM PUBLIC;
REVOKE ALL ON TABLE v_groups_in_orga FROM placity;
GRANT ALL ON TABLE v_groups_in_orga TO placity;
GRANT ALL ON TABLE v_groups_in_orga TO PUBLIC;


--
-- Name: v_orga; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_orga FROM PUBLIC;
REVOKE ALL ON TABLE v_orga FROM placity;
GRANT ALL ON TABLE v_orga TO placity;
GRANT ALL ON TABLE v_orga TO PUBLIC;


--
-- Name: v_orgamember; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_orgamember FROM PUBLIC;
REVOKE ALL ON TABLE v_orgamember FROM placity;
GRANT ALL ON TABLE v_orgamember TO placity;
GRANT ALL ON TABLE v_orgamember TO PUBLIC;


--
-- Name: v_route_json; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_route_json FROM PUBLIC;
REVOKE ALL ON TABLE v_route_json FROM placity;
GRANT ALL ON TABLE v_route_json TO placity;
GRANT ALL ON TABLE v_route_json TO PUBLIC;


--
-- Name: v_route_page; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_route_page FROM PUBLIC;
REVOKE ALL ON TABLE v_route_page FROM placity;
GRANT ALL ON TABLE v_route_page TO placity;
GRANT ALL ON TABLE v_route_page TO PUBLIC;


--
-- Name: v_route_page_content_tojson; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE v_route_page_content_tojson FROM PUBLIC;
REVOKE ALL ON TABLE v_route_page_content_tojson FROM placity;
GRANT ALL ON TABLE v_route_page_content_tojson TO placity;
GRANT ALL ON TABLE v_route_page_content_tojson TO PUBLIC;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: -; Owner: placity
--

ALTER DEFAULT PRIVILEGES FOR ROLE placity REVOKE ALL ON SEQUENCES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE placity REVOKE ALL ON SEQUENCES  FROM placity;
ALTER DEFAULT PRIVILEGES FOR ROLE placity GRANT ALL ON SEQUENCES  TO placity;
ALTER DEFAULT PRIVILEGES FOR ROLE placity GRANT ALL ON SEQUENCES  TO PUBLIC;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: placity
--

ALTER DEFAULT PRIVILEGES FOR ROLE placity REVOKE ALL ON TABLES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE placity REVOKE ALL ON TABLES  FROM placity;
ALTER DEFAULT PRIVILEGES FOR ROLE placity GRANT ALL ON TABLES  TO placity;
ALTER DEFAULT PRIVILEGES FOR ROLE placity GRANT ALL ON TABLES  TO PUBLIC;


--
-- PostgreSQL database dump complete
--

