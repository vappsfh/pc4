#!/bin/bash
# ------------------------------------------------------------------------#
# Setup DreamFactory / ffmpeg / postgreSQL                                #
# ------------------------------------------------------------------------# 
# Author: Alexander Weiß                                                  # 
# 15.01.2016                                                              #
# ------------------------------------------------------------------------# 
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        # 
#                                                                         # 
# Permission to use,copy, modify, and/or distribute this software for any # 
# purpose with or without fee is hereby granted, provided that the above  # 
# copyright notice and this permission notice appear in all copies.       # 
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  # 
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        # 
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR # 
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  # 
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   # 
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF # 
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          # 
# #########################################################################
## highlighting with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' ; }      #red
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' ; }      #yellow
function myecho()    { cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' ; } 	  #green	 

exec &2> >(tee -a errorlog) | logError 

export -f logError
export -f highlight
export -f myecho


function installDPKG(){
        echo "Installiere $1" | highlight
        apt-get -y -qq install $1 2> >(logError)
        exitcode=$?
        if [ $exitcode  -gt  0 ]
        then
                echo "Installation nicht erfolgreich bei Paket $1" | logError | tee -a errorlog
        else
                echo "Installation erfolgreich bei Paket $1" | myecho
        fi
        echo "$exitcode $1" | tee -a log | myecho
}



highlight<<YELLOW
###########################################################################
## Install 3rd party Components 
## ----------------------------
## Webserver-Components: 
## 			php5	5.
##   	 apache2 	2.
## 	  postgreSQL	9.3
## 	  phpPGadmin	 
## 	  	 openssh 	
## 	
## Middleware:	v8js node.js npm composer grunt yo bower openssl
## Tools: 		git curl vim git g++ cpp	
###########################################################################
YELLOW

highlight<<eof
## update dpkg-list / upgrade 
eof
apt-get -y update
apt-get -y dist-upgrade
highlight<<eof
## dependencies
eof

installDPKG curl 
installDPKG vim 
installDPKG git 
installDPKG g++ 
installDPKG cpp 
installDPKG jq
installDPKG crudini
installDPKG gzip 
installDPKG bzip2
installDPKG tar
installDPKG mercurial 
installDPKG svn



highlight<<eof
## php5 
eof

installDPKG php5
installDPKG php5-ldap
installDPKG php5-imagick 
installDPKG php5-gd 
installDPKG php5-imap 
installDPKG php5-curl 					
installDPKG php5-mysql
installDPKG php5-sqlite
installDPKG php5-pgsql 
installDPKG php5-sybase 
installDPKG php-pear 
installDPKG php5-common 
installDPKG php5-cli 
installDPKG php5-json 
installDPKG php5-mcrypt 
installDPKG php5-dev 
installDPKG libv8-dev 
installDPKG php5-cli


highlight<<eof
## apache2 
eof

installDPKG apache2 
installDPKG apache2-doc 



highlight<<eof
## postgresql-9.3 
eof
installDPKG postgresql-9.3
installDPKG postgresql
installDPKG postgresql-client-common 
installDPKG postgresql-doc
installDPKG postgresql-common
installDPKG postgresql-client
installDPKG postgresql-contrib
installDPKG libpq5
installDPKG postgresql-contrib-9.3 
installDPKG postgresql-client-9.3
installDPKG libossp-uuid16 
installDPKG ssl-cert 
installDPKG libxslt1.1 
installDPKG postgresql-doc-9.3


highlight<<eof
## phpadmin 
eof
installDPKG phppgadmin


highlight<<eof
## v8js node.js npm composer grunt yo bower
eof
apt-get install -f
apt-get update

curl -sL https://deb.nodesource.com/setup_5.x | bash -
apt-get install -y -qq nodejs
if [ $? -gt 0 ] ; 
	then 
		echo "Installation nicht erfolgreich bei Paket nodejs" | logError | tee -a errorlog 
	else
		echo "Installation erfolgreich bei Paket nodejs" | highlight
	fi
apt-get install -y npm
if [ $? -gt 0 ] ; 
	then 
		echo "Installation nicht erfolgreich bei Paket npm" | logError | tee -a errorlog 
	else
		echo "Installation erfolgreich bei Paket npm" | highlight
	fi
npm install --global yo  
npm install --global bower
npm install --global grunt grunt-cli

				
curl -sS https://getcomposer.org/installer | php 
mv composer.phar /usr/local/bin/composer 

pecl install v8js-0.1.3
touch /etc/php5/mods-available/v8js.ini
echo extension=v8js.so >> /etc/php5/apache2/php.ini
echo extension=v8js.so >> /etc/php5/cli/php.ini

apt-get -y autoremove

