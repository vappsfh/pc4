--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.user_to_app_to_role DROP CONSTRAINT user_to_app_to_role_user_id_foreign;
ALTER TABLE ONLY public.user_to_app_to_role DROP CONSTRAINT user_to_app_to_role_role_id_foreign;
ALTER TABLE ONLY public.user_to_app_to_role DROP CONSTRAINT user_to_app_to_role_app_id_foreign;
ALTER TABLE ONLY public.user_lookup DROP CONSTRAINT user_lookup_user_id_foreign;
ALTER TABLE ONLY public.user_lookup DROP CONSTRAINT user_lookup_last_modified_by_id_foreign;
ALTER TABLE ONLY public.user_lookup DROP CONSTRAINT user_lookup_created_by_id_foreign;
ALTER TABLE ONLY public."user" DROP CONSTRAINT user_last_modified_by_id_foreign;
ALTER TABLE ONLY public.user_custom DROP CONSTRAINT user_custom_user_id_foreign;
ALTER TABLE ONLY public."user" DROP CONSTRAINT user_created_by_id_foreign;
ALTER TABLE ONLY public.user_config DROP CONSTRAINT user_config_service_id_foreign;
ALTER TABLE ONLY public.token_map DROP CONSTRAINT token_map_user_id_foreign;
ALTER TABLE ONLY public.system_setting DROP CONSTRAINT system_setting_last_modified_by_id_foreign;
ALTER TABLE ONLY public.system_setting DROP CONSTRAINT system_setting_created_by_id_foreign;
ALTER TABLE ONLY public.system_lookup DROP CONSTRAINT system_lookup_last_modified_by_id_foreign;
ALTER TABLE ONLY public.system_lookup DROP CONSTRAINT system_lookup_created_by_id_foreign;
ALTER TABLE ONLY public.system_custom DROP CONSTRAINT system_custom_last_modified_by_id_foreign;
ALTER TABLE ONLY public.system_custom DROP CONSTRAINT system_custom_created_by_id_foreign;
ALTER TABLE ONLY public.system_config DROP CONSTRAINT system_config_last_modified_by_id_foreign;
ALTER TABLE ONLY public.system_config DROP CONSTRAINT system_config_created_by_id_foreign;
ALTER TABLE ONLY public.page DROP CONSTRAINT successor_fk;
ALTER TABLE ONLY public.sql_db_config DROP CONSTRAINT sql_db_config_service_id_foreign;
ALTER TABLE ONLY public.soap_config DROP CONSTRAINT soap_config_service_id_foreign;
ALTER TABLE ONLY public.smtp_config DROP CONSTRAINT smtp_config_service_id_foreign;
ALTER TABLE ONLY public.service DROP CONSTRAINT service_type_foreign;
ALTER TABLE ONLY public.service DROP CONSTRAINT service_last_modified_by_id_foreign;
ALTER TABLE ONLY public.service_doc DROP CONSTRAINT service_doc_service_id_foreign;
ALTER TABLE ONLY public.service DROP CONSTRAINT service_created_by_id_foreign;
ALTER TABLE ONLY public.service_cache_config DROP CONSTRAINT service_cache_config_service_id_foreign;
ALTER TABLE ONLY public.script_config DROP CONSTRAINT script_config_type_foreign;
ALTER TABLE ONLY public.script_config DROP CONSTRAINT script_config_service_id_foreign;
ALTER TABLE ONLY public.salesforce_db_config DROP CONSTRAINT salesforce_db_config_service_id_foreign;
ALTER TABLE ONLY public.rws_parameters_config DROP CONSTRAINT rws_parameters_config_service_id_foreign;
ALTER TABLE ONLY public.rws_headers_config DROP CONSTRAINT rws_headers_config_service_id_foreign;
ALTER TABLE ONLY public.rws_config DROP CONSTRAINT rws_config_service_id_foreign;
ALTER TABLE ONLY public.page DROP CONSTRAINT route_fk;
ALTER TABLE ONLY public.routentag DROP CONSTRAINT route_fk;
ALTER TABLE ONLY public.highscore DROP CONSTRAINT route_fk;
ALTER TABLE ONLY public.role_service_access DROP CONSTRAINT role_service_access_service_id_foreign;
ALTER TABLE ONLY public.role_service_access DROP CONSTRAINT role_service_access_role_id_foreign;
ALTER TABLE ONLY public.role_service_access DROP CONSTRAINT role_service_access_last_modified_by_id_foreign;
ALTER TABLE ONLY public.role_service_access DROP CONSTRAINT role_service_access_created_by_id_foreign;
ALTER TABLE ONLY public.role_lookup DROP CONSTRAINT role_lookup_role_id_foreign;
ALTER TABLE ONLY public.role_lookup DROP CONSTRAINT role_lookup_last_modified_by_id_foreign;
ALTER TABLE ONLY public.role_lookup DROP CONSTRAINT role_lookup_created_by_id_foreign;
ALTER TABLE ONLY public.role DROP CONSTRAINT role_last_modified_by_id_foreign;
ALTER TABLE ONLY public.role DROP CONSTRAINT role_created_by_id_foreign;
ALTER TABLE ONLY public.role_adldap DROP CONSTRAINT role_adldap_role_id_foreign;
ALTER TABLE ONLY public.rackspace_config DROP CONSTRAINT rackspace_config_service_id_foreign;
ALTER TABLE ONLY public.content DROP CONSTRAINT page_fk;
ALTER TABLE ONLY public."group" DROP CONSTRAINT orga_fk;
ALTER TABLE ONLY public.route DROP CONSTRAINT orga_fk;
ALTER TABLE ONLY public.orgamember DROP CONSTRAINT orga_fk;
ALTER TABLE ONLY public.address DROP CONSTRAINT orga_fk;
ALTER TABLE ONLY public.filesharedfororga DROP CONSTRAINT orga_fk;
ALTER TABLE ONLY public.oauth_config DROP CONSTRAINT oauth_config_service_id_foreign;
ALTER TABLE ONLY public.oauth_config DROP CONSTRAINT oauth_config_default_role_foreign;
ALTER TABLE ONLY public.routentag DROP CONSTRAINT name_fk;
ALTER TABLE ONLY public.highscore DROP CONSTRAINT name_fk;
ALTER TABLE ONLY public.mongodb_config DROP CONSTRAINT mongodb_config_service_id_foreign;
ALTER TABLE ONLY public.ldap_config DROP CONSTRAINT ldap_config_service_id_foreign;
ALTER TABLE ONLY public.ldap_config DROP CONSTRAINT ldap_config_default_role_foreign;
ALTER TABLE ONLY public.groupmember DROP CONSTRAINT group_fk;
ALTER TABLE ONLY public.filesharedforgroup DROP CONSTRAINT group_fk;
ALTER TABLE ONLY public.one_time_access_code DROP CONSTRAINT group_fk;
ALTER TABLE ONLY public.file_service_config DROP CONSTRAINT file_service_config_service_id_foreign;
ALTER TABLE ONLY public.event_subscriber DROP CONSTRAINT event_subscriber_last_modified_by_id_foreign;
ALTER TABLE ONLY public.event_subscriber DROP CONSTRAINT event_subscriber_created_by_id_foreign;
ALTER TABLE ONLY public.event_script DROP CONSTRAINT event_script_type_foreign;
ALTER TABLE ONLY public.event_script DROP CONSTRAINT event_script_last_modified_by_id_foreign;
ALTER TABLE ONLY public.event_script DROP CONSTRAINT event_script_created_by_id_foreign;
ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_last_modified_by_id_foreign;
ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_created_by_id_foreign;
ALTER TABLE ONLY public.email_parameters_config DROP CONSTRAINT email_parameters_config_service_id_foreign;
ALTER TABLE ONLY public.db_table_extras DROP CONSTRAINT db_table_extras_service_id_foreign;
ALTER TABLE ONLY public.db_table_extras DROP CONSTRAINT db_table_extras_last_modified_by_id_foreign;
ALTER TABLE ONLY public.db_table_extras DROP CONSTRAINT db_table_extras_created_by_id_foreign;
ALTER TABLE ONLY public.db_relationship_extras DROP CONSTRAINT db_relationship_extras_service_id_foreign;
ALTER TABLE ONLY public.db_relationship_extras DROP CONSTRAINT db_relationship_extras_last_modified_by_id_foreign;
ALTER TABLE ONLY public.db_relationship_extras DROP CONSTRAINT db_relationship_extras_created_by_id_foreign;
ALTER TABLE ONLY public.db_field_extras DROP CONSTRAINT db_field_extras_service_id_foreign;
ALTER TABLE ONLY public.db_field_extras DROP CONSTRAINT db_field_extras_ref_service_id_foreign;
ALTER TABLE ONLY public.db_field_extras DROP CONSTRAINT db_field_extras_last_modified_by_id_foreign;
ALTER TABLE ONLY public.db_field_extras DROP CONSTRAINT db_field_extras_created_by_id_foreign;
ALTER TABLE ONLY public.couchdb_config DROP CONSTRAINT couchdb_config_service_id_foreign;
ALTER TABLE ONLY public.cors_config DROP CONSTRAINT cors_config_last_modified_by_id_foreign;
ALTER TABLE ONLY public.cors_config DROP CONSTRAINT cors_config_created_by_id_foreign;
ALTER TABLE ONLY public.content DROP CONSTRAINT contenttype_fk;
ALTER TABLE ONLY public.cloud_email_config DROP CONSTRAINT cloud_email_config_service_id_foreign;
ALTER TABLE ONLY public.aws_config DROP CONSTRAINT aws_config_service_id_foreign;
ALTER TABLE ONLY public.app_to_app_group DROP CONSTRAINT app_to_app_group_group_id_foreign;
ALTER TABLE ONLY public.app_to_app_group DROP CONSTRAINT app_to_app_group_app_id_foreign;
ALTER TABLE ONLY public.app DROP CONSTRAINT app_storage_service_id_foreign;
ALTER TABLE ONLY public.app DROP CONSTRAINT app_role_id_foreign;
ALTER TABLE ONLY public.app_lookup DROP CONSTRAINT app_lookup_last_modified_by_id_foreign;
ALTER TABLE ONLY public.app_lookup DROP CONSTRAINT app_lookup_created_by_id_foreign;
ALTER TABLE ONLY public.app_lookup DROP CONSTRAINT app_lookup_app_id_foreign;
ALTER TABLE ONLY public.app DROP CONSTRAINT app_last_modified_by_id_foreign;
ALTER TABLE ONLY public.app_group DROP CONSTRAINT app_group_last_modified_by_id_foreign;
ALTER TABLE ONLY public.app_group DROP CONSTRAINT app_group_created_by_id_foreign;
ALTER TABLE ONLY public.app DROP CONSTRAINT app_created_by_id_foreign;
DROP INDEX public.user_lookup_name_index;
DROP INDEX public.role_lookup_name_index;
DROP INDEX public.password_resets_token_index;
DROP INDEX public.password_resets_email_index;
DROP INDEX public.oauth_config_default_role_index;
DROP INDEX public.ldap_config_default_role_index;
DROP INDEX public.app_lookup_name_index;
ALTER TABLE ONLY public.user_to_app_to_role DROP CONSTRAINT user_to_app_to_role_pkey;
ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
ALTER TABLE ONLY public.user_lookup DROP CONSTRAINT user_lookup_pkey;
ALTER TABLE ONLY public."user" DROP CONSTRAINT user_email_unique;
ALTER TABLE ONLY public.user_custom DROP CONSTRAINT user_custom_pkey;
ALTER TABLE ONLY public.user_config DROP CONSTRAINT user_config_pkey;
ALTER TABLE ONLY public.tag DROP CONSTRAINT tag_pkey;
ALTER TABLE ONLY public.system_setting DROP CONSTRAINT system_setting_pkey;
ALTER TABLE ONLY public.system_setting DROP CONSTRAINT system_setting_name_unique;
ALTER TABLE ONLY public.system_resource DROP CONSTRAINT system_resource_pkey;
ALTER TABLE ONLY public.system_lookup DROP CONSTRAINT system_lookup_pkey;
ALTER TABLE ONLY public.system_lookup DROP CONSTRAINT system_lookup_name_unique;
ALTER TABLE ONLY public.system_custom DROP CONSTRAINT system_custom_pkey;
ALTER TABLE ONLY public.system_config DROP CONSTRAINT system_config_pkey;
ALTER TABLE ONLY public.sql_db_config DROP CONSTRAINT sql_db_config_pkey;
ALTER TABLE ONLY public.soap_config DROP CONSTRAINT soap_config_pkey;
ALTER TABLE ONLY public.smtp_config DROP CONSTRAINT smtp_config_pkey;
ALTER TABLE ONLY public.contenttype DROP CONSTRAINT sid;
ALTER TABLE ONLY public.service_type DROP CONSTRAINT service_type_pkey;
ALTER TABLE ONLY public.service DROP CONSTRAINT service_pkey;
ALTER TABLE ONLY public.service DROP CONSTRAINT service_name_unique;
ALTER TABLE ONLY public.service_doc DROP CONSTRAINT service_doc_pkey;
ALTER TABLE ONLY public.service_cache_config DROP CONSTRAINT service_cache_config_pkey;
ALTER TABLE ONLY public.script_type DROP CONSTRAINT script_type_pkey;
ALTER TABLE ONLY public.script_config DROP CONSTRAINT script_config_pkey;
ALTER TABLE ONLY public.salesforce_db_config DROP CONSTRAINT salesforce_db_config_pkey;
ALTER TABLE ONLY public.rws_parameters_config DROP CONSTRAINT rws_parameters_config_pkey;
ALTER TABLE ONLY public.rws_headers_config DROP CONSTRAINT rws_headers_config_pkey;
ALTER TABLE ONLY public.rws_config DROP CONSTRAINT rws_config_pkey;
ALTER TABLE ONLY public.routentag DROP CONSTRAINT routentag_pkey;
ALTER TABLE ONLY public.route DROP CONSTRAINT route_pkey;
ALTER TABLE ONLY public.role_service_access DROP CONSTRAINT role_service_access_pkey;
ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
ALTER TABLE ONLY public.role DROP CONSTRAINT role_name_unique;
ALTER TABLE ONLY public.role_lookup DROP CONSTRAINT role_lookup_pkey;
ALTER TABLE ONLY public.role_adldap DROP CONSTRAINT role_adldap_pkey;
ALTER TABLE ONLY public.role_adldap DROP CONSTRAINT role_adldap_dn_unique;
ALTER TABLE ONLY public.rackspace_config DROP CONSTRAINT rackspace_config_pkey;
ALTER TABLE ONLY public.player DROP CONSTRAINT player_pkey;
ALTER TABLE ONLY public.page DROP CONSTRAINT page_pkey;
ALTER TABLE ONLY public.organization DROP CONSTRAINT organization_pkey;
ALTER TABLE ONLY public.orgamember DROP CONSTRAINT orgamember_pkey;
ALTER TABLE ONLY public.one_time_access_code DROP CONSTRAINT one_time_access_code_pkey;
ALTER TABLE ONLY public.contenttype DROP CONSTRAINT old_id;
ALTER TABLE ONLY public.oauth_config DROP CONSTRAINT oauth_config_pkey;
ALTER TABLE ONLY public.mongodb_config DROP CONSTRAINT mongodb_config_pkey;
ALTER TABLE ONLY public.mediafile DROP CONSTRAINT mediafile_file_name_key;
ALTER TABLE ONLY public.ldap_config DROP CONSTRAINT ldap_config_pkey;
ALTER TABLE ONLY public.mediafile DROP CONSTRAINT id;
ALTER TABLE ONLY public.highscore DROP CONSTRAINT highscore_pkey;
ALTER TABLE ONLY public.groupmember DROP CONSTRAINT groupmember_pkey;
ALTER TABLE ONLY public."group" DROP CONSTRAINT group_pkey;
ALTER TABLE ONLY public.page DROP CONSTRAINT fksuccessor_id;
ALTER TABLE ONLY public.address DROP CONSTRAINT fkorg_add_id;
ALTER TABLE ONLY public.filesharedfororga DROP CONSTRAINT filesharedfororga_pkey;
ALTER TABLE ONLY public.filesharedforgroup DROP CONSTRAINT filesharedforgroup_pkey;
ALTER TABLE ONLY public.file_service_config DROP CONSTRAINT file_service_config_pkey;
ALTER TABLE ONLY public.event_subscriber DROP CONSTRAINT event_subscriber_pkey;
ALTER TABLE ONLY public.event_subscriber DROP CONSTRAINT event_subscriber_name_unique;
ALTER TABLE ONLY public.event_script DROP CONSTRAINT event_script_pkey;
ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_pkey;
ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_name_unique;
ALTER TABLE ONLY public.email_parameters_config DROP CONSTRAINT email_parameters_config_pkey;
ALTER TABLE ONLY public.db_table_extras DROP CONSTRAINT db_table_extras_pkey;
ALTER TABLE ONLY public.db_relationship_extras DROP CONSTRAINT db_relationship_extras_pkey;
ALTER TABLE ONLY public.db_field_extras DROP CONSTRAINT db_field_extras_pkey;
ALTER TABLE ONLY public.couchdb_config DROP CONSTRAINT couchdb_config_pkey;
ALTER TABLE ONLY public.cors_config DROP CONSTRAINT cors_config_pkey;
ALTER TABLE ONLY public.cors_config DROP CONSTRAINT cors_config_path_unique;
ALTER TABLE ONLY public.contenttype DROP CONSTRAINT contenttype_pkey;
ALTER TABLE ONLY public.content DROP CONSTRAINT content_pkey;
ALTER TABLE ONLY public.cloud_email_config DROP CONSTRAINT cloud_email_config_pkey;
ALTER TABLE ONLY public.aws_config DROP CONSTRAINT aws_config_pkey;
ALTER TABLE ONLY public.app_to_app_group DROP CONSTRAINT app_to_app_group_pkey;
ALTER TABLE ONLY public.app DROP CONSTRAINT app_pkey;
ALTER TABLE ONLY public.app DROP CONSTRAINT app_name_unique;
ALTER TABLE ONLY public.app_lookup DROP CONSTRAINT app_lookup_pkey;
ALTER TABLE ONLY public.app_group DROP CONSTRAINT app_group_pkey;
ALTER TABLE ONLY public.app_group DROP CONSTRAINT app_group_name_unique;
ALTER TABLE public.user_to_app_to_role ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.user_lookup ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.user_custom ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.system_setting ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.system_lookup ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.service_doc ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.service ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.rws_parameters_config ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.rws_headers_config ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.role_service_access ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.role_lookup ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.event_subscriber ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.email_template ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.email_parameters_config ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.db_table_extras ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.db_relationship_extras ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.db_field_extras ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.cors_config ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.app_to_app_group ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.app_lookup ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.app_group ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.app ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.user_to_app_to_role_id_seq;
DROP TABLE public.user_to_app_to_role;
DROP SEQUENCE public.user_lookup_id_seq;
DROP TABLE public.user_lookup;
DROP SEQUENCE public.user_id_seq;
DROP SEQUENCE public.user_custom_id_seq;
DROP TABLE public.user_custom;
DROP TABLE public.user_config;
DROP TABLE public."user";
DROP TABLE public.token_map;
DROP TABLE public.tag;
DROP SEQUENCE public.system_setting_id_seq;
DROP TABLE public.system_setting;
DROP TABLE public.system_resource;
DROP SEQUENCE public.system_lookup_id_seq;
DROP TABLE public.system_lookup;
DROP TABLE public.system_custom;
DROP TABLE public.system_config;
DROP TABLE public.sql_db_config;
DROP TABLE public.soap_config;
DROP TABLE public.smtp_config;
DROP TABLE public.service_type;
DROP SEQUENCE public.service_id_seq;
DROP SEQUENCE public.service_doc_id_seq;
DROP TABLE public.service_doc;
DROP TABLE public.service_cache_config;
DROP TABLE public.service;
DROP TABLE public.script_type;
DROP TABLE public.script_config;
DROP TABLE public.salesforce_db_config;
DROP SEQUENCE public.rws_parameters_config_id_seq;
DROP TABLE public.rws_parameters_config;
DROP SEQUENCE public.rws_headers_config_id_seq;
DROP TABLE public.rws_headers_config;
DROP TABLE public.rws_config;
DROP TABLE public.routentag;
DROP SEQUENCE public.routemodel_id_seq;
DROP TABLE public.route;
DROP SEQUENCE public.route_id_seq;
DROP SEQUENCE public.role_service_access_id_seq;
DROP TABLE public.role_service_access;
DROP SEQUENCE public.role_lookup_id_seq;
DROP TABLE public.role_lookup;
DROP SEQUENCE public.role_id_seq;
DROP TABLE public.role_adldap;
DROP TABLE public.role;
DROP TABLE public.rackspace_config;
DROP TABLE public.player;
DROP TABLE public.password_resets;
DROP TABLE public.page;
DROP SEQUENCE public.page_id_seq;
DROP TABLE public.organization;
DROP SEQUENCE public.organization_id_seq;
DROP TABLE public.orgamember;
DROP TABLE public.one_time_access_code;
DROP TABLE public.oauth_config;
DROP TABLE public.mongodb_config;
DROP TABLE public.migrations;
DROP TABLE public.mediafile;
DROP SEQUENCE public.mediafile_id_seq;
DROP TABLE public.ldap_config;
DROP TABLE public.highscore;
DROP TABLE public.groupmember;
DROP TABLE public."group";
DROP SEQUENCE public.group_id_seq;
DROP TABLE public.filesharedfororga;
DROP TABLE public.filesharedforgroup;
DROP TABLE public.file_service_config;
DROP SEQUENCE public.event_subscriber_id_seq;
DROP TABLE public.event_subscriber;
DROP TABLE public.event_script;
DROP SEQUENCE public.email_template_id_seq;
DROP TABLE public.email_template;
DROP SEQUENCE public.email_parameters_config_id_seq;
DROP TABLE public.email_parameters_config;
DROP SEQUENCE public.db_table_extras_id_seq;
DROP TABLE public.db_table_extras;
DROP SEQUENCE public.db_relationship_extras_id_seq;
DROP TABLE public.db_relationship_extras;
DROP SEQUENCE public.db_field_extras_id_seq;
DROP TABLE public.db_field_extras;
DROP TABLE public.couchdb_config;
DROP SEQUENCE public.cors_config_id_seq;
DROP TABLE public.cors_config;
DROP TABLE public.contenttype;
DROP SEQUENCE public.ct_id_seq;
DROP TABLE public.content;
DROP SEQUENCE public.content_id_seq;
DROP TABLE public.cloud_email_config;
DROP TABLE public.aws_config;
DROP SEQUENCE public.app_to_app_group_id_seq;
DROP TABLE public.app_to_app_group;
DROP SEQUENCE public.app_lookup_id_seq;
DROP TABLE public.app_lookup;
DROP SEQUENCE public.app_id_seq;
DROP SEQUENCE public.app_group_id_seq;
DROP TABLE public.app_group;
DROP TABLE public.app;
DROP TABLE public.address;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- Name: address; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE address (
    id_orga integer NOT NULL,
    street character varying(255) NOT NULL,
    postcode numeric(5,0) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL
);


ALTER TABLE public.address OWNER TO placity;

SET default_with_oids = false;

--
-- Name: app; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    api_key character varying(255),
    description character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    type integer DEFAULT 0 NOT NULL,
    path text,
    url text,
    storage_service_id integer,
    storage_container character varying(255),
    requires_fullscreen boolean DEFAULT false NOT NULL,
    allow_fullscreen_toggle boolean DEFAULT true NOT NULL,
    toggle_location character varying(64) DEFAULT 'top'::character varying NOT NULL,
    role_id integer,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.app OWNER TO placity;

--
-- Name: app_group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app_group (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255),
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.app_group OWNER TO placity;

--
-- Name: app_group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_group_id_seq OWNER TO placity;

--
-- Name: app_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_group_id_seq OWNED BY app_group.id;


--
-- Name: app_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_id_seq OWNER TO placity;

--
-- Name: app_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_id_seq OWNED BY app.id;


--
-- Name: app_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app_lookup (
    id integer NOT NULL,
    app_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.app_lookup OWNER TO placity;

--
-- Name: app_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_lookup_id_seq OWNER TO placity;

--
-- Name: app_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_lookup_id_seq OWNED BY app_lookup.id;


--
-- Name: app_to_app_group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE app_to_app_group (
    id integer NOT NULL,
    app_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.app_to_app_group OWNER TO placity;

--
-- Name: app_to_app_group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE app_to_app_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_to_app_group_id_seq OWNER TO placity;

--
-- Name: app_to_app_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE app_to_app_group_id_seq OWNED BY app_to_app_group.id;


--
-- Name: aws_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE aws_config (
    service_id integer NOT NULL,
    key text,
    secret text,
    region character varying(255)
);


ALTER TABLE public.aws_config OWNER TO placity;

--
-- Name: cloud_email_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE cloud_email_config (
    service_id integer NOT NULL,
    domain character varying(255),
    key text NOT NULL
);


ALTER TABLE public.cloud_email_config OWNER TO placity;

--
-- Name: content_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE content_id_seq
    START WITH 100
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.content_id_seq OWNER TO placity;

SET default_with_oids = true;

--
-- Name: content; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE content (
    id integer DEFAULT nextval('content_id_seq'::regclass) NOT NULL,
    id_page integer,
    id_content_type integer NOT NULL,
    data_obj json,
    pos integer
);


ALTER TABLE public.content OWNER TO placity;

--
-- Name: ct_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE ct_id_seq
    START WITH 100
    INCREMENT BY 1
    MINVALUE 2
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ct_id_seq OWNER TO placity;

SET default_with_oids = false;

--
-- Name: contenttype; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE contenttype (
    id integer DEFAULT nextval('ct_id_seq'::regclass) NOT NULL,
    sid integer,
    old_id integer,
    name text NOT NULL,
    description text,
    app_ctrl_class text,
    app_html_tpl_file text,
    qe_ctrl_class text,
    qe_html_tpl_file text,
    data_schema json,
    helptext json
);


ALTER TABLE public.contenttype OWNER TO placity;

--
-- Name: COLUMN contenttype.name; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.name IS 'Name der Instanz des Entitytypen ContentType. Eine Entity des Entitytypen ContentType klassifiziert eine Gruppe von gleich strukturierten Contentinstanzen; ähnlich einer Klassendefinition in objektorientierten Programmiersprachen.   ';


--
-- Name: COLUMN contenttype.description; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.description IS 'Multiligualität fehlt hier. ';


--
-- Name: COLUMN contenttype.app_ctrl_class; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.app_ctrl_class IS 'Wiedergabe Controller AngularJS';


--
-- Name: COLUMN contenttype.app_html_tpl_file; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.app_html_tpl_file IS 'Wiedergabe Struktur';


--
-- Name: COLUMN contenttype.qe_ctrl_class; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.qe_ctrl_class IS 'Eingabeformular Controller AngularJS ';


--
-- Name: COLUMN contenttype.qe_html_tpl_file; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN contenttype.qe_html_tpl_file IS 'Eingabeformular';


--
-- Name: cors_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE cors_config (
    id integer NOT NULL,
    path character varying(255) NOT NULL,
    origin character varying(255) NOT NULL,
    header text NOT NULL,
    method integer DEFAULT 0 NOT NULL,
    max_age integer DEFAULT 3600 NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.cors_config OWNER TO placity;

--
-- Name: cors_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE cors_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cors_config_id_seq OWNER TO placity;

--
-- Name: cors_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE cors_config_id_seq OWNED BY cors_config.id;


--
-- Name: couchdb_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE couchdb_config (
    service_id integer NOT NULL,
    dsn character varying(255) DEFAULT '0'::character varying,
    options text
);


ALTER TABLE public.couchdb_config OWNER TO placity;

--
-- Name: db_field_extras; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE db_field_extras (
    id integer NOT NULL,
    service_id integer NOT NULL,
    "table" character varying(255) NOT NULL,
    field character varying(255) NOT NULL,
    label character varying(255),
    extra_type character varying(255),
    description text,
    picklist text,
    validation text,
    client_info text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer,
    alias character varying(255),
    db_function text,
    ref_service_id integer,
    ref_table character varying(255),
    ref_fields character varying(255),
    ref_on_update character varying(255),
    ref_on_delete character varying(255)
);


ALTER TABLE public.db_field_extras OWNER TO placity;

--
-- Name: db_field_extras_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE db_field_extras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.db_field_extras_id_seq OWNER TO placity;

--
-- Name: db_field_extras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE db_field_extras_id_seq OWNED BY db_field_extras.id;


--
-- Name: db_relationship_extras; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE db_relationship_extras (
    id integer NOT NULL,
    service_id integer NOT NULL,
    "table" character varying(255) NOT NULL,
    relationship character varying(255) NOT NULL,
    alias character varying(255),
    label character varying(255),
    description text,
    always_fetch boolean DEFAULT false NOT NULL,
    flatten boolean DEFAULT false NOT NULL,
    flatten_drop_prefix boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.db_relationship_extras OWNER TO placity;

--
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE db_relationship_extras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.db_relationship_extras_id_seq OWNER TO placity;

--
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE db_relationship_extras_id_seq OWNED BY db_relationship_extras.id;


--
-- Name: db_table_extras; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE db_table_extras (
    id integer NOT NULL,
    service_id integer NOT NULL,
    "table" character varying(255) NOT NULL,
    label character varying(255),
    plural character varying(255),
    name_field character varying(128),
    model character varying(255),
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer,
    alias character varying(255)
);


ALTER TABLE public.db_table_extras OWNER TO placity;

--
-- Name: db_table_extras_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE db_table_extras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.db_table_extras_id_seq OWNER TO placity;

--
-- Name: db_table_extras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE db_table_extras_id_seq OWNED BY db_table_extras.id;


--
-- Name: email_parameters_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE email_parameters_config (
    id integer NOT NULL,
    service_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE public.email_parameters_config OWNER TO placity;

--
-- Name: email_parameters_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE email_parameters_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_parameters_config_id_seq OWNER TO placity;

--
-- Name: email_parameters_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE email_parameters_config_id_seq OWNED BY email_parameters_config.id;


--
-- Name: email_template; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE email_template (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255),
    "to" text,
    cc text,
    bcc text,
    subject character varying(80),
    body_text text,
    body_html text,
    from_name character varying(80),
    from_email character varying(255),
    reply_to_name character varying(80),
    reply_to_email character varying(255),
    defaults text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.email_template OWNER TO placity;

--
-- Name: email_template_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE email_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_template_id_seq OWNER TO placity;

--
-- Name: email_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE email_template_id_seq OWNED BY email_template.id;


--
-- Name: event_script; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE event_script (
    name character varying(80) NOT NULL,
    type character varying(40) NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    affects_process boolean DEFAULT false NOT NULL,
    content text,
    config text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.event_script OWNER TO placity;

--
-- Name: event_subscriber; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE event_subscriber (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    type character varying(255) NOT NULL,
    config text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.event_subscriber OWNER TO placity;

--
-- Name: event_subscriber_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE event_subscriber_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_subscriber_id_seq OWNER TO placity;

--
-- Name: event_subscriber_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE event_subscriber_id_seq OWNED BY event_subscriber.id;


--
-- Name: file_service_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE file_service_config (
    service_id integer NOT NULL,
    public_path text,
    container text
);


ALTER TABLE public.file_service_config OWNER TO placity;

SET default_with_oids = true;

--
-- Name: filesharedforgroup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE filesharedforgroup (
    id_group integer NOT NULL,
    id_file integer NOT NULL
);


ALTER TABLE public.filesharedforgroup OWNER TO placity;

--
-- Name: filesharedfororga; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE filesharedfororga (
    id_file integer NOT NULL,
    id_orga integer NOT NULL
);


ALTER TABLE public.filesharedfororga OWNER TO placity;

--
-- Name: group_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE group_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_id_seq OWNER TO placity;

--
-- Name: group; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE "group" (
    id integer DEFAULT nextval('group_id_seq'::regclass) NOT NULL,
    groupname text NOT NULL,
    group_admin integer NOT NULL,
    id_orga integer,
    description text
);


ALTER TABLE public."group" OWNER TO placity;

--
-- Name: groupmember; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE groupmember (
    id_group integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.groupmember OWNER TO placity;

--
-- Name: highscore; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE highscore (
    name text NOT NULL,
    id_route integer NOT NULL,
    points numeric(7,7) NOT NULL,
    date date DEFAULT now() NOT NULL
);


ALTER TABLE public.highscore OWNER TO placity;

SET default_with_oids = false;

--
-- Name: ldap_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE ldap_config (
    service_id integer NOT NULL,
    default_role integer NOT NULL,
    host character varying(255) NOT NULL,
    base_dn character varying(255) NOT NULL,
    account_suffix character varying(255),
    map_group_to_role boolean DEFAULT false NOT NULL,
    username character varying(50),
    password text
);


ALTER TABLE public.ldap_config OWNER TO placity;

--
-- Name: mediafile_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE mediafile_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mediafile_id_seq OWNER TO placity;

SET default_with_oids = true;

--
-- Name: mediafile; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE mediafile (
    id integer DEFAULT nextval('mediafile_id_seq'::regclass) NOT NULL,
    file_name text NOT NULL,
    path text NOT NULL,
    url text,
    public integer,
    file_type text NOT NULL,
    filesize integer,
    id_user integer NOT NULL,
    encoding boolean DEFAULT false,
    encoded boolean DEFAULT false
);


ALTER TABLE public.mediafile OWNER TO placity;

--
-- Name: COLUMN mediafile.encoding; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN mediafile.encoding IS 'true, solange Datei in kompatibles Format überführt wird. ';


--
-- Name: COLUMN mediafile.encoded; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN mediafile.encoded IS 'true, wenn Transkodierung abgeschlossen, oder Format erfolgrich auf Kompatibilität geprüft wurde. ';


SET default_with_oids = false;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO placity;

--
-- Name: mongodb_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE mongodb_config (
    service_id integer NOT NULL,
    dsn character varying(255) DEFAULT '0'::character varying,
    options text,
    driver_options text
);


ALTER TABLE public.mongodb_config OWNER TO placity;

--
-- Name: oauth_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE oauth_config (
    service_id integer NOT NULL,
    default_role integer NOT NULL,
    client_id character varying(255) NOT NULL,
    client_secret text NOT NULL,
    redirect_url character varying(255) NOT NULL,
    icon_class character varying(255)
);


ALTER TABLE public.oauth_config OWNER TO placity;

SET default_with_oids = true;

--
-- Name: one_time_access_code; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE one_time_access_code (
    code text NOT NULL,
    id_orga integer NOT NULL,
    id_group integer NOT NULL
);


ALTER TABLE public.one_time_access_code OWNER TO placity;

--
-- Name: orgamember; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE orgamember (
    id_orga integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.orgamember OWNER TO placity;

--
-- Name: organization_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE organization_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organization_id_seq OWNER TO placity;

--
-- Name: organization; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE organization (
    id integer DEFAULT nextval('organization_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    description text,
    orga_admin integer NOT NULL
);


ALTER TABLE public.organization OWNER TO placity;

--
-- Name: page_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE page_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_id_seq OWNER TO placity;

--
-- Name: page; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE page (
    id integer DEFAULT nextval('page_id_seq'::regclass) NOT NULL,
    pos integer,
    id_route integer,
    next integer,
    page_name text NOT NULL
);


ALTER TABLE public.page OWNER TO placity;

--
-- Name: TABLE page; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON TABLE page IS 'Page ist eine Seite einer bestimmten Route. Eine Page enthält verschiedene Instanzen vom Content ';


SET default_with_oids = false;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.password_resets OWNER TO placity;

SET default_with_oids = true;

--
-- Name: player; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE player (
    name text NOT NULL,
    avatar_img text
);


ALTER TABLE public.player OWNER TO placity;

--
-- Name: COLUMN player.avatar_img; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN player.avatar_img IS 'base64';


SET default_with_oids = false;

--
-- Name: rackspace_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rackspace_config (
    service_id integer NOT NULL,
    username character varying(255),
    password text,
    tenant_name character varying(255),
    api_key text,
    url character varying(255),
    region character varying(255),
    storage_type character varying(255)
);


ALTER TABLE public.rackspace_config OWNER TO placity;

--
-- Name: role; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.role OWNER TO placity;

--
-- Name: role_adldap; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role_adldap (
    role_id integer NOT NULL,
    dn character varying(255) NOT NULL
);


ALTER TABLE public.role_adldap OWNER TO placity;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO placity;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: role_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role_lookup (
    id integer NOT NULL,
    role_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.role_lookup OWNER TO placity;

--
-- Name: role_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE role_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_lookup_id_seq OWNER TO placity;

--
-- Name: role_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE role_lookup_id_seq OWNED BY role_lookup.id;


--
-- Name: role_service_access; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE role_service_access (
    id integer NOT NULL,
    role_id integer NOT NULL,
    service_id integer,
    component character varying(255),
    verb_mask integer DEFAULT 0 NOT NULL,
    requestor_mask integer DEFAULT 0 NOT NULL,
    filters text,
    filter_op character varying(32) DEFAULT 'and'::character varying NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.role_service_access OWNER TO placity;

--
-- Name: role_service_access_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE role_service_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_service_access_id_seq OWNER TO placity;

--
-- Name: role_service_access_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE role_service_access_id_seq OWNED BY role_service_access.id;


--
-- Name: route_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE route_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.route_id_seq OWNER TO placity;

SET default_with_oids = true;

--
-- Name: route; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE route (
    id integer DEFAULT nextval('route_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    title text,
    subtitle text,
    description text,
    lang text,
    image text,
    id_user integer NOT NULL,
    id_orga integer,
    id_group integer,
    created date DEFAULT now(),
    private boolean DEFAULT true NOT NULL,
    app_scheme text DEFAULT 'APP_SCHEME'::text NOT NULL,
    android_scheme text DEFAULT 'ANDROID_SCHEME'::text NOT NULL,
    win_scheme text DEFAULT 'ANDROID_SCHEME'::text NOT NULL
);


ALTER TABLE public.route OWNER TO placity;

--
-- Name: COLUMN route.image; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.image IS 'encoded in base64';


--
-- Name: COLUMN route.app_scheme; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.app_scheme IS 'todo: App_Scheme sec. ID ';


--
-- Name: COLUMN route.android_scheme; Type: COMMENT; Schema: public; Owner: placity
--

COMMENT ON COLUMN route.android_scheme IS 'todo: ANDROID_SCHEME sec. ID';


--
-- Name: routemodel_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE routemodel_id_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.routemodel_id_seq OWNER TO placity;

--
-- Name: routentag; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE routentag (
    id_route integer NOT NULL,
    tag_name text NOT NULL
);


ALTER TABLE public.routentag OWNER TO placity;

SET default_with_oids = false;

--
-- Name: rws_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rws_config (
    service_id integer NOT NULL,
    base_url text NOT NULL,
    options text
);


ALTER TABLE public.rws_config OWNER TO placity;

--
-- Name: rws_headers_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rws_headers_config (
    id integer NOT NULL,
    service_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    pass_from_client boolean DEFAULT false NOT NULL,
    action integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.rws_headers_config OWNER TO placity;

--
-- Name: rws_headers_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE rws_headers_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rws_headers_config_id_seq OWNER TO placity;

--
-- Name: rws_headers_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE rws_headers_config_id_seq OWNED BY rws_headers_config.id;


--
-- Name: rws_parameters_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE rws_parameters_config (
    id integer NOT NULL,
    service_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    exclude boolean DEFAULT false NOT NULL,
    outbound boolean DEFAULT false NOT NULL,
    cache_key boolean DEFAULT false NOT NULL,
    action integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.rws_parameters_config OWNER TO placity;

--
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE rws_parameters_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rws_parameters_config_id_seq OWNER TO placity;

--
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE rws_parameters_config_id_seq OWNED BY rws_parameters_config.id;


--
-- Name: salesforce_db_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE salesforce_db_config (
    service_id integer NOT NULL,
    username character varying(255),
    password text,
    security_token text
);


ALTER TABLE public.salesforce_db_config OWNER TO placity;

--
-- Name: script_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE script_config (
    service_id integer NOT NULL,
    type character varying(40) NOT NULL,
    content text,
    config text
);


ALTER TABLE public.script_config OWNER TO placity;

--
-- Name: script_type; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE script_type (
    name character varying(40) NOT NULL,
    class_name character varying(255) NOT NULL,
    label character varying(80) NOT NULL,
    description character varying(255),
    sandboxed boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.script_type OWNER TO placity;

--
-- Name: service; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service (
    id integer NOT NULL,
    name character varying(40) NOT NULL,
    label character varying(80) NOT NULL,
    description character varying(255),
    is_active boolean DEFAULT false NOT NULL,
    type character varying(40) NOT NULL,
    mutable boolean DEFAULT true NOT NULL,
    deletable boolean DEFAULT true NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.service OWNER TO placity;

--
-- Name: service_cache_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service_cache_config (
    service_id integer NOT NULL,
    cache_enabled boolean DEFAULT false NOT NULL,
    cache_ttl integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.service_cache_config OWNER TO placity;

--
-- Name: service_doc; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service_doc (
    id integer NOT NULL,
    service_id integer NOT NULL,
    format integer DEFAULT 0 NOT NULL,
    content text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.service_doc OWNER TO placity;

--
-- Name: service_doc_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE service_doc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_doc_id_seq OWNER TO placity;

--
-- Name: service_doc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE service_doc_id_seq OWNED BY service_doc.id;


--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_id_seq OWNER TO placity;

--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE service_id_seq OWNED BY service.id;


--
-- Name: service_type; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE service_type (
    name character varying(40) NOT NULL,
    class_name character varying(255) NOT NULL,
    config_handler character varying(255),
    label character varying(80) NOT NULL,
    description character varying(255),
    "group" character varying(255),
    singleton boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.service_type OWNER TO placity;

--
-- Name: smtp_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE smtp_config (
    service_id integer NOT NULL,
    host character varying(255) NOT NULL,
    port character varying(255) DEFAULT '587'::character varying NOT NULL,
    encryption character varying(255) DEFAULT 'tls'::character varying NOT NULL,
    username text NOT NULL,
    password text NOT NULL
);


ALTER TABLE public.smtp_config OWNER TO placity;

--
-- Name: soap_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE soap_config (
    service_id integer NOT NULL,
    wsdl character varying(255) DEFAULT '0'::character varying,
    options text
);


ALTER TABLE public.soap_config OWNER TO placity;

--
-- Name: sql_db_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE sql_db_config (
    service_id integer NOT NULL,
    driver character varying(32) NOT NULL,
    dsn character varying(255) NOT NULL,
    username text,
    password text,
    options text,
    attributes text,
    default_schema_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sql_db_config OWNER TO placity;

--
-- Name: system_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_config (
    db_version character varying(32) NOT NULL,
    login_with_user_name boolean DEFAULT false NOT NULL,
    default_app_id integer,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_config OWNER TO placity;

--
-- Name: system_custom; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_custom (
    name character varying(255) NOT NULL,
    value text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_custom OWNER TO placity;

--
-- Name: system_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_lookup (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_lookup OWNER TO placity;

--
-- Name: system_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE system_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_lookup_id_seq OWNER TO placity;

--
-- Name: system_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE system_lookup_id_seq OWNED BY system_lookup.id;


--
-- Name: system_resource; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_resource (
    name character varying(40) NOT NULL,
    class_name character varying(255) NOT NULL,
    label character varying(80) NOT NULL,
    description character varying(255),
    singleton boolean DEFAULT false NOT NULL,
    read_only boolean DEFAULT false NOT NULL,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.system_resource OWNER TO placity;

--
-- Name: system_setting; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE system_setting (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.system_setting OWNER TO placity;

--
-- Name: system_setting_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE system_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_setting_id_seq OWNER TO placity;

--
-- Name: system_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE system_setting_id_seq OWNED BY system_setting.id;


SET default_with_oids = true;

--
-- Name: tag; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE tag (
    tag_name character varying(255) NOT NULL
);


ALTER TABLE public.tag OWNER TO placity;

SET default_with_oids = false;

--
-- Name: token_map; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE token_map (
    user_id integer NOT NULL,
    token text NOT NULL,
    iat integer NOT NULL,
    exp integer NOT NULL
);


ALTER TABLE public.token_map OWNER TO placity;

--
-- Name: user; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    last_login_date timestamp(0) without time zone,
    email character varying(255) NOT NULL,
    password text,
    is_sys_admin boolean DEFAULT false NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    phone character varying(32),
    security_question character varying(255),
    security_answer text,
    confirm_code character varying(255),
    default_app_id integer,
    remember_token character varying(100),
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer,
    oauth_provider character varying(50),
    adldap character varying(50)
);


ALTER TABLE public."user" OWNER TO placity;

--
-- Name: user_config; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_config (
    service_id integer NOT NULL,
    allow_open_registration boolean DEFAULT false NOT NULL,
    open_reg_role_id integer,
    open_reg_email_service_id integer,
    open_reg_email_template_id integer,
    invite_email_service_id integer,
    invite_email_template_id integer,
    password_email_service_id integer,
    password_email_template_id integer
);


ALTER TABLE public.user_config OWNER TO placity;

--
-- Name: user_custom; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_custom (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.user_custom OWNER TO placity;

--
-- Name: user_custom_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_custom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_custom_id_seq OWNER TO placity;

--
-- Name: user_custom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_custom_id_seq OWNED BY user_custom.id;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO placity;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_lookup; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_lookup (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    private boolean DEFAULT false NOT NULL,
    description text,
    created_date timestamp(0) without time zone NOT NULL,
    last_modified_date timestamp(0) without time zone NOT NULL,
    created_by_id integer,
    last_modified_by_id integer
);


ALTER TABLE public.user_lookup OWNER TO placity;

--
-- Name: user_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_lookup_id_seq OWNER TO placity;

--
-- Name: user_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_lookup_id_seq OWNED BY user_lookup.id;


--
-- Name: user_to_app_to_role; Type: TABLE; Schema: public; Owner: placity; Tablespace: 
--

CREATE TABLE user_to_app_to_role (
    id integer NOT NULL,
    user_id integer NOT NULL,
    app_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.user_to_app_to_role OWNER TO placity;

--
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE; Schema: public; Owner: placity
--

CREATE SEQUENCE user_to_app_to_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_to_app_to_role_id_seq OWNER TO placity;

--
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: placity
--

ALTER SEQUENCE user_to_app_to_role_id_seq OWNED BY user_to_app_to_role.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app ALTER COLUMN id SET DEFAULT nextval('app_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_group ALTER COLUMN id SET DEFAULT nextval('app_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup ALTER COLUMN id SET DEFAULT nextval('app_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_to_app_group ALTER COLUMN id SET DEFAULT nextval('app_to_app_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cors_config ALTER COLUMN id SET DEFAULT nextval('cors_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras ALTER COLUMN id SET DEFAULT nextval('db_field_extras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras ALTER COLUMN id SET DEFAULT nextval('db_relationship_extras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras ALTER COLUMN id SET DEFAULT nextval('db_table_extras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_parameters_config ALTER COLUMN id SET DEFAULT nextval('email_parameters_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_template ALTER COLUMN id SET DEFAULT nextval('email_template_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_subscriber ALTER COLUMN id SET DEFAULT nextval('event_subscriber_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup ALTER COLUMN id SET DEFAULT nextval('role_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access ALTER COLUMN id SET DEFAULT nextval('role_service_access_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_headers_config ALTER COLUMN id SET DEFAULT nextval('rws_headers_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_parameters_config ALTER COLUMN id SET DEFAULT nextval('rws_parameters_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service ALTER COLUMN id SET DEFAULT nextval('service_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service_doc ALTER COLUMN id SET DEFAULT nextval('service_doc_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_lookup ALTER COLUMN id SET DEFAULT nextval('system_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_setting ALTER COLUMN id SET DEFAULT nextval('system_setting_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_custom ALTER COLUMN id SET DEFAULT nextval('user_custom_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup ALTER COLUMN id SET DEFAULT nextval('user_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role ALTER COLUMN id SET DEFAULT nextval('user_to_app_to_role_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: app; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO app VALUES (1, 'admin', '6498a8ad1beb9d84d63035c5d1120c007fad6de706734db9689f8996707e0f7d', 'An application for administering this instance.', true, 3, 'dreamfactory/dist/index.html', NULL, NULL, NULL, false, true, 'top', NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL);
INSERT INTO app VALUES (2, 'api_docs', '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88', 'A Swagger-base application allowing documentation viewing and testing of the API.', true, 3, 'df-swagger-ui/dist/index.html', NULL, NULL, NULL, false, true, 'top', NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL);
INSERT INTO app VALUES (3, 'file_manager', 'b5cb82af7b5d4130f36149f90aa2746782e59a872ac70454ac188743cb55b0ba', 'An application for managing file services.', true, 3, 'filemanager/index.html', NULL, NULL, NULL, false, true, 'top', NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL);
INSERT INTO app VALUES (5, 'QuestionEditor', '44bd772342b15c8fa1ff3e8f92ea825fb308cfe235470d5489cf54b4c7e9a076', 'Questin-Editor', true, 0, NULL, 'http://143.93.91.92/steffen/index.html', NULL, NULL, false, true, 'top', NULL, '2016-02-16 02:32:09', '2016-02-16 02:32:09', 1, NULL);
INSERT INTO app VALUES (6, 'placity-app', '1d4342bea76dec0f1fb8eb79e786773415a5057182526550400843d4097b735f', '', true, 0, NULL, 'index.html', NULL, NULL, false, true, 'top', NULL, '2016-02-16 02:32:52', '2016-02-16 02:32:52', 1, NULL);


--
-- Data for Name: app_group; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: app_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_group_id_seq', 1, false);


--
-- Name: app_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_id_seq', 6, true);


--
-- Data for Name: app_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: app_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_lookup_id_seq', 1, false);


--
-- Data for Name: app_to_app_group; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: app_to_app_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_to_app_group_id_seq', 1, false);


--
-- Data for Name: aws_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: cloud_email_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: content; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('content_id_seq', 100, false);


--
-- Data for Name: contenttype; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: cors_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: cors_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('cors_config_id_seq', 1, false);


--
-- Data for Name: couchdb_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: ct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('ct_id_seq', 100, false);


--
-- Data for Name: db_field_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO db_field_extras VALUES (1, 1, 'sql_db_config', 'dsn', 'Connection String (DSN)', NULL, 'Specify the connection string for the database you''re connecting to.', NULL, NULL, NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras VALUES (2, 1, 'sql_db_config', 'options', 'Driver Options', NULL, 'A key=>value array of driver-specific connection options.', NULL, NULL, NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras VALUES (3, 1, 'sql_db_config', 'attributes', 'Driver Attributes', NULL, 'A key=>value array of driver-specific attributes.', NULL, NULL, NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Name: db_field_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_field_extras_id_seq', 3, true);


--
-- Data for Name: db_relationship_extras; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_relationship_extras_id_seq', 1, false);


--
-- Data for Name: db_table_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO db_table_extras VALUES (1, 1, 'user', NULL, NULL, NULL, 'DreamFactory\Core\Models\User', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (2, 1, 'user_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\UserLookup', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (3, 1, 'user_to_app_to_role', NULL, NULL, NULL, 'DreamFactory\Core\Models\UserAppRole', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (4, 1, 'service', NULL, NULL, NULL, 'DreamFactory\Core\Models\Service', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (5, 1, 'service_type', NULL, NULL, NULL, 'DreamFactory\Core\Models\ServiceType', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (6, 1, 'service_doc', NULL, NULL, NULL, 'DreamFactory\Core\Models\ServiceDoc', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (7, 1, 'role', NULL, NULL, NULL, 'DreamFactory\Core\Models\Role', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (8, 1, 'role_service_access', NULL, NULL, NULL, 'DreamFactory\Core\Models\RoleServiceAccess', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (9, 1, 'role_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\RoleLookup', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (10, 1, 'app', NULL, NULL, NULL, 'DreamFactory\Core\Models\App', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (11, 1, 'app_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\AppLookup', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (12, 1, 'app_group', NULL, NULL, NULL, 'DreamFactory\Core\Models\AppGroup', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (13, 1, 'app_to_app_group', NULL, NULL, NULL, 'DreamFactory\Core\Models\AppToAppGroup', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (14, 1, 'system_resource', NULL, NULL, NULL, 'DreamFactory\Core\Models\SystemResource', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (15, 1, 'script_type', NULL, NULL, NULL, 'DreamFactory\Core\Models\ScriptType', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (16, 1, 'event_script', NULL, NULL, NULL, 'DreamFactory\Core\Models\EventScript', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (17, 1, 'event_subscriber', NULL, NULL, NULL, 'DreamFactory\Core\Models\EventSubscriber', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (18, 1, 'email_template', NULL, NULL, NULL, 'DreamFactory\Core\Models\EmailTemplate', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (19, 1, 'system_setting', NULL, NULL, NULL, 'DreamFactory\Core\Models\Setting', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (20, 1, 'system_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\Lookup', NULL, '2016-02-09 22:05:08', '2016-02-09 22:05:08', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (21, 1, 'sql_db_config', NULL, NULL, NULL, 'DreamFactory\Core\SqlDb\Models\SqlDbConfig', NULL, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL, NULL);
INSERT INTO db_table_extras VALUES (22, 1, 'role_adldap', NULL, NULL, NULL, 'DreamFactory\Core\ADLdap\Models\RoleADLdap', NULL, '2016-02-09 22:05:12', '2016-02-09 22:05:12', NULL, NULL, NULL);


--
-- Name: db_table_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_table_extras_id_seq', 22, true);


--
-- Data for Name: email_parameters_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: email_parameters_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('email_parameters_config_id_seq', 1, false);


--
-- Data for Name: email_template; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO email_template VALUES (1, 'User Invite Default', 'Email sent to invite new users to your DreamFactory instance.', NULL, NULL, NULL, '[DF] New User Invitation', NULL, '<div style="padding: 10px;">
                                <p>
                                Hi {first_name},
                                </p>
                                <p>
                                    You have been invited to the DreamFactory Instance of {instance_name}. Go to the following url, enter the code below, and set
                                    your password to confirm your account.
                                    <br/>
                                    <br/>
                                    {link}
                                    <br/>
                                    <br/>
                                    Confirmation Code: {confirm_code}<br/>
                                </p>
                                <p>
                                    <cite>-- The Dream Team</cite>
                                </p>
                              </div>', 'DO NOT REPLY', 'no-reply@dreamfactory.com', NULL, NULL, NULL, '2016-02-09 22:05:11', '2016-02-09 22:05:11', NULL, NULL);
INSERT INTO email_template VALUES (2, 'User Registration Default', 'Email sent to new users to complete registration.', NULL, NULL, NULL, '[DF] Registration Confirmation', NULL, '<div style="padding: 10px;">
                                <p>
                                    Hi {first_name},
                                </p>
                                <p>
                                    You have registered an user account on the DreamFactory instance of {instance_name}. Go to the following url, enter the
                                    code below, and set your password to confirm your account.
                                    <br/>
                                    <br/>
                                    {link}
                                    <br/>
                                    <br/>
                                    Confirmation Code: {confirm_code}
                                    <br/>
                                </p>
                                <p>
                                    <cite>-- The Dream Team</cite>
                                </p>
                            </div>', 'DO NOT REPLY', 'no-reply@dreamfactory.com', NULL, NULL, NULL, '2016-02-09 22:05:11', '2016-02-09 22:05:11', NULL, NULL);
INSERT INTO email_template VALUES (3, 'Password Reset Default', 'Email sent to users following a request to reset their password.', NULL, NULL, NULL, '[DF] Password Reset', NULL, '<div style="padding: 10px;">
                                <p>
                                    Hi {first_name},
                                </p>
                                <p>
                                    You have requested to reset your password. Go to the following url, enter the code below, and set your new password.
                                    <br>
                                    <br>
                                    {link}
                                    <br>
                                    <br>
                                    Confirmation Code: {confirm_code}
                                </p>
                                <p>
                                    <cite>-- The Dream Team</cite>
                                </p>
                            </div>', 'DO NOT REPLY', 'no-reply@dreamfactory.com', NULL, NULL, NULL, '2016-02-09 22:05:11', '2016-02-09 22:05:11', NULL, NULL);


--
-- Name: email_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('email_template_id_seq', 3, true);


--
-- Data for Name: event_script; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: event_subscriber; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: event_subscriber_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('event_subscriber_id_seq', 1, false);


--
-- Data for Name: file_service_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: filesharedforgroup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: filesharedfororga; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: group; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('group_id_seq', 100, false);


--
-- Data for Name: groupmember; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: highscore; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: ldap_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: mediafile; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: mediafile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('mediafile_id_seq', 100, false);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO migrations VALUES ('2015_01_27_190908_create_system_tables', 1);
INSERT INTO migrations VALUES ('2015_02_03_161456_create_sqldb_tables', 1);
INSERT INTO migrations VALUES ('2015_02_03_161457_create_couchdb_tables', 1);
INSERT INTO migrations VALUES ('2015_02_03_161457_create_mongodb_tables', 1);
INSERT INTO migrations VALUES ('2015_02_03_161457_create_salesforce_tables', 1);
INSERT INTO migrations VALUES ('2015_03_10_135522_create_aws_tables', 1);
INSERT INTO migrations VALUES ('2015_03_11_143913_create_rackspace_tables', 1);
INSERT INTO migrations VALUES ('2015_03_20_205504_create_remote_web_service_tables', 1);
INSERT INTO migrations VALUES ('2015_03_20_205504_create_soap_service_tables', 1);
INSERT INTO migrations VALUES ('2015_05_02_134911_update_user_table_for_oauth_support', 1);
INSERT INTO migrations VALUES ('2015_05_04_034605_create_adldap_tables', 1);
INSERT INTO migrations VALUES ('2015_05_21_190727_create_user_config_table', 1);
INSERT INTO migrations VALUES ('2015_07_10_161839_create_user_custom_table', 1);
INSERT INTO migrations VALUES ('2015_08_24_180219_default_schema_only', 1);
INSERT INTO migrations VALUES ('2015_08_25_202632_db_alias', 1);
INSERT INTO migrations VALUES ('2015_11_06_155036_db_function', 1);
INSERT INTO migrations VALUES ('2015_11_10_225902_db_foreign_key', 1);
INSERT INTO migrations VALUES ('2015_11_17_181913_ad_enhancements', 1);
INSERT INTO migrations VALUES ('2016_01_21_213101_add_curl_options', 1);


--
-- Data for Name: mongodb_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: oauth_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: one_time_access_code; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: orgamember; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('organization_id_seq', 100, false);


--
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('page_id_seq', 100, false);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: rackspace_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: role_adldap; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_id_seq', 1, false);


--
-- Data for Name: role_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: role_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_lookup_id_seq', 1, false);


--
-- Data for Name: role_service_access; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: role_service_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_service_access_id_seq', 1, false);


--
-- Data for Name: route; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: route_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('route_id_seq', 100, false);


--
-- Name: routemodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('routemodel_id_seq', 100, false);


--
-- Data for Name: routentag; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: rws_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: rws_headers_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: rws_headers_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('rws_headers_config_id_seq', 1, false);


--
-- Data for Name: rws_parameters_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('rws_parameters_config_id_seq', 1, false);


--
-- Data for Name: salesforce_db_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: script_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: script_type; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO script_type VALUES ('php', 'DreamFactory\Core\Scripting\Engines\Php', 'PHP', 'Script handler using native PHP.', false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO script_type VALUES ('nodejs', 'DreamFactory\Core\Scripting\Engines\NodeJs', 'Node.js', 'Server-side JavaScript handler using the Node.js engine.', false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO script_type VALUES ('v8js', 'DreamFactory\Core\Scripting\Engines\V8Js', 'V8js', 'Server-side JavaScript handler using the V8js engine.', true, '2016-02-09 22:05:06', '2016-02-09 22:05:06');


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO service VALUES (1, 'system', 'System Management', 'Service for managing system resources.', true, 'system', false, false, '2016-02-09 22:05:07', '2016-02-09 22:05:07', NULL, NULL);
INSERT INTO service VALUES (2, 'api_docs', 'Live API Docs', 'API documenting and testing service.', true, 'swagger', false, false, '2016-02-09 22:05:07', '2016-02-09 22:05:07', NULL, NULL);
INSERT INTO service VALUES (3, 'files', 'Local File Storage', 'Service for accessing local file storage.', true, 'local_file', true, true, '2016-02-09 22:05:07', '2016-02-09 22:05:07', NULL, NULL);
INSERT INTO service VALUES (4, 'db', 'Local SQL Database', 'Service for accessing local SQLite database.', true, 'sql_db', true, true, '2016-02-09 22:05:09', '2016-02-09 22:05:09', NULL, NULL);
INSERT INTO service VALUES (5, 'email', 'Local Email Service', 'Email service used for sending user invites and/or password reset confirmation.', true, 'local_email', true, true, '2016-02-09 22:05:11', '2016-02-09 22:05:11', NULL, NULL);
INSERT INTO service VALUES (6, 'user', 'User Management', 'Service for managing system users.', true, 'user', true, false, '2016-02-09 22:05:11', '2016-02-09 22:05:11', NULL, NULL);


--
-- Data for Name: service_cache_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: service_doc; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: service_doc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('service_doc_id_seq', 1, false);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('service_id_seq', 7, true);


--
-- Data for Name: service_type; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO service_type VALUES ('system', 'DreamFactory\Core\Services\System', NULL, 'System Management Service', 'Service supporting management of the system.', 'System', true, '2016-02-09 22:05:04', '2016-02-09 22:05:04');
INSERT INTO service_type VALUES ('swagger', 'DreamFactory\Core\Services\Swagger', NULL, 'Swagger API Docs', 'API documenting and testing service using Swagger specifications.', 'API Doc', true, '2016-02-09 22:05:04', '2016-02-09 22:05:04');
INSERT INTO service_type VALUES ('event', 'DreamFactory\Core\Services\Event', NULL, 'Event Service', 'Service that allows clients to subscribe to system broadcast events.', 'Event', true, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('script', 'DreamFactory\Core\Services\Script', 'DreamFactory\Core\Models\ScriptConfig', 'Custom Scripting Service', 'Service that allows client-callable scripts utilizing the system scripting.', 'Custom', false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('local_file', 'DreamFactory\Core\Services\LocalFileService', 'DreamFactory\Core\Models\FilePublicPath', 'Local File Service', 'File service supporting the local file system.', 'File', false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('local_email', 'DreamFactory\Core\Services\Email\Local', 'DreamFactory\Core\Models\LocalEmailConfig', 'Local Email Service', 'Local email service using system configuration.', 'Email', false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('smtp_email', 'DreamFactory\Core\Services\Email\Smtp', 'DreamFactory\Core\Models\SmtpConfig', 'SMTP Email Service', 'SMTP-based email service', 'Email', false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('mailgun_email', 'DreamFactory\Core\Services\Email\MailGun', 'DreamFactory\Core\Models\MailGunConfig', 'Mailgun Email Service', 'Mailgun email service', 'Email', false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('mandrill_email', 'DreamFactory\Core\Services\Email\Mandrill', 'DreamFactory\Core\Models\MandrillConfig', 'Mandrill Email Service', 'Mandrill email service', 'Email', false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO service_type VALUES ('sql_db', 'DreamFactory\Core\SqlDb\Services\SqlDb', 'DreamFactory\Core\SqlDb\Models\SqlDbConfig', 'SQL DB', 'Database service supporting SQL connections.', 'Database', false, '2016-02-09 22:05:09', '2016-02-09 22:05:09');
INSERT INTO service_type VALUES ('mongodb', 'DreamFactory\Core\MongoDb\Services\MongoDb', 'DreamFactory\Core\MongoDb\Models\MongoDbConfig', 'MongoDB', 'Database service for MongoDB connections.', 'Database', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('couchdb', 'DreamFactory\Core\CouchDb\Services\CouchDb', 'DreamFactory\Core\CouchDb\Models\CouchDbConfig', 'CouchDB', 'Database service for CouchDB connections.', 'Database', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('rws', 'DreamFactory\Core\Rws\Services\RemoteWeb', 'DreamFactory\Core\Rws\Models\RwsConfig', 'Remote Web Service', 'A service to handle Remote Web Services', 'Custom', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('soap', 'DreamFactory\Core\Soap\Services\Soap', 'DreamFactory\Core\Soap\Models\SoapConfig', 'SOAP Service', 'A service to handle SOAP Services', 'Custom', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('aws_s3', 'DreamFactory\Core\Aws\Services\S3', 'DreamFactory\Core\Aws\Components\AwsS3Config', 'AWS S3', 'File storage service supporting the AWS S3 file system.', 'File', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('aws_dynamodb', 'DreamFactory\Core\Aws\Services\DynamoDb', 'DreamFactory\Core\Aws\Models\AwsConfig', 'AWS DynamoDB', 'A database service supporting the AWS DynamoDB system.', 'Database', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('aws_sns', 'DreamFactory\Core\Aws\Services\Sns', 'DreamFactory\Core\Aws\Models\AwsConfig', 'AWS SNS', 'Push notification service supporting the AWS SNS system.', 'Notification', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('aws_ses', 'DreamFactory\Core\Aws\Services\Ses', 'DreamFactory\Core\Aws\Models\AwsConfig', 'AWS SES', 'Email service supporting the AWS SES system.', 'Email', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('rackspace_cloud_files', 'DreamFactory\Core\Rackspace\Services\OpenStackObjectStore', 'DreamFactory\Core\Rackspace\Components\RackspaceCloudFilesConfig', 'Rackspace Cloud Files', 'File service supporting Rackspace Cloud Files Storage system.', 'File', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('openstack_object_storage', 'DreamFactory\Core\Rackspace\Services\OpenStackObjectStore', 'DreamFactory\Core\Rackspace\Components\OpenStackObjectStorageConfig', 'OpenStack Object Storage', 'File service supporting OpenStack Object Storage system.', 'File', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('salesforce_db', 'DreamFactory\Core\Salesforce\Services\SalesforceDb', 'DreamFactory\Core\Salesforce\Models\SalesforceConfig', 'SalesforceDB', 'Database service for Salesforce connections.', 'Database', false, '2016-02-09 22:05:10', '2016-02-09 22:05:10');
INSERT INTO service_type VALUES ('user', 'DreamFactory\Core\User\Services\User', 'DreamFactory\Core\User\Models\UserConfig', 'User service', 'User service to allow user management.', 'User', true, '2016-02-09 22:05:11', '2016-02-09 22:05:11');
INSERT INTO service_type VALUES ('oauth_facebook', 'DreamFactory\Core\OAuth\Services\Facebook', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'Facebook OAuth', 'OAuth service for supporting Facebook authentication and API access.', 'OAuth', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');
INSERT INTO service_type VALUES ('oauth_twitter', 'DreamFactory\Core\OAuth\Services\Twitter', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'Twitter OAuth', 'OAuth service for supporting Twitter authentication and API access.', 'OAuth', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');
INSERT INTO service_type VALUES ('oauth_github', 'DreamFactory\Core\OAuth\Services\Github', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'GitHub OAuth', 'OAuth service for supporting GitHub authentication and API access.', 'OAuth', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');
INSERT INTO service_type VALUES ('oauth_google', 'DreamFactory\Core\OAuth\Services\Google', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'Google OAuth', 'OAuth service for supporting Google authentication and API access.', 'OAuth', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');
INSERT INTO service_type VALUES ('oauth_linkedin', 'DreamFactory\Core\OAuth\Services\LinkedIn', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'LinkedIn OAuth', 'OAuth service for supporting LinkedIn authentication and API access.', 'OAuth', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');
INSERT INTO service_type VALUES ('adldap', 'DreamFactory\Core\ADLdap\Services\ADLdap', 'DreamFactory\Core\ADLdap\Models\ADConfig', 'Active Directory LDAP', 'A service for supporting Active Directory integration', 'LDAP', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');
INSERT INTO service_type VALUES ('ldap', 'DreamFactory\Core\ADLdap\Services\LDAP', 'DreamFactory\Core\ADLdap\Models\LDAPConfig', 'Standard LDAP', 'A service for supporting Open LDAP integration', 'LDAP', false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');


--
-- Data for Name: smtp_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: soap_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: sql_db_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO sql_db_config VALUES (4, 'sqlite', 'sqlite:db.sqlite', NULL, NULL, NULL, NULL, false);


--
-- Data for Name: system_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: system_custom; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: system_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: system_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('system_lookup_id_seq', 1, false);


--
-- Data for Name: system_resource; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO system_resource VALUES ('admin', 'DreamFactory\Core\Resources\System\Admin', 'Administrators', 'Allows configuration of system administrators.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('cache', 'DreamFactory\Core\Resources\System\Cache', 'Cache Administration', 'Allows administration of system-wide and service cache.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('config', 'DreamFactory\Core\Resources\System\Config', 'Configuration', 'Global system configuration.', true, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('constant', 'DreamFactory\Core\Resources\System\Constant', 'Constants', 'Read-only listing of constants available for client use.', false, true, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('cors', 'DreamFactory\Core\Resources\System\Cors', 'CORS Configuration', 'Allows configuration of CORS system settings.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('email_template', 'DreamFactory\Core\Resources\System\EmailTemplate', 'Email Templates', 'Allows configuration of email templates.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('environment', 'DreamFactory\Core\Resources\System\Environment', 'Environment', 'Read-only system environment configuration.', true, true, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('event', 'DreamFactory\Core\Resources\System\Event', 'Events', 'Allows registering server-side scripts to system generated events.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('lookup', 'DreamFactory\Core\Resources\System\Lookup', 'Lookup Keys', 'Allows configuration of lookup keys.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('role', 'DreamFactory\Core\Resources\System\Role', 'Roles', 'Allows role configuration.', false, false, '2016-02-09 22:05:05', '2016-02-09 22:05:05');
INSERT INTO system_resource VALUES ('service', 'DreamFactory\Core\Resources\System\Service', 'Services', 'Allows configuration of services.', false, false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('service_type', 'DreamFactory\Core\Resources\System\ServiceType', 'Service Types', 'Read-only system service types.', false, true, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('script_type', 'DreamFactory\Core\Resources\System\ScriptType', 'Script Types', 'Read-only system scripting types.', false, true, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('setting', 'DreamFactory\Core\Resources\System\Setting', 'Custom Settings', 'Allows configuration of system-wide custom settings.', false, false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('app', 'DreamFactory\Core\Resources\System\App', 'Apps', 'Allows management of user application(s)', false, false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('app_group', 'DreamFactory\Core\Resources\System\AppGroup', 'App Groups', 'Allows grouping of user application(s)', false, false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('custom', 'DreamFactory\Core\Resources\System\Custom', 'Custom Settings', 'Allows for creating system-wide custom settings', false, false, '2016-02-09 22:05:06', '2016-02-09 22:05:06');
INSERT INTO system_resource VALUES ('user', 'DreamFactory\Core\User\Resources\System\User', 'User Management', 'Allows user management capability.', false, false, '2016-02-09 22:05:12', '2016-02-09 22:05:12');


--
-- Data for Name: system_setting; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: system_setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('system_setting_id_seq', 1, false);


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: token_map; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO token_map VALUES (1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsInVzZXJfaWQiOjEsImVtYWlsIjoiaW5mb0BhbGJ1cy1pdC5jb20iLCJmb3JldmVyIjpmYWxzZSwiaXNzIjoiaHR0cDpcL1wvZGYucGM0LnZib3hcL2FwaVwvdjJcL3N5c3RlbVwvYWRtaW5cL3Nlc3Npb24iLCJpYXQiOjE0NTU1ODg3NjUsImV4cCI6MTQ1NTU5MjM2NSwibmJmIjoxNDU1NTg4NzY1LCJqdGkiOiIzNzJlY2RkZGQ2ZDNmNWE2ZDI5M2NjZmZhZmM0NTZiNyJ9.2p7mk_gfC1KNPCBFI1kYb6iMcq1R4gik-Y8p5Xbt--c', 1455588765, 1455592365);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO "user" VALUES (1, 'albus', 'Alexander', 'Weiß', '2016-02-16 02:12:45', 'info@albus-it.com', '$2y$10$OEyqbMLku0n2o9hGrBcRFOzc8pzTJcT2xyYtDaHv3CuMFdA2BH45i', true, true, NULL, NULL, NULL, 'y', NULL, NULL, '2016-02-09 22:07:48', '2016-02-16 02:12:45', NULL, NULL, NULL, NULL);


--
-- Data for Name: user_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO user_config VALUES (6, false, NULL, 5, 2, 5, 1, 5, 3);


--
-- Data for Name: user_custom; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: user_custom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_custom_id_seq', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_id_seq', 1, true);


--
-- Data for Name: user_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: user_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_lookup_id_seq', 1, false);


--
-- Data for Name: user_to_app_to_role; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_to_app_to_role_id_seq', 1, false);


--
-- Name: app_group_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_name_unique UNIQUE (name);


--
-- Name: app_group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_pkey PRIMARY KEY (id);


--
-- Name: app_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_pkey PRIMARY KEY (id);


--
-- Name: app_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_name_unique UNIQUE (name);


--
-- Name: app_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_pkey PRIMARY KEY (id);


--
-- Name: app_to_app_group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY app_to_app_group
    ADD CONSTRAINT app_to_app_group_pkey PRIMARY KEY (id);


--
-- Name: aws_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY aws_config
    ADD CONSTRAINT aws_config_pkey PRIMARY KEY (service_id);


--
-- Name: cloud_email_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY cloud_email_config
    ADD CONSTRAINT cloud_email_config_pkey PRIMARY KEY (service_id);


--
-- Name: content_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY content
    ADD CONSTRAINT content_pkey PRIMARY KEY (id);


--
-- Name: contenttype_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT contenttype_pkey PRIMARY KEY (id);


--
-- Name: cors_config_path_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_path_unique UNIQUE (path);


--
-- Name: cors_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_pkey PRIMARY KEY (id);


--
-- Name: couchdb_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY couchdb_config
    ADD CONSTRAINT couchdb_config_pkey PRIMARY KEY (service_id);


--
-- Name: db_field_extras_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_pkey PRIMARY KEY (id);


--
-- Name: db_relationship_extras_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_pkey PRIMARY KEY (id);


--
-- Name: db_table_extras_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_pkey PRIMARY KEY (id);


--
-- Name: email_parameters_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY email_parameters_config
    ADD CONSTRAINT email_parameters_config_pkey PRIMARY KEY (id);


--
-- Name: email_template_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_name_unique UNIQUE (name);


--
-- Name: email_template_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_pkey PRIMARY KEY (id);


--
-- Name: event_script_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_pkey PRIMARY KEY (name);


--
-- Name: event_subscriber_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_name_unique UNIQUE (name);


--
-- Name: event_subscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_pkey PRIMARY KEY (id);


--
-- Name: file_service_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY file_service_config
    ADD CONSTRAINT file_service_config_pkey PRIMARY KEY (service_id);


--
-- Name: filesharedforgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT filesharedforgroup_pkey PRIMARY KEY (id_file, id_group);


--
-- Name: filesharedfororga_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT filesharedfororga_pkey PRIMARY KEY (id_file, id_orga);


--
-- Name: fkorg_add_id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY address
    ADD CONSTRAINT fkorg_add_id PRIMARY KEY (id_orga);


--
-- Name: fksuccessor_id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT fksuccessor_id UNIQUE (next);


--
-- Name: group_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- Name: groupmember_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT groupmember_pkey PRIMARY KEY (id_group, id_user);


--
-- Name: highscore_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT highscore_pkey PRIMARY KEY (name, id_route);


--
-- Name: id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- Name: ldap_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY ldap_config
    ADD CONSTRAINT ldap_config_pkey PRIMARY KEY (service_id);


--
-- Name: mediafile_file_name_key; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mediafile
    ADD CONSTRAINT mediafile_file_name_key UNIQUE (file_name);


--
-- Name: mongodb_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY mongodb_config
    ADD CONSTRAINT mongodb_config_pkey PRIMARY KEY (service_id);


--
-- Name: oauth_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY oauth_config
    ADD CONSTRAINT oauth_config_pkey PRIMARY KEY (service_id);


--
-- Name: old_id; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT old_id UNIQUE (old_id);


--
-- Name: one_time_access_code_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY one_time_access_code
    ADD CONSTRAINT one_time_access_code_pkey PRIMARY KEY (code);


--
-- Name: orgamember_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT orgamember_pkey PRIMARY KEY (id_orga, id_user);


--
-- Name: organization_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);


--
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- Name: player_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY player
    ADD CONSTRAINT player_pkey PRIMARY KEY (name);


--
-- Name: rackspace_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rackspace_config
    ADD CONSTRAINT rackspace_config_pkey PRIMARY KEY (service_id);


--
-- Name: role_adldap_dn_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_adldap
    ADD CONSTRAINT role_adldap_dn_unique UNIQUE (dn);


--
-- Name: role_adldap_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_adldap
    ADD CONSTRAINT role_adldap_pkey PRIMARY KEY (role_id);


--
-- Name: role_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_pkey PRIMARY KEY (id);


--
-- Name: role_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_name_unique UNIQUE (name);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: role_service_access_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_pkey PRIMARY KEY (id);


--
-- Name: route_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY route
    ADD CONSTRAINT route_pkey PRIMARY KEY (id);


--
-- Name: routentag_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT routentag_pkey PRIMARY KEY (id_route, tag_name);


--
-- Name: rws_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rws_config
    ADD CONSTRAINT rws_config_pkey PRIMARY KEY (service_id);


--
-- Name: rws_headers_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rws_headers_config
    ADD CONSTRAINT rws_headers_config_pkey PRIMARY KEY (id);


--
-- Name: rws_parameters_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY rws_parameters_config
    ADD CONSTRAINT rws_parameters_config_pkey PRIMARY KEY (id);


--
-- Name: salesforce_db_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY salesforce_db_config
    ADD CONSTRAINT salesforce_db_config_pkey PRIMARY KEY (service_id);


--
-- Name: script_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY script_config
    ADD CONSTRAINT script_config_pkey PRIMARY KEY (service_id);


--
-- Name: script_type_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY script_type
    ADD CONSTRAINT script_type_pkey PRIMARY KEY (name);


--
-- Name: service_cache_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service_cache_config
    ADD CONSTRAINT service_cache_config_pkey PRIMARY KEY (service_id);


--
-- Name: service_doc_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service_doc
    ADD CONSTRAINT service_doc_pkey PRIMARY KEY (id);


--
-- Name: service_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_name_unique UNIQUE (name);


--
-- Name: service_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- Name: service_type_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY service_type
    ADD CONSTRAINT service_type_pkey PRIMARY KEY (name);


--
-- Name: sid; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY contenttype
    ADD CONSTRAINT sid UNIQUE (sid);


--
-- Name: smtp_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY smtp_config
    ADD CONSTRAINT smtp_config_pkey PRIMARY KEY (service_id);


--
-- Name: soap_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY soap_config
    ADD CONSTRAINT soap_config_pkey PRIMARY KEY (service_id);


--
-- Name: sql_db_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY sql_db_config
    ADD CONSTRAINT sql_db_config_pkey PRIMARY KEY (service_id);


--
-- Name: system_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_config
    ADD CONSTRAINT system_config_pkey PRIMARY KEY (db_version);


--
-- Name: system_custom_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_custom
    ADD CONSTRAINT system_custom_pkey PRIMARY KEY (name);


--
-- Name: system_lookup_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_name_unique UNIQUE (name);


--
-- Name: system_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_pkey PRIMARY KEY (id);


--
-- Name: system_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_resource
    ADD CONSTRAINT system_resource_pkey PRIMARY KEY (name);


--
-- Name: system_setting_name_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_name_unique UNIQUE (name);


--
-- Name: system_setting_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_pkey PRIMARY KEY (id);


--
-- Name: tag_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (tag_name);


--
-- Name: user_config_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_config
    ADD CONSTRAINT user_config_pkey PRIMARY KEY (service_id);


--
-- Name: user_custom_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_custom
    ADD CONSTRAINT user_custom_pkey PRIMARY KEY (id);


--
-- Name: user_email_unique; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_unique UNIQUE (email);


--
-- Name: user_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_to_app_to_role_pkey; Type: CONSTRAINT; Schema: public; Owner: placity; Tablespace: 
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_pkey PRIMARY KEY (id);


--
-- Name: app_lookup_name_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX app_lookup_name_index ON app_lookup USING btree (name);


--
-- Name: ldap_config_default_role_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX ldap_config_default_role_index ON ldap_config USING btree (default_role);


--
-- Name: oauth_config_default_role_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX oauth_config_default_role_index ON oauth_config USING btree (default_role);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: password_resets_token_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX password_resets_token_index ON password_resets USING btree (token);


--
-- Name: role_lookup_name_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX role_lookup_name_index ON role_lookup USING btree (name);


--
-- Name: user_lookup_name_index; Type: INDEX; Schema: public; Owner: placity; Tablespace: 
--

CREATE INDEX user_lookup_name_index ON user_lookup USING btree (name);


--
-- Name: app_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_group_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_group_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_group
    ADD CONSTRAINT app_group_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_lookup_app_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_app_id_foreign FOREIGN KEY (app_id) REFERENCES app(id) ON DELETE CASCADE;


--
-- Name: app_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_lookup
    ADD CONSTRAINT app_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: app_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE SET NULL;


--
-- Name: app_storage_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app
    ADD CONSTRAINT app_storage_service_id_foreign FOREIGN KEY (storage_service_id) REFERENCES service(id) ON DELETE SET NULL;


--
-- Name: app_to_app_group_app_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_to_app_group
    ADD CONSTRAINT app_to_app_group_app_id_foreign FOREIGN KEY (app_id) REFERENCES app(id) ON DELETE CASCADE;


--
-- Name: app_to_app_group_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY app_to_app_group
    ADD CONSTRAINT app_to_app_group_group_id_foreign FOREIGN KEY (group_id) REFERENCES app_group(id) ON DELETE CASCADE;


--
-- Name: aws_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY aws_config
    ADD CONSTRAINT aws_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: cloud_email_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cloud_email_config
    ADD CONSTRAINT cloud_email_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: contenttype_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY content
    ADD CONSTRAINT contenttype_fk FOREIGN KEY (id_content_type) REFERENCES contenttype(id);


--
-- Name: cors_config_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: cors_config_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY cors_config
    ADD CONSTRAINT cors_config_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: couchdb_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY couchdb_config
    ADD CONSTRAINT couchdb_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_field_extras_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_field_extras_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_field_extras_ref_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_ref_service_id_foreign FOREIGN KEY (ref_service_id) REFERENCES service(id);


--
-- Name: db_field_extras_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_field_extras
    ADD CONSTRAINT db_field_extras_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_relationship_extras_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_relationship_extras_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_relationship_extras_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_relationship_extras
    ADD CONSTRAINT db_relationship_extras_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: db_table_extras_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_table_extras_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: db_table_extras_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY db_table_extras
    ADD CONSTRAINT db_table_extras_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: email_parameters_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_parameters_config
    ADD CONSTRAINT email_parameters_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: email_template_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: email_template_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY email_template
    ADD CONSTRAINT email_template_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_script_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_script_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_script_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_script
    ADD CONSTRAINT event_script_type_foreign FOREIGN KEY (type) REFERENCES script_type(name) ON DELETE CASCADE;


--
-- Name: event_subscriber_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: event_subscriber_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY event_subscriber
    ADD CONSTRAINT event_subscriber_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: file_service_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY file_service_config
    ADD CONSTRAINT file_service_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY one_time_access_code
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedforgroup
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: group_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT group_fk FOREIGN KEY (id_group) REFERENCES "group"(id);


--
-- Name: ldap_config_default_role_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY ldap_config
    ADD CONSTRAINT ldap_config_default_role_foreign FOREIGN KEY (default_role) REFERENCES role(id);


--
-- Name: ldap_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY ldap_config
    ADD CONSTRAINT ldap_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: mongodb_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY mongodb_config
    ADD CONSTRAINT mongodb_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: name_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT name_fk FOREIGN KEY (name) REFERENCES player(name);


--
-- Name: name_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT name_fk FOREIGN KEY (tag_name) REFERENCES tag(tag_name);


--
-- Name: oauth_config_default_role_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY oauth_config
    ADD CONSTRAINT oauth_config_default_role_foreign FOREIGN KEY (default_role) REFERENCES role(id);


--
-- Name: oauth_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY oauth_config
    ADD CONSTRAINT oauth_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY filesharedfororga
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY address
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY orgamember
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY route
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: orga_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "group"
    ADD CONSTRAINT orga_fk FOREIGN KEY (id_orga) REFERENCES organization(id);


--
-- Name: page_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY content
    ADD CONSTRAINT page_fk FOREIGN KEY (id_page) REFERENCES page(id);


--
-- Name: rackspace_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rackspace_config
    ADD CONSTRAINT rackspace_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: role_adldap_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_adldap
    ADD CONSTRAINT role_adldap_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: role_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_lookup_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_lookup
    ADD CONSTRAINT role_lookup_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: role_service_access_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_service_access_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: role_service_access_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: role_service_access_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY role_service_access
    ADD CONSTRAINT role_service_access_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY highscore
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY routentag
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: route_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY page
    ADD CONSTRAINT route_fk FOREIGN KEY (id_route) REFERENCES route(id);


--
-- Name: rws_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_config
    ADD CONSTRAINT rws_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: rws_headers_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_headers_config
    ADD CONSTRAINT rws_headers_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: rws_parameters_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY rws_parameters_config
    ADD CONSTRAINT rws_parameters_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: salesforce_db_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY salesforce_db_config
    ADD CONSTRAINT salesforce_db_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: script_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY script_config
    ADD CONSTRAINT script_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: script_config_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY script_config
    ADD CONSTRAINT script_config_type_foreign FOREIGN KEY (type) REFERENCES script_type(name) ON DELETE CASCADE;


--
-- Name: service_cache_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service_cache_config
    ADD CONSTRAINT service_cache_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: service_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: service_doc_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service_doc
    ADD CONSTRAINT service_doc_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: service_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: service_type_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_type_foreign FOREIGN KEY (type) REFERENCES service_type(name) ON DELETE CASCADE;


--
-- Name: smtp_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY smtp_config
    ADD CONSTRAINT smtp_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: soap_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY soap_config
    ADD CONSTRAINT soap_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: sql_db_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY sql_db_config
    ADD CONSTRAINT sql_db_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: successor_fk; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY page
    ADD CONSTRAINT successor_fk FOREIGN KEY (next) REFERENCES page(id);


--
-- Name: system_config_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_config
    ADD CONSTRAINT system_config_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_config_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_config
    ADD CONSTRAINT system_config_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_custom_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_custom
    ADD CONSTRAINT system_custom_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_custom_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_custom
    ADD CONSTRAINT system_custom_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_lookup
    ADD CONSTRAINT system_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_setting_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: system_setting_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY system_setting
    ADD CONSTRAINT system_setting_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: token_map_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY token_map
    ADD CONSTRAINT token_map_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_config_service_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_config
    ADD CONSTRAINT user_config_service_id_foreign FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE;


--
-- Name: user_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_custom_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_custom
    ADD CONSTRAINT user_custom_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_lookup_created_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_created_by_id_foreign FOREIGN KEY (created_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_lookup_last_modified_by_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_last_modified_by_id_foreign FOREIGN KEY (last_modified_by_id) REFERENCES "user"(id) ON DELETE SET NULL;


--
-- Name: user_lookup_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_lookup
    ADD CONSTRAINT user_lookup_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: user_to_app_to_role_app_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_app_id_foreign FOREIGN KEY (app_id) REFERENCES app(id) ON DELETE CASCADE;


--
-- Name: user_to_app_to_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE;


--
-- Name: user_to_app_to_role_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: placity
--

ALTER TABLE ONLY user_to_app_to_role
    ADD CONSTRAINT user_to_app_to_role_user_id_foreign FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: mediafile; Type: ACL; Schema: public; Owner: placity
--

REVOKE ALL ON TABLE mediafile FROM PUBLIC;
REVOKE ALL ON TABLE mediafile FROM placity;
GRANT ALL ON TABLE mediafile TO placity;
GRANT ALL ON TABLE mediafile TO PUBLIC;


--
-- PostgreSQL database dump complete
--

