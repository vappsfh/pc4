﻿## Strategie zur Integration besehtender Useraccount 
---
Diese Anpassung im Dreamfactory-Login-Prozess ermöglicht nun ein Login mit der Kombination aus Benutzername und "alten" (MD5) Passwordhash aus dem bisherigen System. Hierzu einfach alle Benutzernamen und MD5-Passwordhash der Bestandsdaten 1:1 in die "neue" Datenbank übernehemen. 
Beim nächsten erfolgreichen Login des entsprechenden Users wird das eingegebene Passwort gehasht (bcrypt) und der alte Hash in der Datenbank ersetzt. 

Technisch wurden folgende zwei PHP-Klassen-Dateien des Dreamfactory-Moduls modifiziert und ersetzt: 

```
Files: 
rm $PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/DatabaseUserProvider.php
rm $PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/EloquentUserProvider.php
 	
cp ./EloquentUserProvider.php 	$PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/
cp ./DatabaseUserProvider.php	$PC_DF2_DIR/vendor/laravel/framework/src/Illuminate/Auth/

```
Kopien der modifizierten Quelldateien befinden sich in diesem Ordner.

![Anpassung im Quelltext](SourceCode.jpg)  



### Veröffentlichung & Lizenzierung ### 

Alle Ergebnisse des Praxisprojekts sind hiermit veröffentlicht und können unter Beachtung des folgenden Lizenztextes frei verwendet werden.
```
Copyright (c) 2016, VAPPS Projektteam der FH-Bingen Proj-WS1516

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```

----------------------
Alexander Weiß 17.02.2016