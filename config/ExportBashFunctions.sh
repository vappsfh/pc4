#!/usr/bin/env bash                                                       #
# Bash-Script: Export - Hilfsfunktionen                                   #
# -------------------------------------                                   #
# Script enthält verschiedene Hilfsfunktionen für das Setup des Backends  #
# und für die Wartung eines Linux-Servers.                                #
# ------------------------------------------------------------------------#
# Author: Alexander Weiß                                                  #
# 05.02.2016                                                              #
# ------------------------------------------------------------------------#
# Copyright (c) 2016, Alexander Weiß (FH-Bingen Projektteam VAPPS)        #
#                                                                         #
# Permission to use,copy, modify, and/or distribute this software for any #
# purpose with or without fee is hereby granted, provided that the above  #
# copyright notice and this permission notice appear in all copies.       #
#                                                                         #
# THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS ALL WARRANTIES  #
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        #
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR #
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  #
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   #
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF #
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          #
###########################################################################


# Generiert install und Uninstall-Statements
InstallHelper4DebPacks(){
USAGE(){
cat << EOF
USAGE: createInAndUninstallCode4DebPacks deb-pkg [deb-pkg]*
-----------------------------------------------------------------
Generiert für jedes Element einer Liste von dpgk folgenden
Installation und Uninstall-Staments:
  # ----------------------------------------------------------
    ## dpkg Version Beschreibung
    apt-get -y -qq install dpkg
    [...]
    apt-get -y -qq remove  dpkg
  # ----------------------------------------------------------
-----------------------------------------------------------------
EOF
}
if [ $# -eq 0 ] ; then USAGE && exit 1; if

timestamp="$(date +%s)"
exec 3 >>inst$timestamp
exec 4 >>uinst$timestamp

INSTALL(){
cat<< EOF
#------------------------------------------------------------
# UPDATE:
sudo su && apt-get -y update && apt-get -y dist-upgrade"  >&3
#------------------------------------------------------------
# INSTALL:
EOF
}
UNINSTALL(){
cat<<EOF
"#
 # ----------------------------------------------------------
 # UMINSTALL:
EOF
}

for pkg in "$@"
do
  if [ pkgInfo=$(apt-cache show vim) ]
   then
    echo ------------------------------------------------------   >&3
    apt-cache show vim                                            >&3
    echo $pkg                                                     >&3
    echo "apt-get -y -qq install ${pkg}"                          >&3
    echo ------------------------------------------------------   >&3
    echo "# apt-get -y -qq remove  ${pkg}"                        >&4
   else
    echo ###################################################### >&2
    echo FEHLER: $pkg konnte nicht gefunden werden.
    echo ###################################################### >&2
  fi
done

REMOVE(){
cat<<EOF
#------------------------------------------------------------
# REMOVE UNUSED DPKG
# apt-get -y -qq autoremove"
#------------------------------------------------------------
EOF
}
INSTALL
cat /tmp/inst$timestamp
UNINSTALL
cat /tmp/uninst$timestamp
REMOVE
rm inst$timestamp
rm uninst$timestamp
}

export -p createInAndUninstallCode4DebPacks
