/* ***************************************************************************************
 * ISC License (ISC)
 *
 * Copyright (c) 2016, A. Weiss <info@albus-it.com> member of the VAPPS-Team (FH-Bingen)
 * Permission to use, copy, modify, and/or distribute this software for any purpose with
 * or without fee is hereby granted, provided that the above copyright notice and this
 * permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
 * TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Created by Alexander Weiß on 16.02.2016.
 * **************************************************************************************/

/**
 * Anmerkung:
 *  1. MultipleChoise ist Singelchoice: Lösung "answer": "753" -> "answer": ["753"]
 *          + im Controller: um if eine foraech drum herum
 *  2. Correct bzw. WrongMSG ergänzt
 *  3. Lösungshilfe: Feld ergänzen
 *  4. Points: in AppController bei richtiger Lösung auf lokales Profil pushen
 *  5. WayPoint fehlt
 */

/** **************************************************************************************
 * Transformation ct-alt->ct-neu
 *                  1.1 -> 1
 *                  1.2 -> 1
 *                  1.3 -> 3
 *                  2   -> 7
 *                  3   -> 6
 *                  4   -> 8
 *                  5   -> 2
 *                  6   -> 2
 *                  7   -> 5
 *                  8   -> 8
 * **************************************************************************************/

/**
 * Contenttype: ctText
 * id=1
 */
ctTextExample =
{
    "languages": [
        {
            "lang": "de_DE", "fields": [
            {
                "text": "<p>Hier befindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"
            }
        ]
        }, {
            "lang": "en_EN", "fields": [
                {
                    "text": "<p>You'll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"
                }
            ]
        }
    ]
}

/**
 * Contenttype: ctMultipleChoice
 * id=2
 */
ctMultipleChoiceExample =
{
    "languages": [{
        "lang"  : "de_DE",
        "fields": [{
            "text"  : "<p>Wann wurde Rom gegründet?</p>",
            "answer": "753",
            "choice": ["357", "17", "753", "166", "1652"],
            "cmessage": "richtig",
            "wmessage": "falsch"

        }]
    }, {
        "lang"  : "en_EN",
        "fields": [{
            "text"  : "<p>When was Rome foundet?</p>",
            "answer": "753",
            "choice": ["357", "17", "753", "166", "1652"],
            "cmessage": "right",
            "wmessage": "wrong"
        }]
    }],
    "points":5
}



/**
 * Contenttype: ctDialog
 * id=3
 */
ctDialogExample =
{
    "languages": [{
        "lang"  : "de_DE",
        "fields": [{
            "avatar1": "http://143.93.91.92/philipp/avatar1.jpg",
            "avatar2": "http://143.93.91.92/philipp/avatar1.jpg",
            "text"   : ["<p>Hi Alex</p>", "<p>Hi Paul</p>", "<p>Na wie gehts?</p>",
                        "<p>Alles bestens, kann nicht klagen</p>"]
        }]
    }, {
        "lang"  : "en_EN",
        "fields": [{
            "avatar1": "http://143.93.91.92/philipp/avatar1.jpg",
            "avatar2": "http://143.93.91.92/philipp/avatar1.jpg",
            "text"   : ["<p>Hi Alex</p>", "<p>Hi Paul</p>", "<p>How are you?</p>",
                        "<p>Quite nice, thanks!</p>"]
        }]
    }]
}



/**
 * Contenttype: ctInput
 * id=5
 */
ctInputExample =
{
    "languages": [{
        "lang"  : "de_DE",
        "fields": [{"text": "<p>Wann wurde Rom gegründet?</p>", "answer": "753"}],
        "cmessage": "richtig",
        "wmessage": "falsch"
    }, {
        "lang"  : "en_EN",
        "fields": [{"text": "<p>When was Rome foundet?</p>", "answer": "753"}],
        "cmessage": "richtig",
        "wmessage": "falsch"
    }],
    "points":5
}

/**
 * Contenttype: ctAudio
 * id=6
 */
ctAudioExample =
{
    "languages": [{
        "lang"  : "de_DE",
        "fields": [{
            "id"          : "14",
            "audiofileurl": "http://143.93.91.92/upload/2016_1_16/Ab%20An%20De%20See.mp3",
            "info"        : "InfoText",
            "autoplay"    : true
        }]
    }, {
        "lang"  : "en_EN",
        "fields": [{
            "id"          : "14",
            "audiofileurl": "http://143.93.91.92/upload/2016_1_16/Ab%20An%20De%20See.mp3",
            "info"        : "InfoText",
            "autoplay"    : true
        }]
    }]
}

/**
 * Contenttype: ctVideo
 * id=7
 */
ctVideoExample =
{
    "languages": [{
        "lang"  : "de_DE",
        "fields": [{
            "contenttype": "video",
            "src"        : "https://upload.wikimedia.org/wikipedia/commons/transcoded/8/87/Schlossbergbahn.webm/Schlossbergbahn.webm.480p.webm",
            "description": "Eine Bahn faehrt einen Berg hoch",
            "autoplay"   : true
        }]
    }, {
        "lang"  : "en_EN",
        "fields": [{
            "contenttype": "video",
            "src"        : "https://upload.wikimedia.org/wikipedia/commons/transcoded/8/87/Schlossbergbahn.webm/Schlossbergbahn.webm.480p.webm",
            "description": "Eine Bahn faehrt einen Berg hoch",
            "autoplay"   : true
        }]
    }]
}

/**
 * Contenttype: ctImage
 * id=8
 */
ctImageExample=
{
    "languages":[{
        "lang"  : "de_DE",
        "fields": [{
            "src": "http://143.93.91.92/philipp/avatar1.jpg",
            "alt": "Dies ist ein AvatarBild"
        }]
    }, {
        "lang"  : "en_EN",
        "fields": [{
            "src": "http://143.93.91.92/philipp/avatar1.jpg",
            "alt": "Dies ist ein AvatarBild"
        }]
    }]
}