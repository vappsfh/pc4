# Installationsskript des Placity-Servers #
---
inkl. des QuestionEditors und der Placity-App als Entwicklungsumgebung (WebApp nicht voll funktionsfähig)

### Requirements:
```
(virtueller) RootServer mit
    SSH Zugang:
    OS: Ubuntu Server 14.04 64bit
    2 vCore
    min. 1GB RAM
    und 25gb HDD
    Internetanbindung: min 100mbit ( f. Spitzenlasten: z.B. Workshop )
```
### WICHTIGES VOR DER INSTALLATION ###

Der Apache-Server benötigt für die richtige Konfiguration der Alias-Direktiven
und genutzten vHosts folgende Vorbereitung:

#### I. ####
Die Einrichtung von drei Subdomänen, die per DNS-Eintrag auf die IP den
hostenden Webserver verweisen. Diese Subdomänen referenzieren später auf

  1. die Dreamfactory als **Backendenmodul**   z.B. df.Domain.tld
  2. den **QuestionEditor**                    z.B. qe.Domain.tld
  3. die **PlacityApp**                        z.B. placity.Domain.tld


#### II. ####

Die Webserver-Domain sowie die im vorigen Schritt eingerichteten Subdomänen 
müssen in der, im Archiv `pc4installer.tar` enthalten Datei `config.sh` eingetragen werden. 
Dazu das Archiv entpacken und die Datei entsprechend anpassen. 

Die automatische Konfiguration der vHosts und Alias Direktiven übernimmt 
das Shell-Script ``` apache-config.sh```. In diesem Script sind in aus-
kommentierter Form, alle Apache-Direktiven für den Betrieb mit SSL gesicherten
http-Verbindungen hinterlegt.

Liegt ein SSL-Zertifikat vor, sollten diese Datei entsprechen angepasst werden.

```
## ######################################################################################
## APACHE Konfiguration
## --------------------
## alias:
##	$DOMAIN/phppgadmin  -> phpPGadmin		/usr/share/phppgadmin
##	$DOMAIN/media       -> media			$FILES
##
## vhosts:
##	$DF2_SUBDOMAIN	    -> dreamfactory		$DF_ROOT_DIR
##	$QE_SUBDOMAIN		-> QuestionEditor	$QE_ROOT_DIR
##	$PLACITY_SUBDOMAIN	-> App 				$APP_ROOT_DIR
#### ######################################################################################
```

#### III. ####

Zur Installation muss das Shell-Script `pc4.sh` ausgeführt werden; dabei muss die im II. Schritt 
angepasste Datei `config.sh` unbedingt im gleichen Ordner liegen und lesbar sein.   

Die Installation selber sollte weitestgehend automatisiert erfolgen - es kann ggf. erforderlich
sein, einige Befehle mit 'Enter' zu bestätigen.

 ```
 ## Installation starten:  pc4Installer.tar

     ## become root
     sudo su
     cd /root
     ## pc4Installer.tar && config.sh z.B. im Ordner `root` bereitstellen
	 ## pc4installer.tar entpacken 
	 
     . pc4.sh  ##ausführen

	 
 ## alexander.weiss@live.de  (falls es klemmt)
 ```

---

 ### "Placity" Verzeichnisstruktur ###

 ```
 /opt/PC4
 ├───app                -> Placity-App zur Wiedergabe von Routen
 ├───qe                 -> QuestionEditor zur Erstellung von Routen
 ├───df2                -> Server-Backend: Schnittstelle zu DB-, Fileserver
 |                            und verschiedener Webservices via REST-API
 ├───ffmpeg             -> Custom-Build von ffmpeg zur Video-Encoding
 ├───fileserver         -> Medienserver und Video-Encoding
 ├───config             -> Dateien zur Server- und Systemkonfiguration
 └───DBSchema-CTs-Doku  -> Teil-, Zwischenlösungen und Doku

```

### Git-Repositories für die verschiedenen Teile

Git-Repositories für die verschiedenen Komponenten befinden sich hier: 
  
```
PC4_GIT=git@bitbucket.org:vappsfh/pc4.git
APP_GIT=https://github.com/paul280/placity_2.git
DF2_GIT=git@bitbucket.org:albus-fh/df2.git
QE_GIT=git@bitbucket.org:vappsfh/qe.git
FF_MPEG_GIT=git@bitbucket.org:vappsfh/ffmpeg-custom-build.git
FILESERVER_GIT=git@bitbucket.org:albus-fh/transcoding.git

```

Für den Zugriff ist folgender SSH-Key nötig

```
-----BEGIN RSA PRIVATE KEY-----
MIIEoQIBAAKCAQEAiBuwooj4SehP+H9bcgt9HG38o4HY26HCjVLIwX1C1t3HWUi1
kfH34Hvwk8gl+2lewKey/aiCs6Zj3FAMt5ImJhdjTp1s7G2UgEXVmGXVWEnOjfq9
7xFmTALOsDcE7RCfDaDjmyGxIGhz9mCHpkIAV9FHUcF4ydwXXAZLFFBi9kJnCmDs
I1/SIMZAOuK1Ug5jAMbGnqnd6Y1vOgijANCfi6vvSfxxoRbnTrbQaYbRibNKL+nQ
QgkWFNDco6mAn/TBLqDqGTr9u6CbuIqTIVFuPhTU5RZtBUSJP8B1gSZPvL83LJDY
BnvFImN+In5OIXtRa2AxIJa08CXIZ5JGwtgkYwIBJQKCAQBJkm1Q8BeB5Vuo6uVS
ZxMx9kNfTRtGSZmYf8Z2bTji2L7IelRO46iVBLlko4onCIY+n9ZfmVtoBune3yl3
+/jx/s3lTi0KLWUHEP3V1i4h4rTXH8Cc5s+DCG+5Mn80CPwHXeLQZT0mRkyFLUJn
tPlEOcWvptmB3sCExSGqHZ1G1v6BXQ7xWqXEwyiMPVDy+gIsdDYfmymD2t5d9Aso
QeaI+smcUGekVVFhvQXO9RMODyB1dKe78CvyikBv4DZMTbYmZHpLgnMIR63KVtn4
ous/0l0MGrxELnDzW2IkygqCBjQ9wsOjnN8lxO+ysPFmWCE6gRZejwm4MaCiIbj8
XxdNAoGBAMrAa6WZ4LjH/gH4BqqjwIRFBxotsqpuJ7POlb0On9HImOqT8u4YLgVO
PbvDAQTC+olcFUwCWaSL6rtetBja6EWaHNtiVf4hhxzwK7JFDZZReDzsZuKXD4FM
Z4ANPqpgjL+2klDqURAf53DmQCBkcNbKaYP/oJZRhmGpr6J4DysPAoGBAKvaorex
ZDN5kmZync6GZcEctXuDS1nhENM9op4a6hZ43KlU2yoyfj6lQcnnps/stl5g7+eK
ZlNumZB1/QNRfLdnFvlEzURkYbB4cWc2sAsc8QVvF64jRYPKvLMWPquFQJbQiWn6
SH6WNh6YEG8mGqOOVKolOkyjdspevdreTEFtAoGAfgjo+D0PJsGCOJNCahLDx9fh
0gC7K6xCMYBq6yTENkxDYWLjHmIOxQcfdLd9LHkz9IVZWMMwzhHJRApv88ZZCKvo
bLK/1VMco0kv61tbeR3i9XBboZwsO54ygAg7tgSqhQLJp+uaLJ417DwaBkxGJKdW
WPjZcjKtevrHH84XREcCgYA3vIfM35YeiEsoJSxC+yfrmpu5WwOucoH/UkKUJGeY
leazdXeDTqV8GevSx6u5DoBV9ezxJfesWzi5OwXzPQXanfmc87gx2130C2MMt8pk
eBbRVHZh/ZoBOtVqg8FMYpF9BVz/xstnU0/Ski7X8K6xlfHx/jxr4f0DXP9N5ztT
fQKBgQCaIHvBh9v8wbGbJISP3EGxmAbKNYssav9AyYSPrPKEYEdlcV3wxh9jemN1
oAH1qvm3aRmmbGXNKv2m7ft7WWoRxi0+ezwFyc/VQPcAviTa8gLM5riUEo6IdRv5
T2aUNR0hplDsv4SVwz3cXdZ9LOYHYUkLSkTsdpZMPX5M5lI5FA==
-----END RSA PRIVATE KEY-----
```

Um den SSH-Key auf dem Server dem SSH-Agent bekannt zugeben sind sind folgenden Befehle nützlich: 

```
## deployment-key unter ~/ssh/deployment-key speichern
deployment_key > ~/.ssh/deployment-key

## Zugriffsbeschränkung setzen
chmod -R 0600 ~/.ssh

## ssh-key adden 
ssh-add ~/.ssh/deployment-key
```



---

 ### Veröffentlichung & Lizenzierung ###

 Alle Ergebnisse des Praxisprojekts sind hiermit veröffentlicht und können unter Beachtung des folgenden Lizenztextes frei verwendet werden.
 ```
 Copyright (c) 2016, VAPPS Projektteam der FH-Bingen Proj-WS1516

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ```

 ----------------------
 Alexander Weiß 17.02.2016