## highlighting with some colors
function logError()  { cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,' ; }    #red 
function highlight() { cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' ; }    #yellow 


highlight<<YELLOW
###########################################################################
## config apache php 
###########################################################################
YELLOW
a2enmod rewrite
a2enmod ssl
php5enmod v8js
php5enmod mcrypt
php5enmod ldap
php5enmod `ls  /etc/php5/mods-av* | grep -e '^[^.)]*' -o`


highlight<< yellow
############################################################################
## APACHE Konfiguration 
## --------------------
## alias:    
##	$DOMAIN/phppgadmin  -> phpPGadmin		/usr/share/phppgadmin
##	$DOMAIN/media       -> media			$FILES
##
## vhosts: 
##	$DF2_SUBDOMAIN	    -> dreamfactory		$DF_ROOT_DIR
##	$QE_SUBDOMAIN		-> QuestionEditor	$QE_ROOT_DIR
##	$PLACITY_SUBDOMAIN	-> App 				$APP_ROOT_DIR
##			
############################################################################
yellow



mkdir $PC_CONFIG/apache2
mkdir $PC_CONFIG/apache2/conf-enabled
mkdir $PC_CONFIG/apache2/sites-enabled


echo "
Alias /phppgadmin /usr/share/phppgadmin

<Directory /usr/share/phppgadmin>

DirectoryIndex index.php
AllowOverride None

order deny,allow
#deny from all
#allow from 127.0.0.0/255.0.0.0 ::1/128
allow from all

<IfModule mod_php5.c>
  php_flag magic_quotes_gpc Off
  php_flag track_vars On
  #php_value include_path .
</IfModule>

</Directory>
" > $PC_CONFIG/apache2/conf-enabled/phppgadmin.conf

echo "
Alias /media $FILES
        <Directory $FILES>
                Options all
               AllowOverride All
               AddDefaultCharset utf-8
                Require all granted
        </Directory>
" > $PC_CONFIG/apache2/conf-enabled/alias-media.conf

echo "
<VirtualHost *:80>
   ServerName $DF2_SUBDOMAIN
    DocumentRoot $DF2_ROOT_DIR
    ServerAdmin webmaster@$DOMAIN

    <Directory $DF2_ROOT_DIR>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        AllowOverride None
        Require all granted
        RewriteEngine on
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^.*$ /index.php [L]

        <LimitExcept GET HEAD PUT DELETE PATCH POST>
            Allow from all
        </LimitExcept>
    </Directory>
</VirtualHost>
" > $PC_CONFIG/apache2/sites-enabled/df-vhost.conf 
echo "
<VirtualHost *:80>
    ServerName $QE_SUBDOMAIN 
    DocumentRoot $QE_ROOT_DIR
    ServerAdmin webmaster@$DOMAIN

    <Directory $QE_ROOT_DIR>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        AllowOverride None
        Require all granted
        RewriteEngine on
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^.*$ /index.php [L]

        <LimitExcept GET HEAD PUT DELETE PATCH POST>
            Allow from all
        </LimitExcept>
    </Directory>
</VirtualHost>
" > $PC_CONFIG/apache2/sites-enabled/qe-vhost.conf 

echo "
<VirtualHost *:80>
   ServerName $PLACITY_SUBDOMAIN
    DocumentRoot $APP_ROOT_DIR
    ServerAdmin webmaster@$DOMAIN

    <Directory $APP_ROOT_DIR>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        AllowOverride None
        Require all granted
        RewriteEngine on
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^.*$ /index.php [L]

        <LimitExcept GET HEAD PUT DELETE PATCH POST>
            Allow from all
        </LimitExcept>
    </Directory>
</VirtualHost>
" > $PC_CONFIG/apache2/sites-enabled/placity-vhost.conf 

rm /etc/apache2/conf-enabled/phppgadmin.conf
rm /etc/apache2/conf-enabled/alias-media.conf
rm /etc/apache2/sites-enabled/df-vhost.conf
rm /etc/apache2/sites-enabled/qe-vhost.conf
rm /etc/apache2/sites-enabled/placity-vhost.conf

ln $PC_CONFIG/apache2/conf-enabled/phppgadmin.conf /etc/apache2/conf-enabled
ln $PC_CONFIG/apache2/conf-enabled/alias-media.conf /etc/apache2/conf-enabled
ln $PC_CONFIG/apache2/sites-enabled/df-vhost.conf /etc/apache2/sites-enabled
ln $PC_CONFIG/apache2/sites-enabled/qe-vhost.conf /etc/apache2/sites-enabled
ln $PC_CONFIG/apache2/sites-enabled/placity-vhost.conf /etc/apache2/sites-enabled
service apache2 restart || service apache2 start


############################################################################
## ssl 
############################################################################
##
## echo "
##<IfModule mod_ssl.c>
##<VirtualHost *:443>
##   ServerName df.$DOMAIN 
##    DocumentRoot /opt/df2/public
##    ServerAdmin webmaster@$DOMAIN
##
##    <Directory /opt/df2/public>
##        Options -Indexes +FollowSymLinks -MultiViews
##        AllowOverride All
##        AllowOverride None
##        Require all granted
##        RewriteEngine on
##        RewriteBase /
##        RewriteCond %{REQUEST_FILENAME} !-f
##        RewriteCond %{REQUEST_FILENAME} !-d
##        RewriteRule ^.*$ /index.php [L]
##
##        <LimitExcept GET HEAD PUT DELETE PATCH POST>
##            Allow from all
##        </LimitExcept>
##    </Directory>
##</VirtualHost>
##</IfModule>
##" > /etc/apache2/sites-available/df-ssl.conf 

## echo "
##<IfModule mod_ssl.c>
##<VirtualHost *:443>
##   ServerName df.$DOMAIN 
##    DocumentRoot /opt/df2/public
##    ServerAdmin webmaster@$DOMAIN
##
##    <Directory /opt/df2/public>
##        Options -Indexes +FollowSymLinks -MultiViews
##        AllowOverride All
##        AllowOverride None
##        Require all granted
##        RewriteEngine on
##        RewriteBase /
##        RewriteCond %{REQUEST_FILENAME} !-f
##        RewriteCond %{REQUEST_FILENAME} !-d
##        RewriteRule ^.*$ /index.php [L]
##
##        <LimitExcept GET HEAD PUT DELETE PATCH POST>
##            Allow from all
##        </LimitExcept>
##    </Directory>
##</VirtualHost>
##</IfModule>
##" > $PC_CONFIG/apache2/sites-enabled/qe-ssl.conf 

## echo "
##<IfModule mod_ssl.c>
##<VirtualHost *:443>
##   ServerName df.$DOMAIN 
##    DocumentRoot /opt/df2/public
##    ServerAdmin webmaster@$DOMAIN
##
##    <Directory /opt/df2/public>
##        Options -Indexes +FollowSymLinks -MultiViews
##        AllowOverride All
##        AllowOverride None
##        Require all granted
##        RewriteEngine on
##        RewriteBase /
##        RewriteCond %{REQUEST_FILENAME} !-f
##        RewriteCond %{REQUEST_FILENAME} !-d
##        RewriteRule ^.*$ /index.php [L]
##
##        <LimitExcept GET HEAD PUT DELETE PATCH POST>
##            Allow from all
##        </LimitExcept>
##    </Directory>
##</VirtualHost>
##</IfModule>
##" > $PC_CONFIG/apache2/sites-enabled/placity-ssl.conf
## 

## ln $PC_CONFIG/apache2/sites-enabled/df-ssl.conf /etc/apache2/sites-enabled
## ln $PC_CONFIG/apache2/sites-enabled/qe-ssl.conf /etc/apache2/sites-enabled
## ln $PC_CONFIG/apache2/sites-enabled/placity-ssl.conf /etc/apache2/sites-enabled
## service apache2 restart || service apache2 start
############################################################################ 
