--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.10
-- Dumped by pg_dump version 9.3.1
-- Started on 2016-02-16 22:19:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2803 (class 0 OID 26446)
-- Dependencies: 267
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO organization (id, name, description, orga_admin) VALUES (1, 'albus-it', 'Beschreibung der Organisation albus-it', 1);
INSERT INTO organization (id, name, description, orga_admin) VALUES (2, 'test-orga', 'test-orga1', 2);
INSERT INTO organization (id, name, description, orga_admin) VALUES (8, 'Orga-XP', 'Windows 7', 2);
INSERT INTO organization (id, name, description, orga_admin) VALUES (9, 'Steffens Orga', 'Steffen', 12);


--
-- TOC entry 2794 (class 0 OID 26393)
-- Dependencies: 256
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO address (id_orga, street, postcode, city, country) VALUES (1, 'Im Kloster 8', 56179, 'Niederwerth', 'Germany');
INSERT INTO address (id_orga, street, postcode, city, country) VALUES (8, 'Goethestr.', 55435, 'Gau-Algesheim', 'Deutschland');
INSERT INTO address (id_orga, street, postcode, city, country) VALUES (9, 'Affe', 896, 'Bieber', 'Deutschland');


--
-- TOC entry 2722 (class 0 OID 25300)
-- Dependencies: 172
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (22, 'f.koegel', NULL, NULL, '2016-01-19 15:51:27', 'f.koegel@placity.de', '$2y$10$LoaFJD8Yr/tX2mYrqIjUce0b754QXBtWUOo4oF0zokRFPlz5n9wb6', true, true, NULL, NULL, NULL, 'y', NULL, NULL, '2016-01-14 09:22:48', '2016-01-19 15:51:27', 1, NULL, NULL, NULL);
INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (3, 'vanessa.m.merz', 'Vanessa', 'Merz', '2016-02-15 09:49:01', 'vanessa.m.merz@fh-bingen.de', '$2y$10$UpA5pFDEMsMrYdgMMh4SjeTlfbdJT1JuKbzxZik2wUNGdfVGlxrWW', true, true, NULL, NULL, NULL, 'y', NULL, NULL, '2015-12-29 16:13:59', '2016-02-15 09:49:01', 1, 2, NULL, NULL);
INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (20, 'paul.koch', NULL, NULL, '2016-02-15 22:53:39', 'paul.koch@fh-bingen.de', '$2y$10$KUumJPErjKobuOVAfw8R5eF7rwoMpH4IP0515FRunXZDvp6p/kzvq', true, true, NULL, NULL, NULL, 'y', NULL, NULL, '2016-01-13 09:04:43', '2016-02-15 22:53:39', 1, NULL, NULL, NULL);
INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (1, 'Albus', 'Alexander', 'Weiß', '2016-02-16 04:51:09', 'info@albus-it.com', '$2y$10$2eowG1xUu7obeO.LKGvIBOBH2RJ0e0DUhk679eI8//LWXF5RREEeO', true, true, NULL, NULL, NULL, 'y', 1, NULL, '2015-12-29 16:12:09', '2016-02-16 04:51:09', NULL, 1, NULL, NULL);
INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (2, 'steffen.hollenbach', 'Steffen', 'Hollenbach', '2016-02-02 10:18:58', 'steffen.hollenbach@fh-bingen.de', '$2y$10$vnuJ2jpFGjh5fa0WF7ufb.sTn9itKHvskL1mrr5U6led10YgB1UIG', true, true, '', NULL, NULL, 'y', NULL, NULL, '2015-12-29 16:13:30', '2016-02-02 10:18:58', 1, 2, NULL, NULL);
INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (28, 'MD5_User', 'MD5', 'oldpwhash', '2016-02-05 01:19:48', 'test@md5.de', '$2y$10$Ucloe7rT2a8dQsqC9bNXqOaUghsV69NI8ZAt0iOqSTbvVNuyFGii6', false, true, NULL, NULL, NULL, 'y', NULL, NULL, '2016-02-05 00:00:00', '2016-02-05 01:19:48', NULL, 1, NULL, NULL);
INSERT INTO "user" (id, name, first_name, last_name, last_login_date, email, password, is_sys_admin, is_active, phone, security_question, security_answer, confirm_code, default_app_id, remember_token, created_date, last_modified_date, created_by_id, last_modified_by_id, oauth_provider, adldap) VALUES (21, 'philipp.weller', NULL, NULL, '2016-02-12 17:10:19', 'philipp.weller@fh-bingen.de', '$2y$10$aJXzkuiVTdL8Ek7dpT2FOuYvB7AjmN2rx6bSzHaz3SxKuTbZcf0U2', true, true, NULL, NULL, NULL, 'y', NULL, NULL, '2016-01-13 09:08:28', '2016-02-12 17:10:19', 1, NULL, NULL, NULL);


--
-- TOC entry 2738 (class 0 OID 25500)
-- Dependencies: 188
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO role (id, name, description, is_active, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (2, 'BasicUser', 'Simple test User, Access to everything', true, '2016-01-05 14:30:32', '2016-01-05 14:31:19', 2, 2);
INSERT INTO role (id, name, description, is_active, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (3, 'Player', 'Player ist ein User der Placity App', true, '2016-01-10 06:15:36', '2016-02-12 17:10:36', 1, 21);


--
-- TOC entry 2726 (class 0 OID 25359)
-- Dependencies: 176
-- Data for Name: service_type; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('system', 'DreamFactory\Core\Services\System', NULL, 'System Management Service', 'Service supporting management of the system.', 'System', true, '2015-12-29 16:11:07', '2015-12-29 16:11:07');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('swagger', 'DreamFactory\Core\Services\Swagger', NULL, 'Swagger API Docs', 'API documenting and testing service using Swagger specifications.', 'API Doc', true, '2015-12-29 16:11:08', '2015-12-29 16:11:08');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('event', 'DreamFactory\Core\Services\Event', NULL, 'Event Service', 'Service that allows clients to subscribe to system broadcast events.', 'Event', true, '2015-12-29 16:11:08', '2015-12-29 16:11:08');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('script', 'DreamFactory\Core\Services\Script', 'DreamFactory\Core\Models\ScriptConfig', 'Custom Scripting Service', 'Service that allows client-callable scripts utilizing the system scripting.', 'Custom', false, '2015-12-29 16:11:08', '2015-12-29 16:11:08');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('local_file', 'DreamFactory\Core\Services\LocalFileService', 'DreamFactory\Core\Models\FilePublicPath', 'Local File Service', 'File service supporting the local file system.', 'File', false, '2015-12-29 16:11:08', '2015-12-29 16:11:08');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('local_email', 'DreamFactory\Core\Services\Email\Local', NULL, 'Local Email Service', 'Local email service using system configuration.', 'Email', false, '2015-12-29 16:11:08', '2015-12-29 16:11:08');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('smtp_email', 'DreamFactory\Core\Services\Email\Smtp', 'DreamFactory\Core\Models\SmtpConfig', 'SMTP Email Service', 'SMTP-based email service', 'Email', false, '2015-12-29 16:11:09', '2015-12-29 16:11:09');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('mailgun_email', 'DreamFactory\Core\Services\Email\MailGun', 'DreamFactory\Core\Models\MailGunConfig', 'Mailgun Email Service', 'Mailgun email service', 'Email', false, '2015-12-29 16:11:09', '2015-12-29 16:11:09');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('mandrill_email', 'DreamFactory\Core\Services\Email\Mandrill', 'DreamFactory\Core\Models\MandrillConfig', 'Mandrill Email Service', 'Mandrill email service', 'Email', false, '2015-12-29 16:11:09', '2015-12-29 16:11:09');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('sql_db', 'DreamFactory\Core\SqlDb\Services\SqlDb', 'DreamFactory\Core\SqlDb\Models\SqlDbConfig', 'SQL DB', 'Database service supporting SQL connections.', 'Database', false, '2015-12-29 16:11:19', '2015-12-29 16:11:19');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('mongodb', 'DreamFactory\Core\MongoDb\Services\MongoDb', 'DreamFactory\Core\MongoDb\Models\MongoDbConfig', 'MongoDB', 'Database service for MongoDB connections.', 'Database', false, '2015-12-29 16:11:20', '2015-12-29 16:11:20');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('couchdb', 'DreamFactory\Core\CouchDb\Services\CouchDb', 'DreamFactory\Core\CouchDb\Models\CouchDbConfig', 'CouchDB', 'Database service for CouchDB connections.', 'Database', false, '2015-12-29 16:11:20', '2015-12-29 16:11:20');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('rws', 'DreamFactory\Core\Rws\Services\RemoteWeb', 'DreamFactory\Core\Rws\Models\RwsConfig', 'Remote Web Service', 'A service to handle Remote Web Services', 'Custom', false, '2015-12-29 16:11:21', '2015-12-29 16:11:21');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('soap', 'DreamFactory\Core\Soap\Services\Soap', 'DreamFactory\Core\Soap\Models\SoapConfig', 'SOAP Service', 'A service to handle SOAP Services', 'Custom', false, '2015-12-29 16:11:21', '2015-12-29 16:11:21');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('aws_s3', 'DreamFactory\Core\Aws\Services\S3', 'DreamFactory\Core\Aws\Components\AwsS3Config', 'AWS S3', 'File storage service supporting the AWS S3 file system.', 'File', false, '2015-12-29 16:11:21', '2015-12-29 16:11:21');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('aws_dynamodb', 'DreamFactory\Core\Aws\Services\DynamoDb', 'DreamFactory\Core\Aws\Models\AwsConfig', 'AWS DynamoDB', 'A database service supporting the AWS DynamoDB system.', 'Database', false, '2015-12-29 16:11:21', '2015-12-29 16:11:21');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('aws_sns', 'DreamFactory\Core\Aws\Services\Sns', 'DreamFactory\Core\Aws\Models\AwsConfig', 'AWS SNS', 'Push notification service supporting the AWS SNS system.', 'Notification', false, '2015-12-29 16:11:21', '2015-12-29 16:11:21');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('aws_ses', 'DreamFactory\Core\Aws\Services\Ses', 'DreamFactory\Core\Aws\Models\AwsConfig', 'AWS SES', 'Email service supporting the AWS SES system.', 'Email', false, '2015-12-29 16:11:22', '2015-12-29 16:11:22');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('rackspace_cloud_files', 'DreamFactory\Core\Rackspace\Services\OpenStackObjectStore', 'DreamFactory\Core\Rackspace\Components\RackspaceCloudFilesConfig', 'Rackspace Cloud Files', 'File service supporting Rackspace Cloud Files Storage system.', 'File', false, '2015-12-29 16:11:22', '2015-12-29 16:11:22');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('openstack_obect_storage', 'DreamFactory\Core\Rackspace\Services\OpenStackObjectStore', 'DreamFactory\Core\Rackspace\Components\OpenStackObjectStorageConfig', 'OpenStack Object Storage', 'File service supporting OpenStack Object Storage system.', 'File', false, '2015-12-29 16:11:22', '2015-12-29 16:11:22');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('salesforce_db', 'DreamFactory\Core\Salesforce\Services\SalesforceDb', 'DreamFactory\Core\Salesforce\Models\SalesforceConfig', 'SalesforceDB', 'Database service for Salesforce connections.', 'Database', false, '2015-12-29 16:11:22', '2015-12-29 16:11:22');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('user', 'DreamFactory\Core\User\Services\User', 'DreamFactory\Core\User\Models\UserConfig', 'User service', 'User service to allow user management.', 'User', true, '2015-12-29 16:11:23', '2015-12-29 16:11:23');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('oauth_facebook', 'DreamFactory\Core\OAuth\Services\Facebook', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'Facebook OAuth', 'OAuth service for supporting Facebook authentication and API access.', 'OAuth', false, '2015-12-29 16:11:24', '2015-12-29 16:11:24');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('oauth_twitter', 'DreamFactory\Core\OAuth\Services\Twitter', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'Twitter OAuth', 'OAuth service for supporting Twitter authentication and API access.', 'OAuth', false, '2015-12-29 16:11:24', '2015-12-29 16:11:24');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('oauth_github', 'DreamFactory\Core\OAuth\Services\Github', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'GitHub OAuth', 'OAuth service for supporting GitHub authentication and API access.', 'OAuth', false, '2015-12-29 16:11:25', '2015-12-29 16:11:25');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('oauth_google', 'DreamFactory\Core\OAuth\Services\Google', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'Google OAuth', 'OAuth service for supporting Google authentication and API access.', 'OAuth', false, '2015-12-29 16:11:25', '2015-12-29 16:11:25');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('oauth_linkedin', 'DreamFactory\Core\OAuth\Services\LinkedIn', 'DreamFactory\Core\OAuth\Models\OAuthConfig', 'LinkedIn OAuth', 'OAuth service for supporting LinkedIn authentication and API access.', 'OAuth', false, '2015-12-29 16:11:25', '2015-12-29 16:11:25');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('adldap', 'DreamFactory\Core\ADLdap\Services\ADLdap', 'DreamFactory\Core\ADLdap\Models\ADConfig', 'Active Directory LDAP', 'A service for supporting Active Directory integration', 'LDAP', false, '2015-12-29 16:11:25', '2015-12-29 16:11:25');
INSERT INTO service_type (name, class_name, config_handler, label, description, "group", singleton, created_date, last_modified_date) VALUES ('ldap', 'DreamFactory\Core\ADLdap\Services\LDAP', 'DreamFactory\Core\ADLdap\Models\LDAPConfig', 'Standard LDAP', 'A service for supporting Open LDAP integration', 'LDAP', false, '2015-12-29 16:11:26', '2015-12-29 16:11:26');


--
-- TOC entry 2729 (class 0 OID 25380)
-- Dependencies: 179
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (1, 'system', 'System Management', 'Service for managing system resources.', true, 'system', false, false, '2015-12-29 16:11:13', '2015-12-29 16:11:13', NULL, NULL);
INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (2, 'api_docs', 'Live API Docs', 'API documenting and testing service.', true, 'swagger', false, false, '2015-12-29 16:11:14', '2015-12-29 16:11:14', NULL, NULL);
INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (5, 'email', 'Local Email Service', 'Email service used for sending user invites and/or password reset confirmation.', true, 'local_email', true, true, '2015-12-29 16:11:23', '2015-12-29 16:11:23', NULL, NULL);
INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (9, 'smtp', 'smtp-mail', '', true, 'smtp_email', true, true, '2015-12-29 16:47:49', '2016-01-04 15:55:25', 1, 2);
INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (6, 'user', 'User Management', 'Service for managing system users.', true, 'user', true, false, '2015-12-29 16:11:24', '2016-01-06 11:35:21', NULL, 2);
INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (3, 'files', 'Local File Storage', 'Service for accessing local file storage.', true, 'local_file', true, true, '2015-12-29 16:11:14', '2016-01-10 07:32:44', NULL, 1);
INSERT INTO service (id, name, label, description, is_active, type, mutable, deletable, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (4, 'db', 'Local SQL Database', 'Service for accessing pgSQL database', true, 'sql_db', true, true, '2015-12-29 16:11:20', '2016-02-04 21:37:06', NULL, 1);


--
-- TOC entry 2761 (class 0 OID 25792)
-- Dependencies: 211
-- Data for Name: app; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO app (id, name, api_key, description, is_active, type, path, url, storage_service_id, storage_container, requires_fullscreen, allow_fullscreen_toggle, toggle_location, role_id, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (1, 'admin', '6498a8ad1beb9d84d63035c5d1120c007fad6de706734db9689f8996707e0f7d', 'An application for administering this instance.', true, 3, 'dreamfactory/dist/index.html', NULL, NULL, NULL, false, true, 'top', NULL, '2015-12-29 16:11:18', '2015-12-29 16:11:18', NULL, NULL);
INSERT INTO app (id, name, api_key, description, is_active, type, path, url, storage_service_id, storage_container, requires_fullscreen, allow_fullscreen_toggle, toggle_location, role_id, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (3, 'filemanager', 'b5cb82af7b5d4130f36149f90aa2746782e59a872ac70454ac188743cb55b0ba', 'An application for managing file services.', true, 3, 'filemanager/index.html', NULL, NULL, NULL, false, true, 'top', NULL, '2015-12-29 16:11:19', '2016-01-11 20:45:03', NULL, 1);
INSERT INTO app (id, name, api_key, description, is_active, type, path, url, storage_service_id, storage_container, requires_fullscreen, allow_fullscreen_toggle, toggle_location, role_id, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (12, 'placity-app', '427994563fdc8f1159ff7d04bd00c62ecab42f7bcd3f9e99ae2a5a38f5408d3d', '', true, 0, NULL, NULL, NULL, NULL, false, true, 'top', 3, '2016-01-13 08:32:38', '2016-01-13 09:45:10', 1, 1);
INSERT INTO app (id, name, api_key, description, is_active, type, path, url, storage_service_id, storage_container, requires_fullscreen, allow_fullscreen_toggle, toggle_location, role_id, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (2, 'swagger', '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88', 'A Swagger-base application allowing viewing and testing API documentation.', true, 3, 'swagger/index.html', NULL, NULL, NULL, false, true, 'top', NULL, '2015-12-29 16:11:18', '2016-02-16 02:34:44', NULL, 1);
INSERT INTO app (id, name, api_key, description, is_active, type, path, url, storage_service_id, storage_container, requires_fullscreen, allow_fullscreen_toggle, toggle_location, role_id, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (6, 'QuestionEditor', 'b06beaf7a884e085b20b8c1b10d2f4712c49526c4e11597d70bff5fa4ee05988', 'Question-Editor', true, 0, NULL, 'http://143.93.91.92/steffen/index.html', NULL, NULL, false, true, 'top', 2, '2016-01-05 14:29:01', '2016-02-16 04:11:29', 2, 1);


--
-- TOC entry 2765 (class 0 OID 25858)
-- Dependencies: 215
-- Data for Name: app_group; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2815 (class 0 OID 0)
-- Dependencies: 214
-- Name: app_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_group_id_seq', 1, false);


--
-- TOC entry 2816 (class 0 OID 0)
-- Dependencies: 210
-- Name: app_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_id_seq', 12, true);


--
-- TOC entry 2763 (class 0 OID 25830)
-- Dependencies: 213
-- Data for Name: app_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2817 (class 0 OID 0)
-- Dependencies: 212
-- Name: app_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_lookup_id_seq', 1, false);


--
-- TOC entry 2767 (class 0 OID 25878)
-- Dependencies: 217
-- Data for Name: app_to_app_group; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2818 (class 0 OID 0)
-- Dependencies: 216
-- Name: app_to_app_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('app_to_app_group_id_seq', 1, false);


--
-- TOC entry 2778 (class 0 OID 26025)
-- Dependencies: 228
-- Data for Name: aws_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2757 (class 0 OID 25760)
-- Dependencies: 207
-- Data for Name: cloud_email_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- Data for Name: contenttype; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (1, 'ct_text', 'ContentType Text: Instanz des ct_text,  ist ein einfacher Fließtext in einer Route', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "text"
              ],
              "properties": {
                "text": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctTextAppCtrl.js', 'ctTextAppTpl.html', 'ctTextQECtrl.js', 'ctTextQETpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (4, 'headline', 'Überschrift ', NULL, 'ctHeadlineAppCtrl.js', 'ctHeadlineAppTpl.html', 'ctHeadlineQECtrl.js', 'ctHeadlineQETmpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (2, 'ct_MultipleChoice', 'ContentType MultipleChoice: Instanz des ct_MultipleChoice, ist eine Frage mit mehreren Antwortmöglichkeiten', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "text",
                "answer",
                "cmessage",
                "wmessage"
              ],
              "properties": {
                "text": {
                  "type": "string",
                  "minLength": 1
                },
                "answer": {
                  "type": "string",
                  "minLength": 1
                },
                "choice": {
                  "type": "array",
                  "items": {
                    "required": [],
                    "properties": {}
                  }
                },
                "cmessage": {
                  "type": "string",
                  "minLength": 1
                },
                "wmessage": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          }
        }
      }
    },
    "points": {
      "type": "number"
    }
  },
  "required": [
    "languages",
    "points"
  ]
}
', 'ctMultipleChoice.js', 'ctMultipleChoice.html', 'ctMultipleChoiceQE.js', 'ctMultipleChoiceQE.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (3, 'ct_Dialog', 'ContentType Dialog: Instanz des ct_Dialog, ist eine Unterhaltung zwischen 2 Personen', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "avatar1",
                "avatar2"
              ],
              "properties": {
                "avatar1": {
                  "type": "string",
                  "minLength": 1
                },
                "avatar2": {
                  "type": "string",
                  "minLength": 1
                },
                "text": {
                  "type": "array",
                  "items": {
                    "required": [],
                    "properties": {}
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctDialog.js', 'ctDialog.html', 'ctDialogQE.js', 'ctDialogQE.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (5, 'ct_FrageFreiText', 'ContentType FrageFreitext: Textuelle Frage, dessen Antwort frei formuliert wird.', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang",
          "cmessage",
          "wmessage"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "text",
                "answer"
              ],
              "properties": {
                "text": {
                  "type": "string",
                  "minLength": 1
                },
                "answer": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          },
          "cmessage": {
            "type": "string",
            "minLength": 1
          },
          "wmessage": {
            "type": "string",
            "minLength": 1
          }
        }
      }
    },
    "points": {
      "type": "number"
    }
  },
  "required": [
    "languages",
    "points"
  ]
}
', 'ctFrageFreitext.js', 'ctFrageFreitext.html', 'ctFrageFreitextQETmpl.js', 'ctFrageFreitextQETmpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (6, 'ct_Audio', 'ContentType Audio: Audio Inhalt', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "id",
                "audiofileurl",
                "info",
                "autoplay"
              ],
              "properties": {
                "id": {
                  "type": "string",
                  "minLength": 1
                },
                "audiofileurl": {
                  "type": "string",
                  "minLength": 1
                },
                "info": {
                  "type": "string",
                  "minLength": 1
                },
                "autoplay": {
                  "type": "boolean"
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctAudio.js', 'ctAudioApp.html', 'ctAudioQETmpl.js', 'ctAudioQETmpl.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (7, 'ct_Video', 'ContentType Video: Video Inhalt', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "contenttype",
                "src",
                "description",
                "autoplay"
              ],
              "properties": {
                "contenttype": {
                  "type": "string",
                  "minLength": 1
                },
                "src": {
                  "type": "string",
                  "minLength": 1
                },
                "description": {
                  "type": "string",
                  "minLength": 1
                },
                "autoplay": {
                  "type": "boolean"
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctVideo.js', 'ctVideo.html', 'ctVideoQETmpl.js', 'ctVideoQETml.html', NULL, NULL);
INSERT INTO contenttype(id,name,description,data_schema,app_ctrl_class,app_html_tpl_file,qe_ctrl_class,qe_html_tpl_file,sid,helptext) VALUES (8, 'ct_Image', 'ContentType Image: Bild Inhalt', '{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "",
  "type": "object",
  "properties": {
    "languages": {
      "type": "array",
      "uniqueItems": true,
      "minItems": 1,
      "items": {
        "required": [
          "lang"
        ],
        "properties": {
          "lang": {
            "type": "string",
            "minLength": 1
          },
          "fields": {
            "type": "array",
            "uniqueItems": true,
            "minItems": 1,
            "items": {
              "required": [
                "src",
                "alt"
              ],
              "properties": {
                "src": {
                  "type": "string",
                  "minLength": 1
                },
                "alt": {
                  "type": "string",
                  "minLength": 1
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "languages"
  ]
}
', 'ctImage.js', 'ctImage.html', 'ctImageQETmpl.js', 'ctImageQETmpl.html', NULL, NULL);


--
-- TOC entry 2806 (class 0 OID 26467)
-- Dependencies: 272
-- Data for Name: route; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO route (id, name, title, subtitle, description, image, app_scheme, android_scheme, win_scheme, private, id_orga, id_user, id_group) VALUES (32, 'neueRoute', NULL, NULL, NULL, NULL, 'APP_SCHEME', 'android_scheme', NULL, ' ', NULL, 2, NULL);
INSERT INTO route (id, name, title, subtitle, description, image, app_scheme, android_scheme, win_scheme, private, id_orga, id_user, id_group) VALUES (770, 'Text_Test_Seite_1', 'text test titel', 'text test subtitel', 'text test', NULL, 'APP_SCHEME', 'android_scheme', NULL, '0', NULL, 2, NULL);
INSERT INTO route (id, name, title, subtitle, description, image, app_scheme, android_scheme, win_scheme, private, id_orga, id_user, id_group) VALUES (2, 'Mega Geile SUper Route', 'title', 'subtitle', 'description', 'IMAGE-URL|BASE64', 'app_scheme', 'android_scheme', 'win_scheme', '0', NULL, 12, NULL);
INSERT INTO route (id, name, title, subtitle, description, image, app_scheme, android_scheme, win_scheme, private, id_orga, id_user, id_group) VALUES (31, 'neueRoute 31', NULL, NULL, NULL, NULL, 'APP_SCHEME', 'android_scheme', NULL, ' ', NULL, 2, NULL);
INSERT INTO route (id, name, title, subtitle, description, image, app_scheme, android_scheme, win_scheme, private, id_orga, id_user, id_group) VALUES (1, 'testroute', 'title', 'subtitle', 'description', 'IMAGE-URL|BASE64', 'app_scheme', 'android_scheme', 'win_scheme', '0', 1, 2, 1);


--
-- TOC entry 2804 (class 0 OID 26455)
-- Dependencies: 269
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO page (id, next, page_name, pos, id_route) VALUES (1, NULL, 'seite1', 1, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (2, NULL, 'seite_2', 2, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (19, NULL, 'Seite_08', 10, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (3, NULL, 'seite_3', 3, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (4, NULL, 'Seite_4', 4, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (5, NULL, 'Seite_5', 5, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (6, NULL, 'Seite_6', 6, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (7, NULL, 'Seite_7', 7, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (8, NULL, 'Seite_8', 8, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (10, NULL, 'Seite_10', 1, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (11, NULL, 'Seite_11', 2, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (12, NULL, 'Seite_12', 3, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (13, NULL, 'Seite_13', 4, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (14, NULL, 'Seite_14', 5, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (15, NULL, 'Seite_15', 6, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (16, NULL, 'Seite_05', 7, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (18, NULL, 'Seite_07', 9, 2);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (17, NULL, 'Seite_06', 8, NULL);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (98, NULL, 'Text_Test_Seite_1', 1, 770);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (9, NULL, 'Seite_9', 9, 1);
INSERT INTO page (id, next, page_name, pos, id_route) VALUES (99, NULL, 'Text_Test_Seite_2', 2, 770);


--
-- TOC entry 2809 (class 0 OID 26624)
-- Dependencies: 278
-- Data for Name: content; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (1, 1, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 1);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (14, 2, 5, '{"languages":[{"lang":"de_DE","fields":[{"text":"<p>Wann wurde Rom gegründet?</p>","answer":"753","cmessage": "richtig","wmessage": "falsch"}]},{"lang":"en_EN","fields":[{"text":"<p>When was Rome foundet?</p>","answer":"753","cmessage": "right","wmessage": "wrong"}]}]}', 2);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (19, 3, 6, '{"languages":[{"lang":"de_DE","fields":[{"id":"14","audiofileurl":"http://143.93.91.92/upload/2016_1_16/Ab%20An%20De%20See.mp3","info":"InfoText"}]},{"lang":"en_EN","fields":[{"id":"14","audiofileurl":"http://143.93.91.92/upload/2016_1_16/Ab%20An%20De%20See.mp3","info":"InfoText"}]}]}', 4);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (24, 5, 8, '{"languages": [{"lang": "de_DE","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]},{"lang": "en_EN","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]}]}', 2);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (25, 5, 8, '{"languages": [{"lang": "de_DE","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]},{"lang": "en_EN","fields": [{"src": "http://143.93.91.92/philipp/avatar1.jpg","alt": "Dies ist ein AvatarBild"}]}]}', 3);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (16, 3, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 1);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (2, 1, 2, '{"languages":[{"lang":"de_DE","fields":[{"text":"<p>Wann wurde Rom gegründet?</p>","answer":"753","choice":["357","17","753","166","1652"],"cmessage": "richtig","wmessage": "falsch"}]},{"lang":"en_EN","fields":[{"text":"<p>When was Rome foundet?</p>","answer":"753","choice":["357","17","753","166","1652"],"cmessage": "right","wmessage": "wrong"}]}]}', 2);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (22, 4, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>2Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 2);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (23, 5, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>3Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 1);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (17, 3, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 2);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (18, 3, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 3);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (20, 3, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 5);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (13, 2, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 1);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (9, 1, 3, '{"languages":[{"lang": "de_DE","fields": [{"avatar1" : "http://143.93.91.92/philipp/avatar1.jpg","avatar2" : "http://143.93.91.92/philipp/avatar1.jpg","text" :["<p>Hi Alex</p>","<p>Hi Paul</p>","<p>Na wie gehts?</p>","<p>Alles bestens, kann nicht klagen</p>"]}]},{"lang": "en_EN","fields": [{"avatar1" : "http://143.93.91.92/philipp/avatar1.jpg","avatar2" : "http://143.93.91.92/philipp/avatar1.jpg","text" :["<p>Hi Alex</p>","<p>Hi Paul</p>","<p>How are you?</p>","<p>Quite nice, thanks!</p>"]}]}]}', 3);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (21, 4, 7, '{"languages": [{"lang": "de_DE","fields": [{"contenttype": "video", "src": "https://upload.wikimedia.org/wikipedia/commons/transcoded/8/87/Schlossbergbahn.webm/Schlossbergbahn.webm.480p.webm", "description": "Eine Bahn faehrt einen Berg hoch", "autoplay": true}]},{"lang": "en_EN","fields": [{"contenttype": "video", "src": "https://upload.wikimedia.org/wikipedia/commons/transcoded/8/87/Schlossbergbahn.webm/Schlossbergbahn.webm.480p.webm", "description": "Eine Bahn faehrt einen Berg hoch", "autoplay": true}]}]}', 1);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (98, 98, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text</p> <p>der vom <h1>Nutzer</h1> als contenttype text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 1);
INSERT INTO content (id, id_page, id_content_type, data_obj, pos) VALUES (15, 2, 1, '{"languages": [{"lang": "de_DE","fields": [{"text": "<p>Hier beindet sich html text,</p> <p>der vom <h1 style=\"color: red\">MEGA MAN</h1> als <span style=\"color: blue\">magischer</span>  text angelegt wurde</p>"}]},{"lang": "en_EN","fields": [{"text": "<p>You''ll find HTML Text here</p> <p>which the<h1>user</h1>produced as ContentType Text</p>"}]}]}', 3);


--
-- TOC entry 2755 (class 0 OID 25721)
-- Dependencies: 205
-- Data for Name: cors_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO cors_config (id, path, origin, header, method, max_age, enabled, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (1, '*', '*', '*', 31, 0, true, '2015-12-29 17:06:43', '2016-01-13 20:07:50', 1, 2);


--
-- TOC entry 2819 (class 0 OID 0)
-- Dependencies: 204
-- Name: cors_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('cors_config_id_seq', 1, true);


--
-- TOC entry 2775 (class 0 OID 25984)
-- Dependencies: 225
-- Data for Name: couchdb_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2752 (class 0 OID 25679)
-- Dependencies: 202
-- Data for Name: db_field_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (1, 1, 'sql_db_config', 'dsn', 'Connection String (DSN)', NULL, 'Specify the connection string for the database you''re connecting to.', NULL, NULL, NULL, '2015-12-29 16:11:19', '2015-12-29 16:11:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (2, 1, 'sql_db_config', 'options', 'Driver Options', NULL, 'A key=>value array of driver-specific connection options.', NULL, NULL, NULL, '2015-12-29 16:11:19', '2015-12-29 16:11:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (3, 1, 'sql_db_config', 'attributes', 'Driver Attributes', NULL, 'A key=>value array of driver-specific attributes.', NULL, NULL, NULL, '2015-12-29 16:11:20', '2015-12-29 16:11:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (4, 4, 'contact', 'id', 'Contact Id', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (5, 4, 'contact', 'first_name', NULL, NULL, NULL, NULL, '{"not_empty":{"on_fail":"First name value must not be empty."}}', NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (6, 4, 'contact', 'last_name', NULL, NULL, NULL, NULL, '{"not_empty":{"on_fail":"Last name value must not be empty."}}', NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (7, 4, 'contact', 'image_url', 'image_url', NULL, NULL, NULL, '{"url":{"on_fail":"Not a valid URL value."}}', NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (8, 4, 'contact', 'twitter', 'Twitter Handle', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (9, 4, 'contact', 'skype', 'Skype Account', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (10, 4, 'contact', 'notes', 'notes', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (11, 4, 'contact_info', 'id', 'Info Id', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (12, 4, 'contact_info', 'ordinal', NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (13, 4, 'contact_info', 'contact_id', NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (14, 4, 'contact_info', 'info_type', NULL, NULL, NULL, '["work","home","mobile","other"]', '{"not_empty":{"on_fail":"Information type can not be empty."},"picklist":{"on_fail":"Not a valid information type."}}', NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (15, 4, 'contact_info', 'phone', 'Phone Number', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (16, 4, 'contact_info', 'email', 'Email Address', NULL, NULL, NULL, '{"email":{"on_fail":"Not a valid email address."}}', NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (17, 4, 'contact_info', 'address', 'Street Address', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (18, 4, 'contact_info', 'city', 'city', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (19, 4, 'contact_info', 'state', 'state', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (20, 4, 'contact_info', 'zip', 'zip', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (21, 4, 'contact_info', 'country', 'country', NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (22, 4, 'contact_group', 'id', NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (23, 4, 'contact_group', 'name', NULL, NULL, NULL, NULL, '{"not_empty":{"on_fail":"Group name value must not be empty."}}', NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (24, 4, 'contact_group_relationship', 'id', NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (25, 4, 'contact_group_relationship', 'contact_id', NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (26, 4, 'contact_group_relationship', 'contact_group_id', NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (27, 4, 'TestToDelete', 'example_field', 'Example Field', NULL, NULL, NULL, NULL, NULL, '2016-01-05 22:08:59', '2016-01-05 22:08:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO db_field_extras (id, service_id, "table", field, label, extra_type, description, picklist, validation, client_info, created_date, last_modified_date, created_by_id, last_modified_by_id, alias, db_function, ref_service_id, ref_table, ref_fields, ref_on_update, ref_on_delete) VALUES (28, 4, 'TestToDelete2', 'example_field', 'Example Field', NULL, NULL, NULL, NULL, NULL, '2016-01-05 22:09:23', '2016-01-05 22:09:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 2820 (class 0 OID 0)
-- Dependencies: 201
-- Name: db_field_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_field_extras_id_seq', 28, true);


--
-- TOC entry 2792 (class 0 OID 26206)
-- Dependencies: 242
-- Data for Name: db_relationship_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO db_relationship_extras (id, service_id, "table", relationship, alias, label, description, always_fetch, flatten, flatten_drop_prefix, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (5, 4, 'route', 'highscore_by_id_route', NULL, 'Highscore By Id Route', NULL, false, false, false, '2016-01-15 21:39:48', '2016-01-15 21:39:48', NULL, NULL);
INSERT INTO db_relationship_extras (id, service_id, "table", relationship, alias, label, description, always_fetch, flatten, flatten_drop_prefix, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (6, 4, 'route', 'page_by_id_route', NULL, 'Page By Id Route', NULL, false, false, false, '2016-01-16 01:23:19', '2016-01-16 01:23:19', NULL, NULL);


--
-- TOC entry 2821 (class 0 OID 0)
-- Dependencies: 241
-- Name: db_relationship_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_relationship_extras_id_seq', 6, true);


--
-- TOC entry 2750 (class 0 OID 25653)
-- Dependencies: 200
-- Data for Name: db_table_extras; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (1, 1, 'user', NULL, NULL, NULL, 'DreamFactory\Core\Models\User', NULL, '2015-12-29 16:11:14', '2015-12-29 16:11:14', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (2, 1, 'user_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\UserLookup', NULL, '2015-12-29 16:11:14', '2015-12-29 16:11:14', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (3, 1, 'user_to_app_to_role', NULL, NULL, NULL, 'DreamFactory\Core\Models\UserAppRole', NULL, '2015-12-29 16:11:14', '2015-12-29 16:11:14', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (4, 1, 'service', NULL, NULL, NULL, 'DreamFactory\Core\Models\Service', NULL, '2015-12-29 16:11:15', '2015-12-29 16:11:15', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (5, 1, 'service_type', NULL, NULL, NULL, 'DreamFactory\Core\Models\ServiceType', NULL, '2015-12-29 16:11:15', '2015-12-29 16:11:15', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (6, 1, 'service_doc', NULL, NULL, NULL, 'DreamFactory\Core\Models\ServiceDoc', NULL, '2015-12-29 16:11:15', '2015-12-29 16:11:15', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (7, 1, 'role', NULL, NULL, NULL, 'DreamFactory\Core\Models\Role', NULL, '2015-12-29 16:11:15', '2015-12-29 16:11:15', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (8, 1, 'role_service_access', NULL, NULL, NULL, 'DreamFactory\Core\Models\RoleServiceAccess', NULL, '2015-12-29 16:11:15', '2015-12-29 16:11:15', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (9, 1, 'role_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\RoleLookup', NULL, '2015-12-29 16:11:16', '2015-12-29 16:11:16', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (10, 1, 'app', NULL, NULL, NULL, 'DreamFactory\Core\Models\App', NULL, '2015-12-29 16:11:16', '2015-12-29 16:11:16', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (11, 1, 'app_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\AppLookup', NULL, '2015-12-29 16:11:16', '2015-12-29 16:11:16', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (12, 1, 'app_group', NULL, NULL, NULL, 'DreamFactory\Core\Models\AppGroup', NULL, '2015-12-29 16:11:16', '2015-12-29 16:11:16', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (13, 1, 'app_to_app_group', NULL, NULL, NULL, 'DreamFactory\Core\Models\AppToAppGroup', NULL, '2015-12-29 16:11:16', '2015-12-29 16:11:16', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (14, 1, 'system_resource', NULL, NULL, NULL, 'DreamFactory\Core\Models\SystemResource', NULL, '2015-12-29 16:11:17', '2015-12-29 16:11:17', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (15, 1, 'script_type', NULL, NULL, NULL, 'DreamFactory\Core\Models\ScriptType', NULL, '2015-12-29 16:11:17', '2015-12-29 16:11:17', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (16, 1, 'event_script', NULL, NULL, NULL, 'DreamFactory\Core\Models\EventScript', NULL, '2015-12-29 16:11:17', '2015-12-29 16:11:17', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (17, 1, 'event_subscriber', NULL, NULL, NULL, 'DreamFactory\Core\Models\EventSubscriber', NULL, '2015-12-29 16:11:17', '2015-12-29 16:11:17', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (18, 1, 'email_template', NULL, NULL, NULL, 'DreamFactory\Core\Models\EmailTemplate', NULL, '2015-12-29 16:11:18', '2015-12-29 16:11:18', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (19, 1, 'system_setting', NULL, NULL, NULL, 'DreamFactory\Core\Models\Setting', NULL, '2015-12-29 16:11:18', '2015-12-29 16:11:18', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (20, 1, 'system_lookup', NULL, NULL, NULL, 'DreamFactory\Core\Models\Lookup', NULL, '2015-12-29 16:11:18', '2015-12-29 16:11:18', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (21, 1, 'sql_db_config', NULL, NULL, NULL, 'DreamFactory\Core\SqlDb\Models\SqlDbConfig', NULL, '2015-12-29 16:11:19', '2015-12-29 16:11:19', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (22, 1, 'role_adldap', NULL, NULL, NULL, 'DreamFactory\Core\ADLdap\Models\RoleADLdap', NULL, '2015-12-29 16:11:25', '2015-12-29 16:11:25', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (25, 4, 'contact', NULL, NULL, NULL, NULL, 'The main table for tracking contacts.', '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (26, 4, 'contact_info', NULL, NULL, NULL, NULL, 'The contact details sub-table, owned by contact table row.', '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (27, 4, 'contact_group', NULL, NULL, NULL, NULL, 'The main table for tracking groups of contact.', '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (28, 4, 'contact_group_relationship', NULL, NULL, NULL, NULL, 'The join table for tracking contacts in groups.', '2016-01-05 14:37:13', '2016-01-05 14:37:13', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (29, 4, 'TestToDelete', NULL, NULL, NULL, NULL, NULL, '2016-01-05 22:08:59', '2016-01-05 22:08:59', NULL, NULL, NULL);
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (30, 4, 'TestToDelete2', 'TestToDelete2', 'TestToDelete2', NULL, NULL, NULL, '2016-01-05 22:09:23', '2016-01-05 22:09:23', NULL, NULL, 'TestToDelete2');
INSERT INTO db_table_extras (id, service_id, "table", label, plural, name_field, model, description, created_date, last_modified_date, created_by_id, last_modified_by_id, alias) VALUES (31, 4, 'v_route_page_content_tojson', 'V Route Page Content Tojson', 'V Route Page Content Tojsons', NULL, NULL, NULL, '2016-01-11 08:37:53', '2016-01-11 08:37:53', NULL, NULL, NULL);


--
-- TOC entry 2822 (class 0 OID 0)
-- Dependencies: 199
-- Name: db_table_extras_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('db_table_extras_id_seq', 31, true);


--
-- TOC entry 2759 (class 0 OID 25775)
-- Dependencies: 209
-- Data for Name: email_parameters_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2823 (class 0 OID 0)
-- Dependencies: 208
-- Name: email_parameters_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('email_parameters_config_id_seq', 1, false);


--
-- TOC entry 2748 (class 0 OID 25630)
-- Dependencies: 198
-- Data for Name: email_template; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO email_template (id, name, description, "to", cc, bcc, subject, body_text, body_html, from_name, from_email, reply_to_name, reply_to_email, defaults, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (1, 'User Invite Default', 'Email sent to invite new users to your DreamFactory instance.', NULL, NULL, NULL, '[DF] New User Invitation', NULL, '<div style="padding: 10px;">
                                <p>
                                Hi {first_name},
                                </p>
                                <p>
                                    You have been invited to the DreamFactory Instance of {instance_name}. Go to the following url, enter the code below, and set
                                    your password to confirm your account.
                                    <br/>
                                    <br/>
                                    {link}
                                    <br/>
                                    <br/>
                                    Confirmation Code: {confirm_code}<br/>
                                </p>
                                <p>
                                    <cite>-- The Dream Team</cite>
                                </p>
                              </div>', 'DO NOT REPLY', 'no-reply@dreamfactory.com', NULL, NULL, NULL, '2015-12-29 16:11:23', '2015-12-29 17:07:04', NULL, 1);
INSERT INTO email_template (id, name, description, "to", cc, bcc, subject, body_text, body_html, from_name, from_email, reply_to_name, reply_to_email, defaults, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (3, 'Password Reset Default', 'Email sent to users following a request to reset their password.', NULL, NULL, NULL, '[DF] Password Reset', NULL, '<div style="padding: 10px;">
                                <p>
                                    Hi {first_name},
                                </p>
                                <p>
                                    You have requested to reset your password. Go to the following url, enter the code below, and set your new password.
                                    <br>
                                    <br>
                                    {link}
                                    <br>
                                    <br>
                                    Confirmation Code: {confirm_code}
                                </p>
                                <p>
                                    <cite>-- The Dream Team</cite>
                                </p>
                            </div>', 'DO NOT REPLY', 'no-reply@dreamfactory.com', NULL, NULL, NULL, '2015-12-29 16:11:23', '2015-12-29 17:07:22', NULL, 1);
INSERT INTO email_template (id, name, description, "to", cc, bcc, subject, body_text, body_html, from_name, from_email, reply_to_name, reply_to_email, defaults, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (2, 'User Registration Default', 'Email sent to new users to complete registration.', NULL, NULL, NULL, '[DF] Registration Confirmation', NULL, '<div style="padding: 10px;">
                                <p>
                                    Hi {first_name},
                                </p>
                                <p>
                                    You have registered an user account on the DreamFactory instance of {instance_name}. Go to the following url, enter the
                                    code below, and set your password to confirm your account.
                                    <br/>
                                    <br/>
                                    {link}
                                    <br/>
                                    <br/>
                                    Confirmation Code: {confirm_code}
                                    <br/>
                                </p>
                                <p>
                                    <cite>-- The Dream Team</cite>
                                </p>
                            </div>', 'DO NOT REPLY', 'no-reply@dreamfactory.com', NULL, NULL, NULL, '2015-12-29 16:11:23', '2015-12-29 17:07:30', NULL, 1);


--
-- TOC entry 2824 (class 0 OID 0)
-- Dependencies: 197
-- Name: email_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('email_template_id_seq', 3, true);


--
-- TOC entry 2732 (class 0 OID 25423)
-- Dependencies: 182
-- Data for Name: script_type; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO script_type (name, class_name, label, description, sandboxed, created_date, last_modified_date) VALUES ('php', 'DreamFactory\Core\Scripting\Engines\Php', 'PHP', 'Script handler using native PHP.', false, '2015-12-29 16:11:13', '2015-12-29 16:11:13');
INSERT INTO script_type (name, class_name, label, description, sandboxed, created_date, last_modified_date) VALUES ('nodejs', 'DreamFactory\Core\Scripting\Engines\NodeJs', 'Node.js', 'Server-side JavaScript handler using the Node.js engine.', false, '2015-12-29 16:11:13', '2015-12-29 16:11:13');
INSERT INTO script_type (name, class_name, label, description, sandboxed, created_date, last_modified_date) VALUES ('v8js', 'DreamFactory\Core\Scripting\Engines\V8Js', 'V8js', 'Server-side JavaScript handler using the V8js engine.', true, '2015-12-29 16:11:13', '2015-12-29 16:11:13');


--
-- TOC entry 2736 (class 0 OID 25473)
-- Dependencies: 186
-- Data for Name: event_script; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO event_script (name, type, is_active, affects_process, content, config, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES ('sample-scripts', 'v8js', false, false, '//	Here are a few examples to get you started. var_dump() can be used to log script output.

//****************** Pre-processing script on a table ******************

//	A script that is triggered by a POST on /api/v2/db/_table/<tablename>. Runs before the db call is made.
//  The script validates that every record in the request has a value for the name field.

var_dump(event.request); // outputs to file in storage/log of dreamfactory install directory

var lodash = require("lodash.min.js");

if (event.request.payload.resource) {

    lodash._.each (event.request.payload.resource, function( record ) {

        if (!record.name) {
            throw ''Name cannot be empty'';
        }
    });
}

//****************** Post-processing script on a table ******************

//  A script that is triggered by a GET on /api/v2/db/_table/<tablename>. Runs after the db call is made.
//  The script adds a new field to each record in the response.

var_dump(event.response); // outputs to file in storage/log of dreamfactory install directory

var lodash = require("lodash.min.js");

if (event.response.resource) {

    lodash._.each (event.response.resource, function( record ) {

        record.extraField = ''Feed the dog.'';
    });
}
', NULL, '2016-01-26 03:57:56', '2016-01-26 04:01:52', 1, 1);
INSERT INTO event_script (name, type, is_active, affects_process, content, config, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES ('db._table.player.post.pre_process', 'php', true, true, '$param= \Request::json()->all();
foreach($param as $e){
    shell_exec("echo $e >> /opt/df2/test_params.info");
    
}
 throw new \Exception(''Missing);', NULL, '2016-02-05 01:55:08', '2016-02-05 02:33:27', 1, 1);
INSERT INTO event_script (name, type, is_active, affects_process, content, config, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES ('files.{file_path}.post.pre_process', 'php', true, true, 'shell_exec("echo script > /opt/df2/file_script.info");', NULL, '2016-01-27 22:05:15', '2016-02-05 03:18:01', 1, 1);
INSERT INTO event_script (name, type, is_active, affects_process, content, config, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES ('db._table.mediafile.post.post_process', 'php', true, true, 'shell_exec(''nohup php /opt/pc4/df2/fileserver/videoencoder.php &'');
', NULL, '2016-02-04 21:40:14', '2016-02-16 15:46:03', 1, 1);


--
-- TOC entry 2735 (class 0 OID 25452)
-- Dependencies: 185
-- Data for Name: event_subscriber; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2825 (class 0 OID 0)
-- Dependencies: 184
-- Name: event_subscriber_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('event_subscriber_id_seq', 1, false);


--
-- TOC entry 2771 (class 0 OID 25928)
-- Dependencies: 221
-- Data for Name: file_service_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO file_service_config (service_id, public_path, container) VALUES (3, '[]', 'local');


--
-- TOC entry 2799 (class 0 OID 26422)
-- Dependencies: 261
-- Data for Name: group; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (1, 'albus-group', 2, 1);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (11, 'Group001', 1, 1);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (13, 'test2', 1, 1);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (12, 'test2', 3, 2);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (14, 'test4', 1, 2);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (16, 'toDelete', 14, 2);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (15, 'Gruppe XY', 13, 9);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (18, 'Test Gruppe ABC', 13, 9);
INSERT INTO "group" (id, groupname, group_admin, id_orga) VALUES (17, 'Test Gruppe', 27, 9);


--
-- TOC entry 2796 (class 0 OID 26413)
-- Dependencies: 258
-- Data for Name: filesharedforgroup; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO filesharedforgroup (id_group, id_file) VALUES (15, 42);
INSERT INTO filesharedforgroup (id_group, id_file) VALUES (12, 42);
INSERT INTO filesharedforgroup (id_group, id_file) VALUES (1, 46);


--
-- TOC entry 2797 (class 0 OID 26416)
-- Dependencies: 259
-- Data for Name: filesharedfororga; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO filesharedfororga (id_file, id_orga) VALUES (42, 9);
INSERT INTO filesharedfororga (id_file, id_orga) VALUES (44, 1);
INSERT INTO filesharedfororga (id_file, id_orga) VALUES (38, 9);
INSERT INTO filesharedfororga (id_file, id_orga) VALUES (47, 9);
INSERT INTO filesharedfororga (id_file, id_orga) VALUES (39, 1);


--
-- TOC entry 2798 (class 0 OID 26419)
-- Dependencies: 260
-- Data for Name: groupmember; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO groupmember (id_group, id_user) VALUES (18, 12);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 12);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 11);
INSERT INTO groupmember (id_group, id_user) VALUES (18, 2);
INSERT INTO groupmember (id_group, id_user) VALUES (18, 13);
INSERT INTO groupmember (id_group, id_user) VALUES (17, 12);
INSERT INTO groupmember (id_group, id_user) VALUES (18, 11);
INSERT INTO groupmember (id_group, id_user) VALUES (18, 26);
INSERT INTO groupmember (id_group, id_user) VALUES (17, 27);
INSERT INTO groupmember (id_group, id_user) VALUES (17, 13);
INSERT INTO groupmember (id_group, id_user) VALUES (17, 3);
INSERT INTO groupmember (id_group, id_user) VALUES (1, 1);
INSERT INTO groupmember (id_group, id_user) VALUES (1, 3);
INSERT INTO groupmember (id_group, id_user) VALUES (11, 2);
INSERT INTO groupmember (id_group, id_user) VALUES (11, 1);
INSERT INTO groupmember (id_group, id_user) VALUES (11, 3);
INSERT INTO groupmember (id_group, id_user) VALUES (1, 2);
INSERT INTO groupmember (id_group, id_user) VALUES (12, 3);
INSERT INTO groupmember (id_group, id_user) VALUES (12, 12);
INSERT INTO groupmember (id_group, id_user) VALUES (1, 12);
INSERT INTO groupmember (id_group, id_user) VALUES (13, 1);
INSERT INTO groupmember (id_group, id_user) VALUES (13, 2);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 13);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 15);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 17);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 18);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 14);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 21);
INSERT INTO groupmember (id_group, id_user) VALUES (15, 3);


--
-- TOC entry 2805 (class 0 OID 26461)
-- Dependencies: 271
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO player (name, avatar_img) VALUES ('player1', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('player2', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('player3', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('player4', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('mybodyplayer1', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('mybodyplayer21', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('allRequestFields', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('player111', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('player222', 'myimg.jpg');
INSERT INTO player (name, avatar_img) VALUES ('player333', 'myimg.jpg');


--
-- TOC entry 2800 (class 0 OID 26428)
-- Dependencies: 263
-- Data for Name: highscore; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2787 (class 0 OID 26135)
-- Dependencies: 237
-- Data for Name: ldap_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2810 (class 0 OID 26782)
-- Dependencies: 289
-- Data for Name: mediafile; Type: TABLE DATA; Schema: public; Owner: placity
--

-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (88, 'DSC01206.JPG', '/upload/2016_1_29/', 'http://143.93.91.92/upload/2016_1_29/DSC01206.JPG', 1, 'image/jpeg', 5439488, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (90, 'Test123Rakete.jpg', '/upload/2016_1_29/', 'http://143.93.91.92/upload/2016_1_29/Test123Rakete.jpg', 1, 'image/jpeg', 610318, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (94, '123.jpg.jpg', '/upload/2016_2_2/', 'http://143.93.91.92/upload/2016_2_2/123.jpg.jpg', 1, 'image/jpeg', 610318, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (96, 'Affe_undefined.jpg', '/upload/12/', 'http://143.93.91.92/upload/12/Affe_undefined.jpg', 1, 'image/jpeg', 722020, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (98, 'Alligatoah - Musik Ist Keine Loesung (2015_12.jpg', '/upload/12/', 'http://143.93.91.92/upload/12/Alligatoah - Musik Ist Keine Loesung (2015_12.jpg', 1, 'image/jpeg', 722020, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (100, 'iHerzU_12.gif', '/upload/12/', 'http://143.93.91.92/upload/12/iHerzU_12.gif', 1, 'image/gif', 381940, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (104, 'Mein1.JPG', '/upload/2016_1_29/', 'http://143.93.91.92/upload/2016_1_29/DSC01206.JPG', 1, 'image/jpeg', 5439488, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (47, 'raclette.JPG', '2016_1_24/', 'http://143.93.91.92/upload/2016_1_24/raclette.JPG', 1, 'image/jpeg', 5242880, 11, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (46, 'darth_vader_armor_star_wars_film_hat_snow_93645_2560x1024.jpg', '2016_1_20/', 'http://143.93.91.92/upload/2016_1_20/darth_vader_armor_star_wars_film_hat_snow_93645_2560x1024.jpg', 1, 'image/jpeg', 358894, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (44, '123.JPG', '2016_1_19/', 'http://143.93.91.92/upload/2016_1_19/123.JPG', 1, 'image/jpeg', 4947968, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (43, 'giphy.gif', '2016_1_17/', 'http://143.93.91.92/upload/2016_1_17/giphy.gif', 1, 'image/gif', 381940, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (42, '90453.jpg', '2016_1_16/', 'http://143.93.91.92/upload/2016_1_16/90453.jpg', 1, 'image/jpeg', 524425, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (50, 'big_buck_bunny_720p_5mb.mp4', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_5mb.mp4', 1, 'video', 5253880, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (64, 'big_buck_bunny_720p_50mb.flv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_50mb.flv', 1, 'video', 52512308, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (61, 'big_buck_bunny_240p_50mb.mp4', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_240p_50mb.mp4', 1, 'video', 51246445, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (38, 'big-buck-bunny_trailer.webm', '2016_1_16/', 'http://143.93.91.92/upload/2016_1_16/big-buck-bunny_trailer.webm', 1, 'video/webm', 2165175, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (39, 'Ab An De See.mp3', '2016_1_16/', 'http://143.93.91.92/upload/2016_1_16/Ab An De See.mp3', 1, 'audio/mp3', 7719028, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (58, 'big_buck_bunny_360p_50mb.mp4', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_360p_50mb.mp4', 1, 'video', 52489326, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (55, 'big_buck_bunny_480p_50mb.mp4', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_480p_50mb.mp4', 1, 'video', 52460977, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (52, 'big_buck_bunny_720p_50mb.mp4', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_50mb.mp4', 1, 'video', 52464391, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (51, 'big_buck_bunny_720p_10mb.mp4', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_10mb.mp4', 1, 'video', 10498677, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (89, 'rakete.jpg', '/upload/2016_1_29/', 'http://143.93.91.92/upload/2016_1_29/rakete.jpg', 1, 'image/jpeg', 610318, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (91, '123.jpg', '/upload/2016_2_1/', 'http://143.93.91.92/upload/2016_2_1/123.jpg', 1, 'image/jpeg', 1303391, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (95, 'Alligatoah - Musik Ist Keine Loesung (2015).jpg', '/upload/2016_2_2/', 'http://143.93.91.92/upload/2016_2_2/Alligatoah - Musik Ist Keine Loesung (2015).jpg', 1, 'image/jpeg', 722020, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (97, 'AffeABC_12.jpg', '/upload/12/', 'http://143.93.91.92/upload/12/AffeABC_12.jpg', 1, 'image/jpeg', 722020, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (101, 'DS_12.JPG', '/upload/12/', 'http://143.93.91.92/upload/12/DS_12.JPG', 1, 'image/jpeg', 3490897, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (103, 'Mein.JPG', '/upload/2016_1_29/', 'http://143.93.91.92/upload/2016_1_29/DSC01206.JPG', 1, 'image/jpeg', 5439488, 12, false, true);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (87, 'baderegeln.pdf', '/upload/2016_1_25/', 'http://143.93.91.92/upload/2016_1_25/baderegeln.pdf', 1, 'application/pdf', 710773, 12, false, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (85, 'big_buck_bunny_240p_50mb.mkv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_240p_50mb.mkv', 1, 'video', 52474687, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (82, 'big_buck_bunny_360p_50mb.mkv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_360p_50mb.mkv', 1, 'video', 52459935, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (79, 'big_buck_bunny_480p_50mb.mkv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_480p_50mb.mkv', 1, 'video', 52515793, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (76, 'big_buck_bunny_720p_50mb.mkv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_720p_50mb.mkv', 1, 'video', 52524751, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (73, 'big_buck_bunny_240p_50mb.flv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_240p_50mb.flv', 1, 'video', 52505638, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (70, 'big_buck_bunny_360p_50mb.flv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_360p_50mb.flv', 1, 'video', 52498525, 1, true, false);
-- INSERT INTO mediafile (id, file_name, path, url, public, file_type, filesize, id_user, encoding, encoded) VALUES (67, 'big_buck_bunny_480p_50mb.flv', '/upload/testfiles/', 'http://143.93.91.92/upload/testfiles/big_buck_bunny_480p_50mb.flv', 1, 'video', 52480075, 1, true, false);



-- TOC entry 2720 (class 0 OID 25295)
-- Dependencies: 170
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO migrations (migration, batch) VALUES ('2015_01_27_190908_create_system_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_02_03_161456_create_sqldb_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_02_03_161457_create_couchdb_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_02_03_161457_create_mongodb_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_02_03_161457_create_salesforce_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_03_10_135522_create_aws_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_03_11_143913_create_rackspace_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_03_20_205504_create_remote_web_service_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_03_20_205504_create_soap_service_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_05_02_134911_update_user_table_for_oauth_support', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_05_04_034605_create_adldap_tables', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_05_21_190727_create_user_config_table', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_07_10_161839_create_user_custom_table', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_08_24_180219_default_schema_only', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_08_25_202632_db_alias', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_11_06_155036_db_function', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_11_10_225902_db_foreign_key', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_11_17_181913_ad_enhancements', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_12_29_154754_myDB', 1);


--
-- TOC entry 2776 (class 0 OID 25998)
-- Dependencies: 226
-- Data for Name: mongodb_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2786 (class 0 OID 26116)
-- Dependencies: 236
-- Data for Name: oauth_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2801 (class 0 OID 26440)
-- Dependencies: 265
-- Data for Name: one_time_access_code; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO one_time_access_code (code, id_orga, id_group) VALUES ('ddd73ecb623c73dedad0897a006644', 9, 18);
INSERT INTO one_time_access_code (code, id_orga, id_group) VALUES ('16638b3ec41215ed31e49cc1fde9e7', 9, 18);


--
-- TOC entry 2802 (class 0 OID 26443)
-- Dependencies: 266
-- Data for Name: orgamember; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO orgamember (id_orga, id_user) VALUES (1, 1);
INSERT INTO orgamember (id_orga, id_user) VALUES (1, 2);
INSERT INTO orgamember (id_orga, id_user) VALUES (2, 3);
INSERT INTO orgamember (id_orga, id_user) VALUES (2, 1);


--
-- TOC entry 2725 (class 0 OID 25351)
-- Dependencies: 175
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2779 (class 0 OID 26038)
-- Dependencies: 229
-- Data for Name: rackspace_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2793 (class 0 OID 26242)
-- Dependencies: 243
-- Data for Name: role_adldap; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2826 (class 0 OID 0)
-- Dependencies: 187
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_id_seq', 4, true);


--
-- TOC entry 2742 (class 0 OID 25555)
-- Dependencies: 192
-- Data for Name: role_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2827 (class 0 OID 0)
-- Dependencies: 191
-- Name: role_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_lookup_id_seq', 1, false);


--
-- TOC entry 2740 (class 0 OID 25521)
-- Dependencies: 190
-- Data for Name: role_service_access; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO role_service_access (id, role_id, service_id, component, verb_mask, requestor_mask, filters, filter_op, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (2, 2, NULL, '*', 31, 1, '[]', 'AND', '2016-01-05 14:31:06', '2016-01-05 17:45:48', NULL, NULL);
INSERT INTO role_service_access (id, role_id, service_id, component, verb_mask, requestor_mask, filters, filter_op, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (4, 3, NULL, '*', 31, 3, '[]', 'AND', '2016-01-16 11:31:38', '2016-01-16 11:31:58', NULL, NULL);
INSERT INTO role_service_access (id, role_id, service_id, component, verb_mask, requestor_mask, filters, filter_op, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (5, 3, 4, '*', 1, 1, '[]', 'AND', '2016-01-16 11:34:09', '2016-02-12 17:14:00', NULL, NULL);
INSERT INTO role_service_access (id, role_id, service_id, component, verb_mask, requestor_mask, filters, filter_op, created_date, last_modified_date, created_by_id, last_modified_by_id) VALUES (6, 3, 4, '_table/highscore/', 3, 1, '[]', 'AND', '2016-02-12 17:14:00', '2016-02-12 17:14:00', NULL, NULL);


--
-- TOC entry 2828 (class 0 OID 0)
-- Dependencies: 189
-- Name: role_service_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('role_service_access_id_seq', 6, true);


--
-- TOC entry 2808 (class 0 OID 26480)
-- Dependencies: 275
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO tag (tag_name) VALUES ('best');
INSERT INTO tag (tag_name) VALUES ('hammer');
INSERT INTO tag (tag_name) VALUES ('placity');
INSERT INTO tag (tag_name) VALUES ('route');
INSERT INTO tag (tag_name) VALUES ('hunde');
INSERT INTO tag (tag_name) VALUES ('geil');


--
-- TOC entry 2807 (class 0 OID 26477)
-- Dependencies: 274
-- Data for Name: routentag; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO routentag (id_route, tag_name) VALUES (1, 'best');
INSERT INTO routentag (id_route, tag_name) VALUES (1, 'hammer');
INSERT INTO routentag (id_route, tag_name) VALUES (1, 'placity');
INSERT INTO routentag (id_route, tag_name) VALUES (770, 'route');
INSERT INTO routentag (id_route, tag_name) VALUES (1, 'route');
INSERT INTO routentag (id_route, tag_name) VALUES (1, 'hunde');
INSERT INTO routentag (id_route, tag_name) VALUES (2, 'geil');
INSERT INTO routentag (id_route, tag_name) VALUES (31, 'route');
INSERT INTO routentag (id_route, tag_name) VALUES (32, 'route');
INSERT INTO routentag (id_route, tag_name) VALUES (2, 'route');


--
-- TOC entry 2780 (class 0 OID 26051)
-- Dependencies: 230
-- Data for Name: rws_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2784 (class 0 OID 26086)
-- Dependencies: 234
-- Data for Name: rws_headers_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2829 (class 0 OID 0)
-- Dependencies: 233
-- Name: rws_headers_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('rws_headers_config_id_seq', 1, false);


--
-- TOC entry 2782 (class 0 OID 26066)
-- Dependencies: 232
-- Data for Name: rws_parameters_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2830 (class 0 OID 0)
-- Dependencies: 231
-- Name: rws_parameters_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('rws_parameters_config_id_seq', 1, false);


--
-- TOC entry 2777 (class 0 OID 26012)
-- Dependencies: 227
-- Data for Name: salesforce_db_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2733 (class 0 OID 25432)
-- Dependencies: 183
-- Data for Name: script_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2772 (class 0 OID 25941)
-- Dependencies: 222
-- Data for Name: service_cache_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO service_cache_config (service_id, cache_enabled, cache_ttl) VALUES (4, true, 0);


--
-- TOC entry 2731 (class 0 OID 25408)
-- Dependencies: 181
-- Data for Name: service_doc; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2831 (class 0 OID 0)
-- Dependencies: 180
-- Name: service_doc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('service_doc_id_seq', 1, false);


--
-- TOC entry 2832 (class 0 OID 0)
-- Dependencies: 178
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('service_id_seq', 12, true);


--
-- TOC entry 2756 (class 0 OID 25745)
-- Dependencies: 206
-- Data for Name: smtp_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO smtp_config (service_id, host, port, encryption, username, password) VALUES (9, 'smtp.gmail.com', '587', 'tls', 'eyJpdiI6ImFOMkpESjlUMmV5M0J4NmtvQmp4bFE9PSIsInZhbHVlIjoiZnFDWCtUNzgwXC94TTVvVXF1NkJVbXNWSmx6bDUycGZFbVZNT3hjcWZqalU9IiwibWFjIjoiMmM4NzQ1NDdjYmU5ZDc3MzZmZjhjNTU3ODlkODM5ODY3ZWM1ZTZhMjc3MWQ3ZDI1YzZiODc2YTNlMzI3MzNkZCJ9', 'eyJpdiI6ImdYamk3UGZ2b0d6TUdvNWVpa1ZHOFE9PSIsInZhbHVlIjoic1hydWF1Nmoxd3FvV05LRml5RitzOG84MHlcL0RTU2VnVWJicjBaamNMUTQ9IiwibWFjIjoiNmIwNzJmMDE3M2U2ZWNhYjM2NjBkYWNiNjM5NTM1ZjU5YTg0MzhhM2VkYzg4YTk2NTQzOWNmZDA2ZmY4NmJmMCJ9');


--
-- TOC entry 2785 (class 0 OID 26102)
-- Dependencies: 235
-- Data for Name: soap_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2774 (class 0 OID 25971)
-- Dependencies: 224
-- Data for Name: sql_db_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO sql_db_config (service_id, driver, dsn, username, password, options, attributes, default_schema_only) VALUES (4, 'pgsql', 'pgsql:host=localhost;port=5432;dbname=placity', 'eyJpdiI6IkRLWkp2eHk1U1BZU1AwM2M4OEViUWc9PSIsInZhbHVlIjoiTjllbTR1a29hUHZCS3FsMEl2UkYrZz09IiwibWFjIjoiZDM3Yzk1NDZmNzNhMzE0NjVmZmM3MmEyMTQyYzA1ZWEzM2Q0MDJhN2I3MzA2N2MyZTVhNmE2Y2YxODhlMDViOCJ9', 'eyJpdiI6InJiblFCd3lmQjZTV0hqenh4SmN3UGc9PSIsInZhbHVlIjoieWFiQndrSWVGM1FUZGJhY2dGbW1LUT09IiwibWFjIjoiZWQ1YzVhNDZmY2I4ODFjNDNmMTEwZDA0MzM3ODljM2Y0MTk3MDM5ZGI2ZWVjMmEwYWIwM2MyNTdlMjBhMTRiYSJ9', NULL, NULL, false);


--
-- TOC entry 2753 (class 0 OID 25703)
-- Dependencies: 203
-- Data for Name: system_config; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2773 (class 0 OID 25953)
-- Dependencies: 223
-- Data for Name: system_custom; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2746 (class 0 OID 25606)
-- Dependencies: 196
-- Data for Name: system_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2833 (class 0 OID 0)
-- Dependencies: 195
-- Name: system_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('system_lookup_id_seq', 1, false);


--
-- TOC entry 2727 (class 0 OID 25368)
-- Dependencies: 177
-- Data for Name: system_resource; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('admin', 'DreamFactory\Core\Resources\System\Admin', 'Administrators', 'Allows configuration of system administrators.', 'DreamFactory\Core\Models\User', false, false, '2015-12-29 16:11:09', '2015-12-29 16:11:09');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('cache', 'DreamFactory\Core\Resources\System\Cache', 'Cache Administration', 'Allows administration of system-wide and service cache.', NULL, false, false, '2015-12-29 16:11:09', '2015-12-29 16:11:09');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('config', 'DreamFactory\Core\Resources\System\Config', 'Configuration', 'Global system configuration.', NULL, true, false, '2015-12-29 16:11:10', '2015-12-29 16:11:10');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('constant', 'DreamFactory\Core\Resources\System\Constant', 'Constants', 'Read-only listing of constants available for client use.', NULL, false, true, '2015-12-29 16:11:10', '2015-12-29 16:11:10');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('cors', 'DreamFactory\Core\Resources\System\Cors', 'CORS Configuration', 'Allows configuration of CORS system settings.', 'DreamFactory\Core\Models\CorsConfig', false, false, '2015-12-29 16:11:10', '2015-12-29 16:11:10');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('email_template', 'DreamFactory\Core\Resources\System\EmailTemplate', 'Email Templates', 'Allows configuration of email templates.', 'DreamFactory\Core\Models\EmailTemplate', false, false, '2015-12-29 16:11:10', '2015-12-29 16:11:10');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('environment', 'DreamFactory\Core\Resources\System\Environment', 'Environment', 'Read-only system environment configuration.', NULL, true, true, '2015-12-29 16:11:10', '2015-12-29 16:11:10');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('event', 'DreamFactory\Core\Resources\System\Event', 'Events', 'Allows registering server-side scripts to system generated events.', NULL, false, false, '2015-12-29 16:11:11', '2015-12-29 16:11:11');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('lookup', 'DreamFactory\Core\Resources\System\Lookup', 'Lookup Keys', 'Allows configuration of lookup keys.', 'DreamFactory\Core\Models\Lookup', false, false, '2015-12-29 16:11:11', '2015-12-29 16:11:11');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('role', 'DreamFactory\Core\Resources\System\Role', 'Roles', 'Allows role configuration.', 'DreamFactory\Core\Models\Role', false, false, '2015-12-29 16:11:11', '2015-12-29 16:11:11');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('service', 'DreamFactory\Core\Resources\System\Service', 'Services', 'Allows configuration of services.', 'DreamFactory\Core\Models\Service', false, false, '2015-12-29 16:11:11', '2015-12-29 16:11:11');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('service_type', 'DreamFactory\Core\Resources\System\ServiceType', 'Service Types', 'Read-only system service types.', 'DreamFactory\Core\Models\ServiceType', false, true, '2015-12-29 16:11:12', '2015-12-29 16:11:12');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('script_type', 'DreamFactory\Core\Resources\System\ScriptType', 'Script Types', 'Read-only system scripting types.', 'DreamFactory\Core\Models\ScriptType', false, true, '2015-12-29 16:11:12', '2015-12-29 16:11:12');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('setting', 'DreamFactory\Core\Resources\System\Setting', 'Custom Settings', 'Allows configuration of system-wide custom settings.', 'DreamFactory\Core\Models\Setting', false, false, '2015-12-29 16:11:12', '2015-12-29 16:11:12');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('app', 'DreamFactory\Core\Resources\System\App', 'Apps', 'Allows management of user application(s)', 'DreamFactory\Core\Models\App', false, false, '2015-12-29 16:11:12', '2015-12-29 16:11:12');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('app_group', 'DreamFactory\Core\Resources\System\AppGroup', 'App Groups', 'Allows grouping of user application(s)', 'DreamFactory\Core\Models\AppGroup', false, false, '2015-12-29 16:11:12', '2015-12-29 16:11:12');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('custom', 'DreamFactory\Core\Resources\System\Custom', 'Custom Settings', 'Allows for creating system-wide custom settings', 'DreamFactory\Core\Models\SystemCustom', false, false, '2015-12-29 16:11:13', '2015-12-29 16:11:13');
INSERT INTO system_resource (name, class_name, label, description, model_name, singleton, read_only, created_date, last_modified_date) VALUES ('user', 'DreamFactory\Core\User\Resources\System\User', 'User Management', 'Allows user management capability.', 'DreamFactory\Core\Models\User', false, false, '2015-12-29 16:11:24', '2015-12-29 16:11:24');


--
-- TOC entry 2744 (class 0 OID 25583)
-- Dependencies: 194
-- Data for Name: system_setting; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2834 (class 0 OID 0)
-- Dependencies: 193
-- Name: system_setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('system_setting_id_seq', 1, false);


--
-- TOC entry 2770 (class 0 OID 25917)
-- Dependencies: 220
-- Data for Name: token_map; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2788 (class 0 OID 26154)
-- Dependencies: 238
-- Data for Name: user_config; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO user_config (service_id, allow_open_registration, open_reg_role_id, open_reg_email_service_id, open_reg_email_template_id, invite_email_service_id, invite_email_template_id, password_email_service_id, password_email_template_id) VALUES (6, true, 2, NULL, NULL, NULL, NULL, 9, 3);


--
-- TOC entry 2790 (class 0 OID 26167)
-- Dependencies: 240
-- Data for Name: user_custom; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2835 (class 0 OID 0)
-- Dependencies: 239
-- Name: user_custom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_custom_id_seq', 7, true);


--
-- TOC entry 2836 (class 0 OID 0)
-- Dependencies: 171
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_id_seq', 30, true);


--
-- TOC entry 2724 (class 0 OID 25325)
-- Dependencies: 174
-- Data for Name: user_lookup; Type: TABLE DATA; Schema: public; Owner: placity
--



--
-- TOC entry 2837 (class 0 OID 0)
-- Dependencies: 173
-- Name: user_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_lookup_id_seq', 1, false);


--
-- TOC entry 2769 (class 0 OID 25896)
-- Dependencies: 219
-- Data for Name: user_to_app_to_role; Type: TABLE DATA; Schema: public; Owner: placity
--

INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (17, 11, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (18, 11, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (19, 11, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (20, 11, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (21, 12, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (22, 12, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (23, 12, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (24, 12, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (25, 13, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (26, 13, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (27, 13, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (28, 13, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (29, 14, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (30, 14, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (31, 14, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (32, 14, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (33, 15, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (34, 15, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (35, 15, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (36, 15, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (41, 17, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (42, 17, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (43, 17, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (44, 17, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (46, 18, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (47, 18, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (48, 18, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (49, 18, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (52, 19, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (53, 19, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (54, 19, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (56, 23, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (57, 23, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (58, 23, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (59, 23, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (61, 23, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (62, 19, 12, 3);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (51, 19, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (63, 24, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (64, 24, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (65, 24, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (66, 24, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (68, 24, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (69, 25, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (70, 25, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (71, 25, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (72, 25, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (74, 25, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (75, 26, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (76, 26, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (77, 26, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (78, 26, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (80, 26, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (81, 27, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (82, 27, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (83, 27, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (84, 27, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (86, 27, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (87, 28, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (88, 28, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (89, 28, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (90, 28, 6, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (92, 28, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (93, 30, 1, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (94, 30, 3, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (95, 30, 12, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (96, 30, 2, 2);
INSERT INTO user_to_app_to_role (id, user_id, app_id, role_id) VALUES (97, 30, 6, 2);


--
-- TOC entry 2838 (class 0 OID 0)
-- Dependencies: 218
-- Name: user_to_app_to_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: placity
--

SELECT pg_catalog.setval('user_to_app_to_role_id_seq', 97, true);


-- Completed on 2016-02-16 22:20:11

--
-- PostgreSQL database dump complete
--

