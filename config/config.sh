#!/bin/bash 

############################################################################
## ----------------------------------------------------------------------- #
## Konfigurationsdatei der Placity-Installation                            #
## File: config.sh                                         pc4install.tar  #
## ----------------------------------------------------------------------- #
## Author: Alexander Weiß                                                  #
## ----------------------------------------------------------------------- #
## Für die Installation des Servers werden drei Subdomains benötigt.       #
##      Placity-App     -> z.B. placity.DOMAIN.tld                         #
##      QuestionEditor  -> z.B. qe.DOMAIN.tld                              #
##      Dreamfactory    -> z.B. df2.DOMAIN.tld                             #
## ----------------------------------------------------------------------- #
## Für Live-System: DNS-Einträge über Webhoster für alle (Sub-)Domains     #
## Für DevEnv: Konfiguration über hosts-Datei z.B. für VMware/vbox         #
## Eintrag in HOSTS-DATEI: IP-VM     pc4.vbox .df.pc4.vbox qe.pc4.vbox ... #
## ####################################################################### #


## INSTALL_DIRs
PC_INSTALL_DIR=/opt/pc4
PC_DF2_DIR=$PC_INSTALL_DIR/df2
PC_APP_DIR=$PC_INSTALL_DIR/app
PC_QE_DIR=$PC_INSTALL_DIR/qe
PC_FILESERVER=$PC_INSTALL_DIR/fileserver
PC_CONFIG=$PC_INSTALL_DIR/config
FF_MPEG_DIR=$PC_INSTALL_DIR/ffmpeg
DB_DUMPS_DIR=$PC_INSTALL_DIR/DB-CTs-Src-Doku/Datenbank/dumps

##(SUB-) DOMAINs / ALIAS
DOMAIN=pc4.vbox
PLACITY_SUBDOMAIN=placity.$DOMAIN
DF2_SUBDOMAIN=df.$DOMAIN
QE_SUBDOMAIN=qe.$DOMAIN
ALIAS_MEDIA=$DOMAIN/media
REST_API_URL=$DF2_SUBDOMAIN
APP_API_KEY=''
QE_API_KEY=b06beaf7a884e085b20b8c1b10d2f4712c49526c4e11597d70bff5fa4ee05988
QE_SERVER_URL=$ALIAS_MEDIA

## passwords
PW_MYSQL_ROOT=VAPPS2016
PW_PGSQL_ROOT=VAPPS2016
PGPASSWORD=placity
PW_PG_PLACITY=$PGPASSWORD


##ROOT_DIRs 
APP_ROOT_DIR=$PC_APP_DIR/platforms/browser/www
QE_ROOT_DIR=$PC_QE_DIR/www
DF2_ROOT_DIR=$PC_DF2_DIR/public
FILE_STORE=$PC_DF2_DIR/storage/app
FILES=$PC_FILESERVER/mediafiles

##CONFIG
MAIN_CONF_INI=$PC_CONFIG/pc4-main.ini
MAIN_CONF_SH=$PC_CONFIG/config.sh
PLACITY_DB_STRUCT=$DB_DUMPS_DIR/Placity_backup_Struct_final.sql
PLACITY_DB_DUMP=$DB_DUMPS_DIR/DF2_DB_dump_16.02.2016_full_.sql
PLACITY_DB_DATA=$DB_DUMPS_DIR/placity_db_data_final.sql


PC4_GIT=git@bitbucket.org:vappsfh/pc4.git
APP_GIT=https://github.com/paul280/placity_2.git
DF2_GIT=git@bitbucket.org:albus-fh/df2.git
FF_MPEG_GIT=git@bitbucket.org:vappsfh/ffmpeg-custom-build.git
FILESERVER_GIT=git@bitbucket.org:albus-fh/transcoding.git
QE_GIT=git@bitbucket.org:vappsfh/qe.git


# -------------------------------------------------------------------------
# INSTALL_DIRs
export PC_INSTALL_DIR PC_DF2_DIR PC_APP_DIR PC_QE_DIR 
export PC_CONFIG FF_MPEG_DIR PC_FILESERVER DB_DUMPS_DIR
#(SUB-) DOMAINs / ALIAS
export DOMAIN PLACITY_SUBDOMAIN DF2_SUBDOMAIN QE_SUBDOMAIN
export ALIAS_MEDIA REST_API_URL QE_API_KEY APP_API_KEY QE_SERVER_URL
# passwords
export PW_MYSQL_ROOT PW_PGSQL_ROOT PW_PG_PLACITY PGPASSWORD
#ROOT_DIRs 
export APP_ROOT_DIR QE_ROOT_DIR DF2_ROOT_DIR FILE_STORE FILES
#CONFIG
export MAIN_CONF_SH PLACITY_DB_DUMP PLACITY_DB_STRUCT PLACITY_DB_DATA
#GIT
export PC4_GIT APP_GIT DF2_GIT FF_MPEG_GIT FILESERVER_GIT QE_GIT 

# -------------------------------------------------------------------------
# setup script environment
addTo(){
cat<<eof

. $PC_CONFIG/config.sh
if [ -f $(pwd)/config.sh ] ; then . $(pwd)/config.sh ; fi
## highlighting with some colors
function logError(){ cat /dev/stdin | sed 's,.*,\x1B[31m&\x1B[0m,'; }   
function highlight(){ cat /dev/stdin | sed 's,.*,\x1B[33m&\x1B[0m,' ; }  
function myecho(){ cat /dev/stdin | sed 's,.*,\x1B[32m&\x1B[0m,' ; }     
export  logError
export  highlight
export  myecho
if [ -d /opt/pc4/df2/storage/app ] && [ -d /opt/pc4/fileserver/mediafiles ] ; then
mount --bind $FILE_STORE $FILES
fi
function pcurl() { 
    curl "$@" | json | pygmentize -l json
}
export -f pcurl
function jcurl() {
    curl "$@" | json 
}
export -f jcurl

function cjson() {
    cat "$@" | json | pygmentize -l json
}
export -f cjson
eof
}
grep -e '## highlighting with some colors' ~/.bashrc     >/dev/null || addTo >> ~/.bashrc
grep -e '## highlighting with some colors'  /etc/profile >/dev/null || addTo >> /etc/profile

