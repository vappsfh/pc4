﻿# AngularJS - Datenbankservice Modul 
---
Das Datenbankservice-Modul erleichtert die Nutzung der REST-API und kapselt alle HTTP-Requsts in einer einheitlichen Form. 

## Einbinden in AngularJS

Zum Einbinden in einer AngularJS-Anwendung folgende Schritte duchführen 

1. In index.html einbinden

![Einbinden in index.html](InclToIndex.jpg)

2. Source-Ordner in APP-Code integriern

![Source-Ordner in APP-Code integriern](copyToModule.jpg)

3. Modul einbinden / Entity Types injizieren 
CRUD Operationen über GET/CREATE/REPLACE/UPDATE/REMOVE

![USAGE-Beipiel](usage_controller.jpg)


## API Doku ind Source-Code 
Parameter für die Methoden-Aufrufe entspricht der Dreamfactory-Api-Doku unter: 
[API-DOKU DREAMFAKTORY](http://df.albus-it.com/swagger/index.html)

Hier ein paar kurze Beispiel: 

```
     *  * ---
     * CRUD - Operationen via REST-Schnittstelle für den Entitytypen Player
     *
     * @example:
     *  - get:       Player.get({id:'1',related:'*'})   selectiert Player mit id=1 und führt alle Fremdschlüssel
     *                                                  Assoziationen an.
     *  - create:    Player.get({Key:VALUE})            erstellt einen neuen Eintrag mit Key:VALUE
     *                                                  Rückgabe: ID
     *  - replace:   Player.replace({id:1,KEY:VALUE})   ersetzt KEY:VALUE in Player.id=1
     *  - update:    Player.update({id:1,KEY:VALUE})    update von KEY:VALUE in Player.id=1
     *  - delete:    Player.remove({id:1})              löscht Player.id=1
     *               Player.remove({filter:'id>2'})     löscht alle Einträge mit ID > 2
     */
```
####JSDoc in SourceCode über IDE 

![JSDoc in SourceCode über IDE](JSDocInSourceCode.jpg)